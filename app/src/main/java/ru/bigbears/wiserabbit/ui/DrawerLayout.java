package ru.bigbears.wiserabbit.ui;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import ru.bigbears.wiserabbit.R;

public class DrawerLayout extends RelativeLayout {

    private final String TAG = "Drawer";
    private static final int MIN_FLING_VELOCITY = 400; // dips per second

    private ViewDragHelper mDragger;
    private ViewDragCallback mDragCallback;
    private View mMenuButton;
    private View mDrawerView;
    private boolean mIsOpen = false;

    private DrawerListener mListener;

    private MotionEvent mLastMotionToIntercept;

    private int mOffset;


    public DrawerLayout(Context context) {
        super(context);
        init(context);
    }

    public DrawerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DrawerLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public void setDrawerListener(DrawerListener l) {
        mListener = l;
    }

    public void setDrawerView(View v) {
        mDrawerView = v;
        mMenuButton = mDrawerView.findViewById(R.id.btnMenu);
        mIsOpen = false;
    }

    private void init(Context ctx) {
        mDragCallback = new ViewDragCallback();
        mDragger = ViewDragHelper.create(this, 1.f, mDragCallback);
        mDragger.setMinVelocity(MIN_FLING_VELOCITY * ctx.getResources().getDisplayMetrics().density);
        mOffset = ctx.getResources().getDimensionPixelOffset(R.dimen.drawer_offset);
    }

    public void toggle() {
        if (mIsOpen) {
            close();
        } else {
            open();
        }
    }

    public boolean isOpen() {
        return mIsOpen;
    }

    public void close() {
        mIsOpen = false;
        mDragger.smoothSlideViewTo(mDrawerView, -mDrawerView.getWidth() + mOffset, mDrawerView.getTop());
        if (mListener != null) {
            mListener.OnDrawerClosed();
        }
        invalidate();
    }

    public void open() {
        mIsOpen = true;
        mDragger.smoothSlideViewTo(mDrawerView, 0, mDrawerView.getTop());
        if (mListener != null) {
            mListener.OnDrawerOpened();
        }
        invalidate();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        mDrawerView.offsetLeftAndRight(-mDrawerView.getWidth() + mOffset);
    }

    @Override
    public void computeScroll() {
        if (mDragger.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mDragger.processTouchEvent(event);
        return true;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        mLastMotionToIntercept = ev;
        switch (ev.getAction()) {

            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP: {
                if (mDrawerView != mDragger.findTopChildUnder((int) ev.getX(), (int) ev.getY())) {
                    close();
                }
                break;
            }
        }

        if (mDragger.shouldInterceptTouchEvent(ev)) {
            return true;
        }
        return false;
    }

    private class ViewDragCallback extends ViewDragHelper.Callback {
        private ViewDragHelper mDragger;


        public ViewDragCallback() {
        }

        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            if (mIsOpen) {
                return child == mDrawerView;

            } else {
                Rect r = new Rect();
                mMenuButton.getGlobalVisibleRect(r);
                return child == mDrawerView && r.contains((int)mLastMotionToIntercept.getX(), (int)mLastMotionToIntercept.getY());
            }
        }

        @Override
        public void onViewDragStateChanged(int state) {
        }

        @Override
        public void onViewPositionChanged(View changedView, int left, int top, int dx, int dy) {
            invalidate();
        }

        @Override
        public void onViewCaptured(View capturedChild, int activePointerId) {
        }


        @Override
        public void onViewReleased(View releasedChild, float xvel, float yvel) {
            float offset = (mDrawerView.getLeft() - (-mDrawerView.getWidth() + mOffset)) / (float) getViewHorizontalDragRange(releasedChild);
            if (xvel > 0 || xvel == 0 && offset > 0.5f) {
                open();
            } else {
                close();
            }
        }

        @Override
        public void onEdgeTouched(int edgeFlags, int pointerId) {
        }


        @Override
        public boolean onEdgeLock(int edgeFlags) {
            return false;
        }

        @Override
        public void onEdgeDragStarted(int edgeFlags, int pointerId) {
        }

        @Override
        public int getViewHorizontalDragRange(View child) {
            return child.getWidth() - mOffset;
        }

        @Override
        public int clampViewPositionHorizontal(View child, int left, int dx) {
            return Math.max(-child.getWidth() + mOffset, Math.min(left, 0));

        }

        @Override
        public int clampViewPositionVertical(View child, int top, int dy) {
            return child.getTop();
        }
    }

    public interface DrawerListener {
        void OnDrawerOpened();
        void OnDrawerClosed();
    }

}
