package ru.bigbears.wiserabbit.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

public class LadderLayout extends RelativeLayout implements View.OnClickListener {

    private int mSelectedIndex = 0;
    private OnItemClickListener mListener;
    private boolean mNeedReorder = true;

    public LadderLayout(Context context) {
        super(context);
    }

    public LadderLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LadderLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public interface OnItemClickListener {
        void onItemClick(View v, int index);
    }

    public void setOnItemClickListener(OnItemClickListener l) {
        mListener = l;
    }

    @Override
    public void addView(View v) {
        super.addView(v);
        v.setOnClickListener(this);
        mNeedReorder = true;
    }

    public void onClick(View v) {
        int index = indexOfChild(v);
        if (mListener != null) {
            mListener.onItemClick(v, index);
        }
        mSelectedIndex = index;
    }

    public void setSelectedIndex(int index) {
        mSelectedIndex = index;
        reorder();
    }

    private void reorder() {
        int childCount = getChildCount();
        Log.e("AA", "reorder");

        if (childCount == 0) return;

        View child;
        int availableWidth = getWidth() - getPaddingRight() - getPaddingLeft();
        int margin;
        if (childCount == 1) {
            margin = 0;
        } else {
            margin = (availableWidth) / (childCount + 1);
            int emptySpace = availableWidth - (margin * (childCount - 1) + getChildAt(childCount - 1).getWidth());
            margin += emptySpace / (childCount - 1);
        }

        LadderLayout.LayoutParams lp;
        for (int i = 0; i < childCount; i++) {
            child = getChildAt(i);
            if (child != null) {
                lp = new LadderLayout.LayoutParams(child.getLayoutParams());
                lp.setMargins(margin * i, 0, 0,  0);
                child.setLayoutParams(lp);
                child.setScaleX(1.f);
                child.setScaleY(1.f);
            }
        }

        child = getChildAt(mSelectedIndex + 1);
        if (child != null) {
            getChildAt(mSelectedIndex + 1).setScaleX(1.15f);
            getChildAt(mSelectedIndex + 1).setScaleY(1.15f);
        }
        child = getChildAt(mSelectedIndex - 1);
        if (child != null) {
            getChildAt(mSelectedIndex - 1).setScaleX(1.15f);
            getChildAt(mSelectedIndex - 1).setScaleY(1.15f);
        }
        child = getChildAt(mSelectedIndex);
        if (child != null) {
            getChildAt(mSelectedIndex).setScaleX(1.3f);
            getChildAt(mSelectedIndex).setScaleY(1.3f);
        }
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        int childCount = getChildCount();

        if (childCount == 0) return;

        if (mNeedReorder) {
            reorder();
            for (int i = 0; i < getChildCount(); i++) {
                View v = getChildAt(i);
                v.setPivotX(v.getWidth() / 2);
                v.setPivotY(v.getHeight());
            }
            mNeedReorder = false;
        }

        long drawingTime = getDrawingTime();
        View child;
        int range = Math.max(mSelectedIndex, childCount - 1 - mSelectedIndex);

        int index;

        for (int i = range; i > 0; i--) {
            index = mSelectedIndex + i;
            //Draw right child
            child = getChildAt(index);
            if (child != null) {
                drawChild(canvas, child, drawingTime);
            }

            //Draw left child
            index = mSelectedIndex - i;
            child = getChildAt(index);
            if (child != null) {
                drawChild(canvas, child, drawingTime);
            }
        }

        //Draw center child
        child = getChildAt(mSelectedIndex);
        if (child != null) {
            drawChild(canvas, child, drawingTime);
        }
    }
}
