/**
 * 
 */
package ru.bigbears.wiserabbit.activities;

import java.io.File;
import java.util.ArrayList;

import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.models.DirectoryElement;
import ru.bigbears.wiserabbit.models.HelperImage;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

/**
 * @author Vladimir
 *
 */
public class ActivityHelper extends ActivityBase {
	private static final String TAG = "My";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		menuButton.setVisibility(View.GONE);
		menuButton.setEnabled(false);

		config.setCanVisibleMenu(false);

        config.setChecked(true);
		checkBtn.setVisibility(View.VISIBLE);
		// get directory index from which we loading images
        Intent intent = getIntent();
        int index = intent.getIntExtra("index", 0);

		if (config.getChecked()) {
			index = config.getHelperDirectoryIndex();
			ArrayList<DirectoryElement> images = xmlParser.getFilesFromHelperDirectory(index);
			Log.i(TAG, "In Helper: " + images.size() + " files");

			navigatorAdapter.addImages(images);
			navigatorAdapter.notifyDataSetChanged();

			config.setHelperDirectoryIndex(index);

			mFlipper.setCurrentIndex(config.getHelperImageIndex());
		} else {
			ArrayList<DirectoryElement> images = xmlParser.getFilesFromHelperDirectory(index);
			Log.i(TAG, "In Helper: " + images.size() + " files");

			navigatorAdapter.addImages(images);
			navigatorAdapter.notifyDataSetChanged();

			config.setHelperDirectoryIndex(index);

			mFlipper.setCurrentIndex(0);
		}
		updateNavigation();
	}

	@Override
	protected void handleButtonHintPressed() {}

	@Override
	protected void handleButtonReferencePressed() {}

	@Override
	protected void handleOnVideoViewAnimationEnd() {
		if (currentDirectoryElement instanceof HelperImage) {
		
			String filePath = ((HelperImage)currentDirectoryElement).getVideoFilePath();
			filePath = filePath.replace('\\', '/');
		
			File videoFile = new File(Environment.getExternalStorageDirectory().getPath() + 
				getString(R.string.root_path) +
				config.getCurrentScenarioFile() + filePath);
		
			Log.d(TAG, "Video: " + videoFile.getPath());
			videoController.play(videoFile);
		}
	}

	@Override
	protected void setupDrawer() {
		mMenuNavigation.setVisibility(View.VISIBLE);
		mMenuNavigation.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				soundMenuButton.start();

				if(mNavigation.getVisibility()==View.VISIBLE) {
					mNavigation.setVisibility(View.INVISIBLE);
				} else {
					mNavigation.setVisibility(View.VISIBLE);
				}

				mDrawer.close();
			}
		});
	}

	@Override
	protected void setPageAt(int position) {
		super.setPageAt(position);
		config.setHelperImageIndex(position);
		
		// Logic for scaling some image in Helper.
		if (currentDirectoryElement instanceof HelperImage) {
			HelperImage helperElement = (HelperImage)currentDirectoryElement;

			/*if (helperElement.isScaled()) {
				Toast.makeText(this, getResources().getString(R.string.helper_info), Toast.LENGTH_SHORT).show();
				pageImage.setOnLongClickListener(pageImageLongClickListener);
			} else 
				pageImage.setOnLongClickListener(null);*/
		}
	}
    
	private OnLongClickListener pageImageLongClickListener = new OnLongClickListener() {
		public boolean onLongClick(View v) {
			HelperImage element = (HelperImage)currentDirectoryElement;

			Intent intent = new Intent(ActivityHelper.this, ScaleActivity.class);
			intent.putExtra("ImagePath", element.getPath());
			startActivity(intent);

			return false;
		}
	};

	@Override
	public void onBackPressed() {
		startActivity(new Intent(ActivityHelper.this, ActivityHelperDashboard.class));
		finish();
	}
}
