package ru.bigbears.wiserabbit.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import ru.bigbears.wiserabbit.Config;
import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.dialogs.DialogExit;

public class PreStartActivity extends AppCompatActivity {
    private static final String TAG = "My";

    private static final String PAK_EXTENSION = "pak";

    EditText editTextCode;

    File mPakDirectory;

    File[] mFilesInPakDirectory;

    MediaPlayer soundStartApp;
    String downlCode;


    public static final int RELOAD=1;

    public static final int DIALOG_DOWNLOAD_PROGRESS = 0;
    private ProgressDialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*requestWindowFeature(Window.FEATURE_NO_TITLE);*/
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pre_start);

        //CleanService.setServiceAlarm(this, true);

        int reqTask=getIntent().getIntExtra("REQ",0);

        soundStartApp = MediaPlayer.create(getBaseContext(), R.raw.app_start);
        editTextCode = (EditText) findViewById(R.id.edittext_code);
        if(reqTask==RELOAD){
            Config config=new Config(this);
            editTextCode.setText(config.getDownloadCode());
        }//else editTextCode.setText("2kndC8");

        Log.e(TAG, (Environment.getExternalStorageDirectory().toString() + getResources().getString(R.string.pak_path)));
        // get directory where will replacing pak file
        mPakDirectory = new File(Environment.getExternalStorageDirectory().toString() + getResources().getString(R.string.pak_path));
        if (!mPakDirectory.exists())
            mPakDirectory.mkdir();

        mFilesInPakDirectory = mPakDirectory.listFiles();

        if (mFilesInPakDirectory.length == 1) {
            showMainActivity();
        } else if (mFilesInPakDirectory.length > 1) {
            Toast.makeText(PreStartActivity.this, getResources().getString(R.string.error_many_files_in_directory),
                    Toast.LENGTH_SHORT).show();
            return;
        }else {
            Config mConfig=new Config(this);
            if(new File(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() + File.separator +
                    mConfig.getCurrentScenarioFile() + ".xml").exists()) {

                // Start browsing notepad content
                Intent intent = new Intent(PreStartActivity.this, ActivityNotepad.class);

                //startService(new Intent(this, TimerService.class));

                startActivity(intent);
                finish();
            }
        }
    }

    private void showMainActivity() {

        Intent intent = new Intent(this, MainActivity.class);
        //startService(new Intent(this, TimerService.class));
        startActivity(intent);
        finish();

    }

    public void buttonsClick(View v) {
        switch (v.getId()) {
            case R.id.button_get_pak:
                // checking for pak file correct

                if (mFilesInPakDirectory.length > 1) {
                    // many files in directory, must be one
                    Toast.makeText(PreStartActivity.this, getResources().getString(R.string.error_many_files_in_directory),
                            Toast.LENGTH_SHORT).show();
                    return;
                } else if (mFilesInPakDirectory.length == 0) {
                    // file not found
                /*Toast.makeText(MainActivity.this, getResources().getString(R.string.error_file_not_found),
	        			Toast.LENGTH_SHORT).show();
	        	return;*/
                    downlCode = editTextCode.getText().toString().trim();
                    downloadFromFTP();
                } else {
                    showMainActivity();
                }
                break;

            default:
                break;
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG_DOWNLOAD_PROGRESS:
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage("Downloading file..");
                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
                return mProgressDialog;
            default:
                return null;
        }
    }

    @Override
    public void onBackPressed() {
        DialogExit.show(this);
    }

    //download file from FTP server
    private void downloadFromFTP() {
        new AsyncTask<String, Void, Void>() {
            private ProgressDialog progressDialog;

            boolean check = false;

            @Override
            protected void onPreExecute() {
                showDialog(DIALOG_DOWNLOAD_PROGRESS);
            }

            @Override
            protected Void doInBackground(String... params) {

                String urlDownload = "http://wr-tour.ru/server/ajax.php?download&code=" + downlCode;
                String saveFile = Environment.getExternalStorageDirectory().toString() + getResources().getString(R.string.pak_path) + downlCode + ".pak";
                File downFile = new File(saveFile);

                int count;

                try {
                    URL url = new URL(urlDownload);
                    HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                    conexion.setReadTimeout(10*60*1000);
                    conexion.setConnectTimeout(10*60*1000);
                    conexion.connect();

                    saveFile = Environment.getExternalStorageDirectory().toString() + getResources().getString(R.string.pak_path) + conexion.getHeaderField("File-Name");

                    System.out.println(saveFile);
                    //System.out.println(conexion.);

                    int lenghtOfFile = conexion.getContentLength();
                    Log.e(TAG, "len: " + lenghtOfFile);
                    //Log.d("ANDRO_ASYNC", "Lenght of file: " + lenghtOfFile);

                    InputStream input = url.openStream(); //new BufferedInputStream(url.openStream());
                    OutputStream output = new FileOutputStream(saveFile);

                    byte data[] = new byte[4096];

                    long total = 0;

                    while ((count = input.read(data)) != -1) {
                        total += count;
                        //publishProgress();
                        onProgressUpdate(String.valueOf((int) (total * 100 / lenghtOfFile)));
                        output.write(data, 0, count);
                        output.flush();
                    }

                    Log.e("Download Finished",""+total);
                    output.close();
                    input.close();

                    check = true;
                } catch (Exception e) {
                    check = false;
                    Log.e("Download interrupted",e.getMessage());
                   // downFile.delete();
                    //Toast.makeText(PreStartActivity.this,"Файл не удалось загрузить",Toast.LENGTH_SHORT).show();
                    Log.d("DOWNLOAD GET","Файл не удалось загрузить");
                }

              /*  String urlResponse = "http://wr-tour.ru/server/ajax.php?download_status=200&code=" + downlCode;
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet(urlResponse);

                if (check) {
                    try {
                        Log.d("POST DOWNLOAD GET", "Запрос отправлен");
                        HttpResponse response = httpclient.execute(httpget);
                        HttpEntity httpEntity = response.getEntity();
                        String line = EntityUtils.toString(httpEntity, "UTF-8");
                        Log.d("POST DOWNLOAD GET", line);
                    } catch (ClientProtocolException e) {
                        Log.d("POST DOWNLOAD GET", "Ошибочка");
                    } catch (IOException e) {
                        Log.d("POST DOWNLOAD GET", "Запрос не отправлен");
                    }
                }*/

                return null;
            }

            protected void onProgressUpdate(String... progress) {
                Log.d("ANDRO_ASYNC",progress[0]);
                mProgressDialog.setProgress(Integer.parseInt(progress[0]));
            }

            @Override
            protected void onPostExecute(Void file) {
                dismissDialog(DIALOG_DOWNLOAD_PROGRESS);
                if (check) {

                    Config config=new Config(PreStartActivity.this);
                    config.setDownloadCode(downlCode);
                    showMainActivity();
                }
            }
        }.execute();

    }
}
