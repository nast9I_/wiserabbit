package ru.bigbears.wiserabbit.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import ru.bigbears.wiserabbit.Config;
import ru.bigbears.wiserabbit.Decompress;
import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.XmlParser;
import ru.bigbears.wiserabbit.consts.ScenarioStructure;
import ru.bigbears.wiserabbit.database.Database;
import ru.bigbears.wiserabbit.dialogs.DialogExit;
import ru.bigbears.wiserabbit.dialogs.InputPasswordDialog;
import ru.bigbears.wiserabbit.dialogs.LicenseDialog;
import ru.bigbears.wiserabbit.dialogs.UserDataDialog;
import ru.bigbears.wiserabbit.models.RemovedDirectory;
import ru.bigbears.wiserabbit.services.CleanService;
import ru.bigbears.wiserabbit.services.TimerService;

public class MainActivity extends FragmentActivity {
    private static final String TAG = "My";

    private static final String PAK_EXTENSION = "pak";

    Config mConfig;

    EditText editTextCode;

    TextView gameName;

    XmlParser mParser;
    File mPakDirectory;

    File[] mFilesInPakDirectory;

    MediaPlayer soundStartApp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        CleanService.setServiceAlarm(this, true);

        soundStartApp = MediaPlayer.create(getBaseContext(), R.raw.app_start);

        editTextCode = (EditText)findViewById(R.id.edittext_code);
        gameName = (TextView)findViewById(R.id.gameCode);

        // get directory where will replacing pak file
        mPakDirectory = new File(Environment.getExternalStorageDirectory().toString() + getResources().getString(R.string.pak_path));
        if(!mPakDirectory.exists())
            mPakDirectory.mkdir();

        //mPakDirectory.setReadable(false);

        String appDirectory = Environment.getExternalStorageDirectory().getPath() +
                getResources().getString(R.string.root_path);
        File appDir = new File(appDirectory);
        appDir.mkdirs();
        appDir.setReadable(false);
        appDir.setWritable(false);
        appDir.setExecutable(false);

        mFilesInPakDirectory = mPakDirectory.listFiles();

        gameName.setText(mFilesInPakDirectory[0].getName().substring(0,mFilesInPakDirectory[0].getName().length()-4));
            //gameName.setText((int) mFilesInPakDirectory[0].length());


        // Get config from preference.
        // If information about current scenario file is absent in config
        // then just return and continue app working.
        mConfig = new Config(MainActivity.this);
        if (mConfig.getCurrentScenarioFile().equals(Config.EMPTY_STRING_VALUE))
            return;

        // Check for new pak file in pak directory..
        // If pak file name in pak directory not equals to saved scenario file
        // then just return. There is new scenario file in pak directory and we must start new session.
        if (mFilesInPakDirectory.length == 1) {
            Log.d(TAG, "Check for new PAK success");

            String fileName = mFilesInPakDirectory[0].getName().replace(".pak", "");
            Log.d(TAG, "File in PAK directory name: " + fileName);
            Log.d(TAG, "Current saved xml name: " + mConfig.getCurrentScenarioFile());

            // If name in config and pak name are different then we must reset
            // config for new pak file...
            if (!fileName.equals(mConfig.getCurrentScenarioFile())) {
                mConfig.reset();

                return;
            }
        } else if (mFilesInPakDirectory.length > 1) {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.error_many_files_in_directory),
                    Toast.LENGTH_SHORT).show();
            return;
        }

        mParser = new XmlParser(this, mConfig, Environment.getExternalStorageDirectory().getPath() +
                getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() + File.separator +
                mConfig.getCurrentScenarioFile() + ".xml");

        // This code using for forward starting the app if exist current scenario.
        // Also this using for delete content when remove date is coming.
        if(mParser.isXmlExist()) {
            File scenarioDirectory = new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.SCENARIO_DIRECTORY);

            if(deletePakIfDateIsComing()) {
                showUnremovedContent();
                return;
            }

            if (!scenarioDirectory.exists()) {
                showUnremovedContent();
                return;
            } else if (scenarioDirectory.listFiles().length == 0) {
                showUnremovedContent();
                return;
            }

            // Start browsing notepad content
            Intent intent = new Intent(MainActivity.this, ActivityNotepad.class);

            //startService(new Intent(this, TimerService.class));

            startActivity(intent);
            finish();
        }
    }

    private void showUnremovedContent() {
        Intent intent = new Intent(this, PreStartActivity.class);
        startActivity(intent);
        finish();
    }

    public void buttonsClick(View v) {
        switch (v.getId()) {
            case R.id.button_start:
                // checking for pak file correct

                if (mFilesInPakDirectory.length > 1) {
                    // many files in directory, must be one
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.error_many_files_in_directory),
                            Toast.LENGTH_SHORT).show();
                    return;
                } else if (mFilesInPakDirectory.length == 0) {
                    // file not found
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.error_file_not_found),
                            Toast.LENGTH_SHORT).show();
                    return;
                    //downloadFromFTP();
                } else {

                    // get file extension and check this on correct
                    String fileExtension = mFilesInPakDirectory[0].getName().substring(mFilesInPakDirectory[0].getName().length() - 3);
                    if (!fileExtension.equals(PAK_EXTENSION)) {
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.error_file_not_found),
                                Toast.LENGTH_SHORT).show();
                        return;

                    } else {
                        final InputPasswordDialog passDialog = new InputPasswordDialog();
                        passDialog.show(getSupportFragmentManager(),InputPasswordDialog.TAG);

                        passDialog.setOnPasswordInputed(new InputPasswordDialog.OnPasswordInputedListener() {
                            public void onPasswordInputed(String password) {
                                if (password.equals("0123")) {
                                    final LicenseDialog licenseDialog = new LicenseDialog();
                                    licenseDialog.show(getSupportFragmentManager(), LicenseDialog.TAG);

                                    licenseDialog.setOnLicenseInputed(new LicenseDialog.OnLicenseListener() {
                                        public void onLicenseInputed() {
                                            final UserDataDialog dialog = new UserDataDialog();
                                            dialog.show(getSupportFragmentManager(), UserDataDialog.TAG);
                                            dialog.setOnUserDataInputed(new UserDataDialog.OnUserDataInputedListener() {

                                                public void onUserDataInputed(String email, String login) {
                                                    mConfig.setUserEmail(email);
                                                    mConfig.setUserLogin(login);
                                                    decompressPak();
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    Toast.makeText(MainActivity.this,"Вы ввели неверный код!!!",Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                    }
                }

                // if all checks are successes then decompress pak-file
                break;

            case R.id.button_del:
                InputPasswordDialog dialog = new InputPasswordDialog();
                dialog.show(getSupportFragmentManager(), InputPasswordDialog.TAG);
                dialog.setOnPasswordInputed(new InputPasswordDialog.OnPasswordInputedListener() {

                    public void onPasswordInputed(String password) {
                        if (password.equals("00000")) {
                            mFilesInPakDirectory[0].delete();
                            String pathPackDir = Environment.getExternalStorageDirectory().getPath() +
                                    getResources().getString(R.string.root_path);
                            File packDir = new File(pathPackDir);
                            packDir.delete();
                            packDir.mkdirs();
                            Toast.makeText(MainActivity.this,
                                    "Пакет успешно удален...", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(MainActivity.this,PreStartActivity.class));
                            finish();
                        } else {
                            Toast.makeText(MainActivity.this,
                                    "Неверный пароль для удаления...", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                });
                break;

            default:
                break;
        }
    }

    private void decompressPak() {
        new AsyncTask<String, Integer, File>() {
            private ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = new ProgressDialog(MainActivity.this);

                progressDialog.setMessage(getResources().getString(R.string.unpacking));
                progressDialog.setCancelable(false);

                progressDialog.show();

                // Delete previous undeletable content
                deleteDirectory(new File(Environment.getExternalStorageDirectory().getPath() +
                        getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile()));
            }

            @Override
            protected File doInBackground(String... params) {
                Log.d("PAK",Environment.getExternalStorageDirectory().getPath() +
                        getResources().getString(R.string.pak_path) + mFilesInPakDirectory[0].getName());

                Decompress.unzipFile(Environment.getExternalStorageDirectory().getPath() +
                                getResources().getString(R.string.pak_path) + mFilesInPakDirectory[0].getName(),
                        Environment.getExternalStorageDirectory().getPath() +
                                getResources().getString(R.string.root_path) + mFilesInPakDirectory[0].getName().replace(".pak", "/"));

                mConfig.setCode(mFilesInPakDirectory[0].getName().replace(".pak", "/"));

                return null;
            }

            @Override
            protected void onPostExecute(File file) {
                progressDialog.dismiss();

                // save current scenario file name
                mConfig.setCurrentScenarioFile(mFilesInPakDirectory[0].getName().replace(".pak", ""));

                // create file for unpacked folder and get xml file from this folder
                File unpackFolder = new File(Environment.getExternalStorageDirectory().getPath() +
                        getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile());
                String xmlPath = unpackFolder.getPath() + File.separator + mConfig.getCurrentScenarioFile() + ".xml";
                File xmlFile = new File(xmlPath);
                Log.d(TAG, "XML path: " + xmlPath);
                Log.d(TAG, "XML file exists: " + xmlFile.exists());

                mParser = new XmlParser(MainActivity.this, mConfig, xmlPath);

                // check password
                //if(editTextCode.getText().toString().equals(mParser.getPassword())) {
                if (true) {
                    soundStartApp.start();

                    //mConfig.setFilePass(mParser.getPassword());
                    mConfig.setFilePass("");

                    // TODO: add directory name here at the database.
                    int hoursToRemove = mParser.getHoursToRemove();
                    if (hoursToRemove > 0) {
                        DateTime removeDate = DateTime.now().plusHours(hoursToRemove);
                        RemovedDirectory directory = new RemovedDirectory(unpackFolder.getPath(), removeDate);
                        Database.getInstance(MainActivity.this).addDirectory(directory);
                        Database.getInstance(MainActivity.this).close();
                    }

                    Intent intent = new Intent(MainActivity.this, ActivityNotepad.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(MainActivity.this, getResources().getString(R.string.error_invalid_password),
                            Toast.LENGTH_SHORT).show();
                    deleteDirectory(unpackFolder);

                    return;
                }

                LocalDateTime start = LocalDateTime.now();
                mConfig.setStartDateTime(start);

                int endHours = mParser.getHoursForGame();

                LocalDateTime end = start.plusHours(endHours);
                mConfig.setEndDateTime(end);
            }
        }.execute();
    }

    private boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();

            if (files == null) {
                return true;
            }
            Log.d(TAG, "In delete directory: " + files.length + " files");

            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    Log.d(TAG, "  Delete directory: " + files[i].getName());
                    deleteDirectory(files[i]);
                } else {
                    Log.d(TAG, "  Delete file: " + files[i].getName());
                    files[i].delete();
                }
            }
        } else {
            Log.d(TAG, "Delete path not exist: " + path.getPath());
        }
        return (path.delete());
    }

    private boolean deletePakIfDateIsComing() {
        boolean isScenarioDeleted = false;

        String removeDate = mParser.getRemoveDate();
        if (removeDate == null)
            return false;

        Date currentDate =  new Date();
        Date deleteDate = null;

        SimpleDateFormat textFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            deleteDate = textFormat.parse(removeDate);
        } catch (ParseException e) {
            return false;
        }

        // if delete date is coming
        if(currentDate.after(deleteDate)) {
            isScenarioDeleted = true;

            // delete scenario content
            deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.SCENARIO_DIRECTORY));
            deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.HELPER_DIRECTORY));
            deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.REFERENCE_DIRECTORY));
            deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.HINT_DIRECTORY));
            deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.FAIL_EXIT_DIRECTORY));
            deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.KEYBOARD_DIRECTORY));

            // get Fotopad directory and clear
            File fotoPadDir = new File(Environment.getExternalStorageDirectory().getPath() + "/.FotoPad");
            File[] files = fotoPadDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }

            Log.d("DELETE","From MainActivity");

            sendGameInfo();

            mConfig.delPak();
        }

        return isScenarioDeleted;
    }

    @Override
    public void onBackPressed() {
        DialogExit.show(this);
    }

    private boolean timerToEnd() {
        boolean isScenarioDeleted = false;

        LocalDateTime startDateTime = LocalDateTime.now();
        DateTime endDateTime;
        String removeDate = mParser.getRemoveDate();
        if (removeDate == null)
            return false;

        Date currentDate =  new Date();
        Date deleteDate = null;

        SimpleDateFormat textFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            deleteDate = textFormat.parse(removeDate);
        } catch (ParseException e) {
            return false;
        }

        // if delete date is coming
        if(currentDate.after(deleteDate)) {
            isScenarioDeleted = true;

            // delete scenario content
            deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.SCENARIO_DIRECTORY));
            deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.HELPER_DIRECTORY));
            deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.REFERENCE_DIRECTORY));
            deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.HINT_DIRECTORY));
            deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.FAIL_EXIT_DIRECTORY));
            deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                    getResources().getString(R.string.root_path) + mConfig.getCurrentScenarioFile() +
                    File.separator + ScenarioStructure.KEYBOARD_DIRECTORY));

            // get Fotopad directory and clear
            File fotoPadDir = new File(Environment.getExternalStorageDirectory().getPath() + "/.FotoPad");
            File[] files = fotoPadDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }

        return isScenarioDeleted;
    }

    public void sendGameInfo() {
        String urlResponse = "http://wr-tour.ru/server/ajax.php?set_statistic&code_value=" + mConfig.getCode() +
                "&code_time_of_game=null" +
                "&code_marks=" + mConfig.getScore() + "&code_player_email=" + mConfig.getUserEmail() +
                "&code_player_nickname=" + mConfig.getUserLogin();
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(urlResponse);

        try {
            Log.d("POST DOWNLOAD GAMEINFO", "Запрос отправлен");
            HttpResponse response = httpclient.execute(httpget);
            HttpEntity httpEntity = response.getEntity();
            String line = EntityUtils.toString(httpEntity, "UTF-8");
            Log.d("POST DOWNLOAD GAMEINFO", line);
        } catch (ClientProtocolException e) {
            Log.d("POST DOWNLOAD GAMEINFO", "Ошибочка");
        } catch (IOException e) {
            Log.d("POST DOWNLOAD GAMEINFO", "Запрос не отправлен");
        }

    }
}
