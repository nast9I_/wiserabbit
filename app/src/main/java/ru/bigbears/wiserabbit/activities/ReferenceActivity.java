package ru.bigbears.wiserabbit.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;

import ru.bigbears.wiserabbit.Config;
import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.Utils;
import ru.bigbears.wiserabbit.VideoController;
import ru.bigbears.wiserabbit.dialogs.DialogExit;
import ru.bigbears.wiserabbit.models.DirectoryElement;
import ru.bigbears.wiserabbit.models.HelperDirectory;
import ru.bigbears.wiserabbit.models.HelperImage;
import ru.bigbears.wiserabbit.models.ReferenceDirectory;
import ru.bigbears.wiserabbit.models.ScenarioDirectory;
import ru.bigbears.wiserabbit.models.ReferenceImage;
import ru.bigbears.wiserabbit.models.ScenarioImage;

public class ReferenceActivity extends ActivityBase {

    public static final String INTENT_PARAM_OPENED_IMAGE_PATH = "OpenedImagePath";
    public static final String INTENT_PARAM_REFERENCE_VIDEO_FILE_PATH = "VideoFilePath";
    public static final String INTENT_PARAM_REFERENCE_FILE_PATH = "ReferenceFilePath";
    public static final String INTENT_PARAM_REFERENCE_PATH = "ReferencePath";

    private ImageView referenceImage;
    private Config config;

    private MediaPlayer soundYesNo;

    private static final String TAG = "My";

    protected VideoController videoController;

    private ArrayList<ReferenceDirectory> mReferenceDirectories;

    int mReferenceDirectoryIndex;

    private MediaPlayer soundOpenHint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        soundYesNo = MediaPlayer.create(this, R.raw.yes_no);
        soundYesNo.setVolume(0.7f, 0.7f);

            initUI();

        mFlipper.setCurrentIndex(0);
    }

    @Override
    protected void initUI() {
        super.initUI();

        config = new Config(this);

        menuButton.setVisibility(View.GONE);
        //notepadReferenceButton.setVisibility(View.VISIBLE);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        videoController = new VideoController(this, metrics);

		/*Button refer = (Button) findViewById(R.id.btnRefer);
		refer.setVisibility(View.VISIBLE);*/

        //mReferenceDirectoryIndex = 0;

        // set notepad name from xml file
        String referTitle = xmlParser.getReferenceName();
        if(referTitle != null && !referTitle.equals(""))
            setTitle(referTitle);
        else
            setTitle(R.string.title_notepad);

        // Add images from directories that was opened by player previously
        mReferenceDirectories = xmlParser.getReferenceDirectories();
        //mReferenceDirectories = xmlParser.getReferenceDirectoryCodes();

        // Now me must to check next directories for 'opening code'. If code is not
        // available then we should add images from not-closed directory too...

        String path = getIntent().getStringExtra(ReferenceActivity.INTENT_PARAM_OPENED_IMAGE_PATH);
        for (int i=0; i != mReferenceDirectories.size(); ++i) {
            if (mReferenceDirectories.get(i).getImage(0).getPath().equalsIgnoreCase(path)) {
                mReferenceDirectoryIndex = i;
                break;
            }
        }

        //Log.d("LOG ARTEM REFERENCE: ", String.valueOf(mReferenceDirectoryIndex));
        //Log.d("LOG ARTEM REFERENCE: ",mReferenceDirectories.get(mReferenceDirectoryIndex).getImage(0).getPath());
            ReferenceDirectory directory = mReferenceDirectories.get(mReferenceDirectoryIndex);

        Log.e("LOG CODE REFERENCE ", directory.getCode());
        try {
            Log.e("LOG VALID REFERENCE ", directory.getMessageValidCode());

        } catch (Exception ex) {}


            // if a code for directory is not exists, add images at the navigation strip
            if (directory.getCode() == null || directory.getCode().equals("")) {

                navigatorAdapter.addImages(directory.getImages());
                navigatorAdapter.notifyDataSetChanged();
            } else {
                navigatorAdapter.addImages(directory.getImages());
                navigatorAdapter.notifyDataSetChanged();
            }


        navigatorAdapter.notifyDataSetChanged();

        String videoFilePath = getIntent().getStringExtra(INTENT_PARAM_REFERENCE_VIDEO_FILE_PATH);
        if (videoFilePath == null) {
            videoController.setControlPanelVisibility(View.INVISIBLE);
        } else if (videoFilePath.length() == 0) {
            videoController.setControlPanelVisibility(View.INVISIBLE);
        }
/*
        referenceImage = (ImageView) findViewById(R.id.pageImage);

        String referImagePath = getIntent().getStringExtra(INTENT_PARAM_OPENED_IMAGE_PATH);

        referImagePath = Environment.getExternalStorageDirectory().getPath() +
                getResources().getString(R.string.root_path) +
                config.getCurrentReferenceFile() + referImagePath;
        referImagePath = referImagePath.replace('\\', '/');
        Log.d(TAG, "Reference image path: " + referImagePath);

        Drawable d = BitmapDrawable.createFromPath(referImagePath);

        referenceImage.setBackgroundDrawable(d);*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data == null) {
            Log.d(TAG, "Result: data is null");
            return;
        }

        if (resultCode == 1) {
            if(data.getBooleanExtra(KeyActivity.EXTRA_IS_CODE_VALID, false)) {
                Log.d(TAG, "Result: good key get!!!");
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    protected void handleButtonHintPressed() {
        ReferenceImage image = (ReferenceImage)currentDirectoryElement;
        try {
            String tipPath = image.getTipPath();
            if (tipPath != null && tipPath.length() != 0) {
                tipPath = tipPath.replace('\\', '/');
                File tipFile = new File(Environment.getExternalStorageDirectory().getPath() +
                        getResources().getString(R.string.root_path) +
                        config.getCurrentReferenceFile() + tipPath);
                if (!tipFile.exists()) {
                    return;
                }
            } else {
                return;
            }
        } catch (Exception e) {
            return;
        }

        if (!config.isReferHintUse(mReferenceDirectoryIndex)) {
            showHintDialog(image.getTipMarks(), image.getTipDescription());
        } else {
            Intent intent = new Intent(ReferenceActivity.this, HintActivity.class);
            intent.putExtra(HintActivity.INTENT_PARAM_OPENED_IMAGE_PATH, image.getTipPath());
            config.setMenuVisible(false);

            /*if (image.getTipVideoPath() != null) {
                intent.putExtra(HintActivity.INTENT_PARAM_TIP_VIDEO_FILE_PATH, image.getTipVideoPath());
            }*/

            startActivity(intent);
        }
    }

    @Override
    protected void handleButtonReferencePressed() {
    }

    @Override
    protected void handleOnVideoViewAnimationEnd() {
        if (currentDirectoryElement instanceof ReferenceImage) {

            String filePath = ((ReferenceImage)currentDirectoryElement).getVideoPath();
            filePath = filePath.replace('\\', '/');

            File videoFile = new File(Environment.getExternalStorageDirectory().getPath() +
                    getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + filePath);

            Log.d(TAG, "Video: " + videoFile.getPath());
            videoController.play(videoFile);
        }
    }

    @Override
    protected void setupDrawer() {}


    private void showHintDialog(int marksBan, String tipDescription) {
        final PopupWindow popup = new PopupWindow(this);

        LayoutInflater inflater = getLayoutInflater();
        View content = inflater.inflate(R.layout.dialog_open_hint, null);

        TextView tv = (TextView) content.findViewById(R.id.hint_desc);

        // ... and show them
        tv.setText(String.format(getResources().getString(R.string.hint_desc), marksBan));
        tv.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));

        tv = (TextView) content.findViewById(R.id.hint_title);
        tv.setTypeface(Utils.getTypeface(this, Utils.Font.PROTO_SANS));

        Button yes = (Button)content.findViewById(R.id.btnYes);
        yes.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
        yes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                soundYesNo.start();

                config.useReferHint(mReferenceDirectoryIndex);

                ReferenceImage image = (ReferenceImage)currentDirectoryElement;

                // write tip count value...
                config.setScoreBunned(config.getScoreBunned() + image.getTipMarks());

                Intent intent = new Intent(ReferenceActivity.this, HintActivity.class);
                intent.putExtra(HintActivity.INTENT_PARAM_OPENED_IMAGE_PATH, image.getTipPath());
                startActivity(intent);

                popup.dismiss();
            }
        });

        Button no = (Button)content.findViewById(R.id.btnNo);
        no.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
        no.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                soundYesNo.start();

                popup.dismiss();
            }
        });

        popup.setContentView(content);

        float density = getResources().getDisplayMetrics().density;
        popup.setWidth( (int)(getResources().getDimensionPixelSize(R.dimen.dialog_width_px)*density) );
        popup.setHeight( (int)(getResources().getDimensionPixelSize(R.dimen.dialog_height_px)*density) );

        popup.setOutsideTouchable(false);
        popup.setTouchable(true);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(getResources().getDrawable(R.drawable.menu));

        RelativeLayout layout = (RelativeLayout)findViewById(R.id.main);
        popup.showAtLocation(layout, Gravity.CENTER_VERTICAL, 0, 0);
    }
}
