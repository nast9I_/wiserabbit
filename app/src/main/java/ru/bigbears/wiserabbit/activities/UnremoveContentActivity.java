/**
 * 
 */
package ru.bigbears.wiserabbit.activities;

import java.io.File;
import ru.bigbears.wiserabbit.Config;
import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.VideoController;
import ru.bigbears.wiserabbit.XmlParser;
import ru.bigbears.wiserabbit.models.UnremoveDirectory;
import ru.bigbears.wiserabbit.models.UnremoveDirectoryImage;
import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.MotionEvent;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * @author Vladimir
 *
 */
public class UnremoveContentActivity extends Activity implements OnClickListener {
	private static final String TAG = "My";
	
	private ImageView mAnimationImage;
	private ImageView mPageImage;
	
	private Animation mAnimationPreviousNew;
	private Animation mAnimationPreviousOld;
	private Animation mAnimationNextNew;
	private Animation mAnimationNextOld;
	
	private Button mLeftButton;
	private Button mRightButton;
	
	private VideoController videoController;
	
	private MediaPlayer mTurnPageSound;
	
	private UnremoveDirectory unremoveDirectory;
	private XmlParser mParser;
	
	// Index for page. Use for controlling current opened page from 
	// Unremove directory.
	private int mOpenedPageIndex = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_unremove_content);
        
        initUI();
        
        mTurnPageSound = MediaPlayer.create(getBaseContext(), R.raw.paper);
        
        mPageImage.setOnTouchListener(mPageTouchListener);
        
        mLeftButton.setOnClickListener(this);
        mRightButton.setOnClickListener(this);
        
        videoController.setOnClickListener(this);
        videoController.setOnCompletionListener(mVideoCompletionListener);
        videoController.setAnimationListener(mVideoViewAnimationListener);
        
        Config config = new Config(this);
        
        mParser = new XmlParser(this, config, Environment.getExternalStorageDirectory().getPath() + getResources().getString(R.string.root_path) + 
        		config.getCurrentScenarioFile() + File.separator + 
                config.getCurrentScenarioFile() + ".xml");
        
        if (mParser.isXmlExist()) {
        	// if pak not contain unremove content then we must show toast with information
        	// and disabled UI elements
        	if (mParser.getUnremoveImages() == null) {
        		Toast.makeText(this, "Сценарий окончен.", Toast.LENGTH_LONG).show();
        		
        		videoController.setControlPanelVisibility(View.INVISIBLE);
        		mPageImage.setOnTouchListener(null);
        		
        		return;
        	}
        		
        	unremoveDirectory = new UnremoveDirectory(mParser.getUnremoveImages());
        	Log.d(TAG, "In unremove directory: " + unremoveDirectory.getCount() + " images");
        	
        	for (int i = 0; i != unremoveDirectory.getCount(); ++i) 
        		Log.d(TAG, "  Image: " + unremoveDirectory.getImageAt(i).getPath());
        } else {
        	Log.e(TAG, "Error: xml file not exist. Unremove images not load.");
        	return;
        }
        
        // Show first (with 0 index) page from Unremove directory...
        if (unremoveDirectory.getCount() != 0)
        	showDrawableAtImageView(mPageImage, mOpenedPageIndex);
	}

	private void initUI() {
		mAnimationImage = (ImageView)findViewById(R.id.imgAnimationImage);
		mPageImage = (ImageView)findViewById(R.id.imgPage);
		
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		videoController = new VideoController(this, metrics);
		
		mLeftButton = (Button)findViewById(R.id.btnLeft);
		mRightButton = (Button)findViewById(R.id.btnRight);
		
		mAnimationPreviousNew = AnimationUtils.loadAnimation(this, R.anim.anim_prev_new);
        mAnimationPreviousOld = AnimationUtils.loadAnimation(this, R.anim.anim_prew_old);
        mAnimationNextNew = AnimationUtils.loadAnimation(this, R.anim.anim_next_new);
        mAnimationNextOld = AnimationUtils.loadAnimation(this, R.anim.anim_next_old);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnLeft:
			if (mOpenedPageIndex > 0) {
				mTurnPageSound.start();
			
				--mOpenedPageIndex;
				
				// Check for showing button logic
				if (mOpenedPageIndex == 0) 
					mLeftButton.setVisibility(View.INVISIBLE);
				if (mOpenedPageIndex != unremoveDirectory.getCount() - 1)
					mRightButton.setVisibility(View.VISIBLE);
			
				showDrawableAtImageView(mPageImage, mOpenedPageIndex);
				showDrawableAtImageView(mAnimationImage, mOpenedPageIndex + 1);
				mPageImage.startAnimation(mAnimationPreviousNew);
				mAnimationImage.startAnimation(mAnimationPreviousOld);
			}
			
			break;
			
		case R.id.btnRight:
			if (mOpenedPageIndex < unremoveDirectory.getCount() - 1) {
				mTurnPageSound.start();
			
				++mOpenedPageIndex;
				if (mOpenedPageIndex != 0)
					mLeftButton.setVisibility(View.VISIBLE);
				
				if (mOpenedPageIndex == unremoveDirectory.getCount() - 1)
					mRightButton.setVisibility(View.INVISIBLE);
			
				showDrawableAtImageView(mPageImage, mOpenedPageIndex);
				showDrawableAtImageView(mAnimationImage, mOpenedPageIndex - 1);
				mPageImage.startAnimation(mAnimationNextNew);
				mAnimationImage.startAnimation(mAnimationNextOld);
			}
			
			break;
			
		case R.id.btnPlayVideo:
			videoController.handlePlayVideoButton();
			break;
			
		case R.id.btnToggleScreenMode:
			videoController.handleToggleScreenModeButton();
			break;

		default:
			break;
		}
	}
	
	/**
	 * Show image at ImageView by index. 
	 * @param imageView - ImageView for showing
	 * @param index - index of showing image
	 */
	private void showDrawableAtImageView(ImageView imageView, int index) {
		Config config = new Config(this);

		UnremoveDirectoryImage image = unremoveDirectory.getImageAt(index);
	    
	    Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
	    		getResources().getString(R.string.root_path) +
	            config.getCurrentScenarioFile() + image.getPath().replace('\\', '/'));
	    imageView.setBackgroundDrawable(d);
	    
	    if (imageView == mPageImage) {
	    	if (image.getVideoFilePath() != null)
	    		if (!image.getVideoFilePath().equals(""))
	    			videoController.setControlPanelVisibility(View.VISIBLE);
	    		else
	    			videoController.setControlPanelVisibility(View.INVISIBLE);
	    	else 
	    		videoController.setControlPanelVisibility(View.INVISIBLE);
	    }
	}
	
	// Using for show/hide navigation buttons when user touch the screen
	private boolean mIsButtonsVisibled = true;
	
	private OnTouchListener mPageTouchListener = new OnTouchListener() {
		public boolean onTouch(View v, MotionEvent event) {
			if (videoController.getScreenVisibility() == View.GONE) {
				if (mIsButtonsVisibled) {

					if (mOpenedPageIndex != 0)
						mLeftButton.setVisibility(View.VISIBLE);

					if (mOpenedPageIndex != unremoveDirectory.getCount() - 1)
						mRightButton.setVisibility(View.VISIBLE);

					mIsButtonsVisibled = false;
				} else {
					mLeftButton.setVisibility(View.INVISIBLE);
					mRightButton.setVisibility(View.INVISIBLE);

					mIsButtonsVisibled = true;
				}
			}

			return false;
		}
	};
	
	private OnCompletionListener mVideoCompletionListener = new OnCompletionListener() {
		public void onCompletion(MediaPlayer mp) {
			videoController.handleOnCompletion();
		}
	};
	
	private AnimationListener mVideoViewAnimationListener = new AnimationListener() {
		public void onAnimationStart(Animation animation) {}
		public void onAnimationRepeat(Animation animation) {}
		
		public void onAnimationEnd(Animation animation) {
			Config config = new Config(UnremoveContentActivity.this);
			
			UnremoveDirectoryImage image = unremoveDirectory.getImageAt(mOpenedPageIndex);
			String videoPath = image.getVideoFilePath().replace('\\', '/');
			
			File videoFile = new File(Environment.getExternalStorageDirectory().getPath() + 
					getString(R.string.root_path) +
					config.getCurrentScenarioFile() + File.separator +
					videoPath);
			
			Log.d(TAG, "Video: " + videoFile.getPath());
			videoController.play(videoFile);
		}
	};
}
