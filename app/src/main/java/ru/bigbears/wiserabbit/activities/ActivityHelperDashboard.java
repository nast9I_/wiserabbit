package ru.bigbears.wiserabbit.activities;

import java.io.File;
import java.util.ArrayList;

import ru.bigbears.wiserabbit.Config;
import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.VideoController;
import ru.bigbears.wiserabbit.XmlParser;
import ru.bigbears.wiserabbit.adapters.HelperDashboardAdapter;
import ru.bigbears.wiserabbit.consts.ScenarioStructure;
import ru.bigbears.wiserabbit.dialogs.InputPasswordDialog;
import ru.bigbears.wiserabbit.dialogs.InputPasswordDialog.OnPasswordInputedListener;
import ru.bigbears.wiserabbit.models.HelperDirectory;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Environment;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class ActivityHelperDashboard extends FragmentActivity implements OnClickListener, AnimationListener {
	private static final String TAG = "My";
	
	private GridView dashboardGrid;
	private HelperDashboardAdapter dashboardAdapter;
	
	private XmlParser mParser;
	private Config config;
	
	private VideoController videoController;
	private String selectedVideoFileName;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_directory);

		findViewById(R.id.back).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				onBackPressed();
			}
		});

		config = new Config(ActivityHelperDashboard.this);
		
		mParser = new XmlParser(this, config, Environment.getExternalStorageDirectory().getPath() + 
				getResources().getString(R.string.root_path) + 
				config.getCurrentScenarioFile() + "/" + 
				config.getCurrentScenarioFile() + ".xml");
		
		initUI();
		
		dashboardGrid.setOnItemClickListener(dashboardGridItemClickListener);
		
		videoController.setOnClickListener(this);
		videoController.setAnimationListener(this);
		videoController.setOnCompletionListener(videoCompletionListener);
	}
	
	private void initUI() {
		dashboardGrid = (GridView) findViewById(R.id.GridView01);
		
		// read names from helper directory
		ArrayList<HelperDirectory> directoryNames = mParser.getHelperDirectoryNames();
		dashboardAdapter = new HelperDashboardAdapter(this, directoryNames);
		dashboardGrid.setAdapter(dashboardAdapter);
		
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		videoController = new VideoController(this, metrics);
		videoController.setControlPanelVisibility(View.INVISIBLE);
	}
	
	private OnItemClickListener dashboardGridItemClickListener = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> adapterView, View v, final int position, long id) {
			
			final HelperDirectory directory = (HelperDirectory) adapterView.getItemAtPosition(position);
			
			selectedVideoFileName = directory.getName();
			
			String extension = selectedVideoFileName.substring(selectedVideoFileName.length());
			if (extension.equals(".3gp") || extension.equals(".mp4")) 
				videoController.setControlPanelVisibility(View.VISIBLE);
			else {
				videoController.setControlPanelVisibility(View.INVISIBLE);
				
				if (directory.getPassword() != null) {
					
					if (!config.isPasswordUsedForHelperDirectory(directory.getName())) {
						startHelper(position);
						return;
					} else {
						InputPasswordDialog dialog = new InputPasswordDialog();
						dialog.show(getSupportFragmentManager(), InputPasswordDialog.TAG);
						dialog.setOnPasswordInputed(new OnPasswordInputedListener() {
							
							public void onPasswordInputed(String password) {
								if (password.equals(directory.getPassword())) {
									config.setPasswordStatusForHelperDirectory(directory.getName(), false);
									dashboardAdapter.notifyDataSetChanged();
									startHelper(position);
								} else {
									Toast.makeText(ActivityHelperDashboard.this, 
											R.string.password_wrong, Toast.LENGTH_SHORT).show();
									return;
								}
							}
						});
					}
				} else {
					startHelper(position);
				}
			}
		}
	};

	private void startHelper(int helperDirectoryIndexForLoadImages) {
		Intent intent = new Intent(ActivityHelperDashboard.this, ActivityHelper.class);
		intent.putExtra("index", helperDirectoryIndexForLoadImages);
		startActivity(intent);
		finish();
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnPlayVideo:
			videoController.handlePlayVideoButton();
			break;
			
		case R.id.btnToggleScreenMode:
			videoController.handleToggleScreenModeButton();
			break;

		default:
			break;
		}
	}
	
	public void onAnimationEnd(Animation animation) {
		String filePath = Environment.getExternalStorageDirectory().getPath() + 
				getString(R.string.root_path) +
				config.getCurrentScenarioFile() + File.separator + 
				ScenarioStructure.HELPER_DIRECTORY + File.separator +
				selectedVideoFileName;
	
		File videoFile = new File(filePath);
	
		Log.d(TAG, "Video: " + videoFile.getPath());
		videoController.play(videoFile);
	}

	public void onAnimationRepeat(Animation animation) {}
	public void onAnimationStart(Animation animation) {}
	
	private OnCompletionListener videoCompletionListener = new OnCompletionListener() {
		public void onCompletion(MediaPlayer mp) {
			videoController.handleOnCompletion();
			videoController.setControlPanelVisibility(View.INVISIBLE);
		}
	};

	@Override
	public void onBackPressed() {
		config.setChecked(false);
		finish();
	}
}
