package ru.bigbears.wiserabbit.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ru.bigbears.wiserabbit.Config;
import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.XmlParser;
import ru.bigbears.wiserabbit.models.AdditionalCode;

/**
 * Created with IntelliJ IDEA.
 * User: antonignatov
 * Date: 12.10.12
 * Time: 8:52
 * To change this template use File | Settings | File Templates.
 */
public class KeyActivity extends Activity {
	
	public static final String EXTRA_IS_CODE_VALID = "is_code_valid";
    public static final String EXTRA_IS_CODE_INVALID = "is_code_invalid";
	public static final String EXTRA_CHECK_CODE_MODE = "check_code_mode";
	public static final String EXTRA_CODE = "code";

    private static final String TAG = "My";
    
	TableLayout page1;
    TableLayout page2;
    TableLayout page3;
    TableLayout page4;
    TableLayout page5;

    ImageButton buttonUser00;
    ImageButton buttonUser01;
    ImageButton buttonUser02;
    ImageButton buttonUser03;
    ImageButton buttonUser04;
    ImageButton buttonUser05;
    ImageButton buttonUser06;
    ImageButton buttonUser07;
    ImageButton buttonUser08;
    ImageButton buttonUser09;
    ImageButton buttonUser10;
    ImageButton buttonUser11;

    Button ruBtn;
    Button enBtn;

    TextView textNumberPage;

    TextView textCode;

    String stringCode = "";
    String intText = "";
    
    MediaPlayer sound;
    MediaPlayer soundNineKey;
    MediaPlayer soundNextKeyboard;
    MediaPlayer soundDelete;
    MediaPlayer soundInvalidKey;
    MediaPlayer soundValidKey;
    
    Button keyButton;

    String code;
    String failKey;
    String failMessage = "";

    String validCode;
    String invalidCode;
    
    String failKey1 = "";
    String failKey2 = "";
    String failKey3 = "";
    String failKey4 = "";
    
    String failMessage1;
    String failMessage2;
    String failMessage3;
    String failMessage4;

    XmlParser mParser;

    Config config;

    ArrayList<String> userButtons;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_key);

        findViewById(R.id.back).setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });

        buttonUser00 = (ImageButton)findViewById(R.id.btn_user_0);
        buttonUser01 = (ImageButton)findViewById(R.id.btn_user_1);
        buttonUser02 = (ImageButton)findViewById(R.id.btn_user_2);
        buttonUser03 = (ImageButton)findViewById(R.id.btn_user_3);
        buttonUser04 = (ImageButton)findViewById(R.id.btn_user_4);
        buttonUser05 = (ImageButton)findViewById(R.id.btn_user_5);
        buttonUser06 = (ImageButton)findViewById(R.id.btn_user_6);
        buttonUser07 = (ImageButton)findViewById(R.id.btn_user_7);
        buttonUser08 = (ImageButton)findViewById(R.id.btn_user_8);
        buttonUser09 = (ImageButton)findViewById(R.id.btn_user_9);
        buttonUser10 = (ImageButton)findViewById(R.id.btn_user_10);
        buttonUser11 = (ImageButton)findViewById(R.id.btn_user_11);

        Intent intent = this.getIntent();

        code = intent.getStringExtra(EXTRA_CODE);
        Log.d(TAG, "Key for opening content: " + code);

        validCode = intent.getStringExtra("validCode");
        invalidCode = intent.getStringExtra("invalidCode");

        failKey = intent.getStringExtra("failKey1");
        if (failKey != null && failKey != "" && (!failKey.equals("")) && (!failKey.isEmpty())) {
            failKey1 = failKey;
        } else failKey1 = "";
        failKey = intent.getStringExtra("failKey2");
        if (failKey != null && failKey != "" && (!failKey.equals("")) && (!failKey.isEmpty())) {
            failKey2 = failKey;
        } else failKey2 = "";
        failKey = intent.getStringExtra("failKey3");
        if (failKey != null && failKey != "" && (!failKey.equals("")) && (!failKey.isEmpty())) {
            failKey3 = failKey;
        } else failKey3 = "";
        failKey = intent.getStringExtra("failKey4");
        if (failKey != null && failKey != "" && (!failKey.equals("")) && (!failKey.isEmpty())) {
            failKey4 = failKey;
        } else failKey4 = "";

        failMessage = intent.getStringExtra("failMessage1");
        if (failMessage != null && failMessage != "" && (!failMessage.equals("")) && (!failMessage.isEmpty())) {
            failMessage1 = failMessage;
        } else failMessage1 = "";
        failMessage = intent.getStringExtra("failMessage2");
        if (failMessage != null && failMessage != "" && (!failMessage.equals("")) && (!failMessage.isEmpty())) {
            failMessage2 = failMessage;
        } else failMessage2 = "";
        failMessage = intent.getStringExtra("failMessage3");
        if (failMessage != null && failMessage != "" && (!failMessage.equals("")) && (!failMessage.isEmpty())) {
            failMessage3 = failMessage;
        } else failMessage3 = "";
        failMessage = intent.getStringExtra("failMessage4");
        if (failMessage != null && failMessage != "" && (!failMessage.equals("")) && (!failMessage.isEmpty())) {
            failMessage4 = failMessage;
        } else failMessage4 = "";

        Log.d(TAG, "Fail key1 : \"" + failKey1 + "\" , message : \"" + failMessage1 + "\"");
        Log.d(TAG, "Fail key2 : \"" + failKey2 + "\" , message : \"" + failMessage2 + "\"");
        Log.d(TAG, "Fail key3 : \"" + failKey3 + "\" , message : \"" + failMessage3 + "\"");
        Log.d(TAG, "Fail key4 : \"" + failKey4 + "\" , message : \"" + failMessage4 + "\"");

        config = new Config(this);

        mParser = new XmlParser(this, config, Environment.getExternalStorageDirectory().getPath() +
                getResources().getString(R.string.root_path) + config.getCurrentScenarioFile() + "/" + config.getCurrentScenarioFile() + ".xml");

        userButtons = mParser.getDefaultKeys();

        initUserButtons();

        // prepare sound effect
        sound = MediaPlayer.create(getBaseContext(), R.raw.sound);
        soundNineKey = MediaPlayer.create(getBaseContext(), R.raw.nine_key);
        soundNextKeyboard = MediaPlayer.create(getBaseContext(), R.raw.next_keyboard);
        soundDelete = MediaPlayer.create(getBaseContext(), R.raw.delete);
        soundInvalidKey = MediaPlayer.create(getBaseContext(), R.raw.invalid_key);
        soundValidKey = MediaPlayer.create(getBaseContext(), R.raw.valid_key);

        textNumberPage = (TextView) findViewById(R.id.text_number_page);
        textCode = (TextView) findViewById(R.id.text_code);
        intText = getIntent().getStringExtra("codeText");
        if (intText != null && intText != "" && (!intText.equals("")) && (!intText.isEmpty())) {
            stringCode = stringCode + intText;
            textCode.setText(stringCode);
        } else {
            stringCode = "";
        }
        
        keyButton = (Button)findViewById(R.id.button_key);
        keyButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				
				// Checking for additional codes...
				List<AdditionalCode> codes = mParser.getAdditionalCodes();
				for (int i = 0; i != codes.size(); ++i) {
					AdditionalCode code = codes.get(i);
					
					if (code.getCode().equals(stringCode)) {
						if (!code.isAnswered()) {
							mParser.setAdditionalCodeAnswered(code);
							
							int score = config.getScore();
							config.setScore(score + code.getMark());
						}
						
						Log.i(TAG, "KeyActivity: work with additional code");
						createPopupOnAdditionalCode(code.getMessage());
						
						return;
					}
				}
				
				// Standard work mode of OPRICL
				Log.i(TAG, "KeyActivity: work with standart code");
				createPopup();
			}
		});

        page1 = (TableLayout) findViewById(R.id.key_list_1);
        page2 = (TableLayout) findViewById(R.id.key_list_2);
        page3 = (TableLayout) findViewById(R.id.key_list_3);
        page4 = (TableLayout) findViewById(R.id.key_list_4);
        page5 = (TableLayout) findViewById(R.id.key_list_5);

        ruBtn = (Button) findViewById(R.id.button_rus);
        enBtn = (Button) findViewById(R.id.button_en);
    }

    public void russian(View v) {
        page1.setVisibility(View.INVISIBLE);
        page2.setVisibility(View.INVISIBLE);
        page3.setVisibility(View.INVISIBLE);
        page4.setVisibility(View.VISIBLE);
        ruBtn.setVisibility(View.INVISIBLE);
        enBtn.setVisibility(View.VISIBLE);
        textNumberPage.setText("4");
    }

    public void english(View v) {
        page4.setVisibility(View.INVISIBLE);
        page5.setVisibility(View.INVISIBLE);
        page1.setVisibility(View.VISIBLE);
        enBtn.setVisibility(View.INVISIBLE);
        ruBtn.setVisibility(View.VISIBLE);
        textNumberPage.setText("1");
    }

    public void nextPage(View v) {
    	soundNextKeyboard.start();
    	
        if(textNumberPage.getText().toString().equals("1")) {
            textNumberPage.setText("2");
            page1.setVisibility(View.INVISIBLE);
            page2.setVisibility(View.VISIBLE);
            page3.setVisibility(View.INVISIBLE);
        } else if(textNumberPage.getText().toString().equals("2")) {
            textNumberPage.setText("3");
            page1.setVisibility(View.INVISIBLE);
            page2.setVisibility(View.INVISIBLE);
            page3.setVisibility(View.VISIBLE);
        } else if(textNumberPage.getText().toString().equals("3")) {
            textNumberPage.setText("1");
            page1.setVisibility(View.VISIBLE);
            page2.setVisibility(View.INVISIBLE);
            page3.setVisibility(View.INVISIBLE);
        } else if(textNumberPage.getText().toString().equals("4")) {
            textNumberPage.setText("5");
            page4.setVisibility(View.INVISIBLE);
            page5.setVisibility(View.VISIBLE);
        } else if(textNumberPage.getText().toString().equals("5")) {
            textNumberPage.setText("4");
            page4.setVisibility(View.VISIBLE);
            page5.setVisibility(View.INVISIBLE);
        }
    }

    public void printKey(View v) {
    	
        switch (v.getId()) {
            case R.id.btn_0:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "0");
                stringCode = stringCode + 0;
                break;
            case R.id.btn_1:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "1");
                stringCode = stringCode + 1;
                break;
            case R.id.btn_2:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "2");
                stringCode = stringCode + 2;
                break;
            case R.id.btn_3:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "3");
                stringCode = stringCode + 3;
                break;
            case R.id.btn_4:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "4");
                stringCode = stringCode + 4;
                break;
            case R.id.btn_5:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "5");
                stringCode = stringCode + 5;
                break;
            case R.id.btn_6:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "6");
                stringCode = stringCode + 6;
                break;
            case R.id.btn_7:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "7");
                stringCode = stringCode + 7;
                break;
            case R.id.btn_8:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "8");
                stringCode = stringCode + 8;
                break;
            case R.id.btn_9:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "9");
                stringCode = stringCode + 9;
                break;
            case R.id.btn_a:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "a");
                stringCode = stringCode + "a";
                break;
            case R.id.btn_b:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "b");
                stringCode = stringCode + "b";
                break;
            case R.id.btn_c:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "c");
                stringCode = stringCode + "c";
                break;
            case R.id.btn_d:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "d");
                stringCode = stringCode + "d";
                break;
            case R.id.btn_e:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "e");
                stringCode = stringCode + "e";
                break;
            case R.id.btn_f:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "f");
                stringCode = stringCode + "f";
                break;
            case R.id.btn_g:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "g");
                stringCode = stringCode + "g";
                break;
            case R.id.btn_h:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "h");
                stringCode = stringCode + "h";
                break;
            case R.id.btn_i:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "i");
                stringCode = stringCode + "i";
                break;
            case R.id.btn_j:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "j");
                stringCode = stringCode + "j";
                break;
            case R.id.btn_k:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "k");
                stringCode = stringCode + "k";
                break;
            case R.id.btn_l:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "l");
                stringCode = stringCode + "l";
                break;
            case R.id.btn_m:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "m");
                stringCode = stringCode + "m";
                break;
            case R.id.btn_n:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "n");
                stringCode = stringCode + "n";
                break;
            case R.id.btn_o:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "o");
                stringCode = stringCode + "o";
                break;
            case R.id.btn_p:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "p");
                stringCode = stringCode + "p";
                break;
            case R.id.btn_q:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "q");
                stringCode = stringCode + "q";
                break;
            case R.id.btn_r:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "r");
                stringCode = stringCode + "r";
                break;
            case R.id.btn_s:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "s");
                stringCode = stringCode + "s";
                break;
            case R.id.btn_t:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "t");
                stringCode = stringCode + "t";
                break;
            case R.id.btn_u:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "u");
                stringCode = stringCode + "u";
                break;
            case R.id.btn_v:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "v");
                stringCode = stringCode + "v";
                break;
            case R.id.btn_w:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "w");
                stringCode = stringCode + "w";
                break;
            case R.id.btn_x:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "x");
                stringCode = stringCode + "x";
                break;
            case R.id.btn_y:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "y");
                stringCode = stringCode + "y";
                break;
            case R.id.btn_z:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
            	sound.start();
                textCode.setText(textCode.getText().toString() + "z");
                stringCode = stringCode + "z";
                break;
            case R.id.btn_ar:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "а");
                stringCode = stringCode + "а";
                break;
            case R.id.btn_br:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "б");
                stringCode = stringCode + "б";
                break;
            case R.id.btn_vr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "в");
                stringCode = stringCode + "в";
                break;
            case R.id.btn_gr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "г");
                stringCode = stringCode + "г";
                break;
            case R.id.btn_dr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "д");
                stringCode = stringCode + "д";
                break;
            case R.id.btn_er:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "е");
                stringCode = stringCode + "е";
                break;
            case R.id.btn_zhr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "ж");
                stringCode = stringCode + "ж";
                break;
            case R.id.btn_zr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "з");
                stringCode = stringCode + "з";
                break;
            case R.id.btn_ir:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "и");
                stringCode = stringCode + "и";
                break;
            case R.id.btn_yr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "й");
                stringCode = stringCode + "й";
                break;
            case R.id.btn_kr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "к");
                stringCode = stringCode + "к";
                break;
            case R.id.btn_lr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "л");
                stringCode = stringCode + "л";
                break;
            case R.id.btn_mr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "м");
                stringCode = stringCode + "м";
                break;
            case R.id.btn_nr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "н");
                stringCode = stringCode + "н";
                break;
            case R.id.btn_or:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "о");
                stringCode = stringCode + "о";
                break;
            case R.id.btn_pr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "п");
                stringCode = stringCode + "п";
                break;
            case R.id.btn_rr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "р");
                stringCode = stringCode + "р";
                break;
            case R.id.btn_sr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "с");
                stringCode = stringCode + "с";
                break;
            case R.id.btn_tr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "т");
                stringCode = stringCode + "т";
                break;
            case R.id.btn_ur:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "у");
                stringCode = stringCode + "у";
                break;
            case R.id.btn_fr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "ф");
                stringCode = stringCode + "ф";
                break;
            case R.id.btn_hr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "х");
                stringCode = stringCode + "х";
                break;
            case R.id.btn_cr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "ц");
                stringCode = stringCode + "ц";
                break;
            case R.id.btn_chr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "ч");
                stringCode = stringCode + "ч";
                break;
            case R.id.btn_shr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "ш");
                stringCode = stringCode + "ш";
                break;
            case R.id.btn_shcr:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "щ");
                stringCode = stringCode + "щ";
                break;
            case R.id.btn_ioir:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "ы");
                stringCode = stringCode + "ы";
                break;
            case R.id.btn_ior:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "ь");
                stringCode = stringCode + "ь";
                break;
            case R.id.btn_ear:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "э");
                stringCode = stringCode + "э";
                break;
            case R.id.btn_yur:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "ю");
                stringCode = stringCode + "ю";
                break;
            case R.id.btn_yar:
                if(textCode.getText().toString().length() == 8) {
                    soundNineKey.start();
                    return;
                }
                sound.start();
                textCode.setText(textCode.getText().toString() + "я");
                stringCode = stringCode + "я";
                break;
            case R.id.btn_backspace:
            	soundDelete.start();
            	
            	if (textCode.getText().toString().length() != 0) {
            		textCode.setText(textCode.getText().toString().substring(0, textCode.getText().toString().length() - 1));

                    if(stringCode.substring(stringCode.length() - 1).equals("]")) {
                        stringCode = stringCode.substring(0, stringCode.lastIndexOf("["));
                    } else {
                        stringCode = stringCode.substring(0, stringCode.length() - 1);
                    }

                    Log.e(TAG, "String code: " + stringCode);

                }
                break;
        }
    }


    public void printUserKey(View v) {
        //sound.start();

        switch (v.getId()) {
            case R.id.btn_user_0:
                if(userButtons.size() > 0) {
                    if(textCode.getText().toString().length() == 8) {
                        soundNineKey.start();
                        return;
                    }
                    sound.start();
                    textCode.setText(textCode.getText().toString() + "#");
                    stringCode = stringCode + "[0]";
                }
                break;
            case R.id.btn_user_1:
                if(userButtons.size() > 1) {
                    if(textCode.getText().toString().length() == 8) {
                        soundNineKey.start();
                        return;
                    }
                    sound.start();
                    textCode.setText(textCode.getText().toString() + "#");
                    stringCode = stringCode + "[1]";
                }
                break;
            case R.id.btn_user_2:
                if(userButtons.size() > 2) {
                    if(textCode.getText().toString().length() == 8) {
                        soundNineKey.start();
                        return;
                    }
                    sound.start();
                    textCode.setText(textCode.getText().toString() + "#");
                    stringCode = stringCode + "[2]";
                }
                break;
            case R.id.btn_user_3:
                if(userButtons.size() > 3) {
                    if(textCode.getText().toString().length() == 8) {
                        soundNineKey.start();
                        return;
                    }
                    sound.start();
                    textCode.setText(textCode.getText().toString() + "#");
                    stringCode = stringCode + "[3]";
                }
                break;
            case R.id.btn_user_4:
                if(userButtons.size() > 4) {
                    if(textCode.getText().toString().length() == 8) {
                        soundNineKey.start();
                        return;
                    }
                    sound.start();
                    textCode.setText(textCode.getText().toString() + "#");
                    stringCode = stringCode + "[4]";
                }
                break;
            case R.id.btn_user_5:
                if(userButtons.size() > 5) {
                    if(textCode.getText().toString().length() == 8) {
                        soundNineKey.start();
                        return;
                    }
                    sound.start();
                    textCode.setText(textCode.getText().toString() + "#");
                    stringCode = stringCode + "[5]";
                }
                break;
            case R.id.btn_user_6:
                if(userButtons.size() > 6) {
                    if(textCode.getText().toString().length() == 8) {
                        soundNineKey.start();
                        return;
                    }
                    textCode.setText(textCode.getText().toString() + "#");
                    stringCode = stringCode + "[6]";
                }
                break;
            case R.id.btn_user_7:
                if(userButtons.size() > 7) {
                    if(textCode.getText().toString().length() == 8) {
                        soundNineKey.start();
                        return;
                    }
                    sound.start();
                    textCode.setText(textCode.getText().toString() + "#");
                    stringCode = stringCode + "[7]";
                }
                break;
            case R.id.btn_user_8:
                if(userButtons.size() > 8) {
                    if(textCode.getText().toString().length() == 8) {
                        soundNineKey.start();
                        return;
                    }
                    sound.start();
                    textCode.setText(textCode.getText().toString() + "#");
                    stringCode = stringCode + "[8]";
                }
                break;
            case R.id.btn_user_9:
                if(userButtons.size() > 9) {
                    if(textCode.getText().toString().length() == 8) {
                        soundNineKey.start();
                        return;
                    }
                    sound.start();
                    textCode.setText(textCode.getText().toString() + "#");
                    stringCode = stringCode + "[9]";
                }
                break;
            case R.id.btn_user_10:
                if(userButtons.size() > 10) {
                    if(textCode.getText().toString().length() == 8) {
                        soundNineKey.start();
                        return;
                    }
                    sound.start();
                    textCode.setText(textCode.getText().toString() + "#");
                    stringCode = stringCode + "[10]";
                }
                break;
            case R.id.btn_user_11:
                if(userButtons.size() > 11) {
                    if(textCode.getText().toString().length() == 8) {
                        soundNineKey.start();
                        return;
                    }
                    sound.start();
                    textCode.setText(textCode.getText().toString() + "#");
                    stringCode = stringCode + "[11]";
                }
                break;
        }
    }
    
    @Override
    public void onBackPressed() {
    	if(isValidKeyOpened) {
	        Intent intent = new Intent();
	        intent.putExtra(EXTRA_IS_CODE_VALID, true);
	        setResult(1, intent);
	        
	        finish();
    	} else 
    		finish();
    }
    
    boolean isValidKeyOpened = false;

    private void createPopup() {
        final PopupWindow popup = new PopupWindow(this);

        LayoutInflater inflater = KeyActivity.this.getLayoutInflater();
        View content = inflater.inflate(R.layout.key_good_layout, null);

        TextView tvMessageOnCode = (TextView)content.findViewById(R.id.text);

        popup.setContentView(content);

        popup.setWidth( (int) getResources().getDimension(R.dimen.opricl_window_width) );
        popup.setHeight( (int) getResources().getDimension(R.dimen.opricl_window_height) );

        popup.setOutsideTouchable(true);
        popup.setTouchable(true);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(getResources().getDrawable(R.drawable.key_good_image));
        
        if(code.equals(stringCode)) {
        	soundValidKey.start();
        	
        	// TODO: remove images after code typed here
        	List<File> files = mParser.getRemovedFilesForCode(code);
        	for (File file : files) {
        		try {
        			file.delete();
        		} catch (Exception e) {}
        	}

        	isValidKeyOpened = true;
        	
            tvMessageOnCode.setText(validCode);

            if(validCode == null) {
                tvMessageOnCode.setText(R.string.key_good_kode);
            }

            Animation anim = AnimationUtils.loadAnimation(KeyActivity.this, R.anim.alpha);
            tvMessageOnCode.startAnimation(anim);

            Timer timer = new Timer();
            timer.schedule(new PauseTask(),2000);
            popup.dismiss();

            content.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_IS_CODE_VALID, true);
                    setResult(1, intent);


                    finish();
                    popup.dismiss();
                }
            });

            popup.dismiss();
        } else if (failKey1.equals(stringCode)) {
        	soundValidKey.start();
            tvMessageOnCode.setText(failMessage1);

            if(invalidCode == null) {
                tvMessageOnCode.setText(R.string.key_bad_kode);
            }

            Animation anim = AnimationUtils.loadAnimation(KeyActivity.this, R.anim.alpha);
            tvMessageOnCode.startAnimation(anim);
            Log.e("MY", "=" + failMessage1);

            content.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_IS_CODE_INVALID, true);
                    setResult(2, intent);

                    finish();
                    popup.dismiss();
                }
            });

            popup.dismiss();
        } else if (failKey2.equals(stringCode)) {
        	soundValidKey.start();
            tvMessageOnCode.setText(failMessage2);

            if(invalidCode == null) {
                tvMessageOnCode.setText(R.string.key_bad_kode);
            }

            Animation anim = AnimationUtils.loadAnimation(KeyActivity.this, R.anim.alpha);
            tvMessageOnCode.startAnimation(anim);
            Log.e("MY", "=" + failMessage2);

            content.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_IS_CODE_INVALID, true);
                    setResult(2, intent);

                    finish();
                    popup.dismiss();
                }
            });

            popup.dismiss();
        } else if (failKey3.equals(stringCode)) {
        	soundValidKey.start();
            tvMessageOnCode.setText(failMessage3);

            if(invalidCode == null) {
                tvMessageOnCode.setText(R.string.key_bad_kode);
            }

            Animation anim = AnimationUtils.loadAnimation(KeyActivity.this, R.anim.alpha);
            tvMessageOnCode.startAnimation(anim);
            Log.e("MY", "=" + failMessage3);

            content.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_IS_CODE_INVALID, true);
                    setResult(2, intent);

                    finish();
                    popup.dismiss();
                }
            });

            popup.dismiss();
        } else if (failKey4.equals(stringCode)) {
        	soundValidKey.start();
            tvMessageOnCode.setText(failMessage4);

            if(invalidCode == null) {
                tvMessageOnCode.setText(R.string.key_bad_kode);
            }

            Animation anim = AnimationUtils.loadAnimation(KeyActivity.this, R.anim.alpha);
            tvMessageOnCode.startAnimation(anim);
            Log.e("MY", "=" + failMessage4);

            content.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.putExtra(EXTRA_IS_CODE_INVALID, true);
                    setResult(2, intent);

                    finish();
                    popup.dismiss();
                }
            });

            popup.dismiss();
        } else {
        	soundInvalidKey.start();
        	
            tvMessageOnCode.setText(invalidCode);

            if(invalidCode == null) {
                tvMessageOnCode.setText(R.string.key_bad_kode);
            }

            Animation anim = AnimationUtils.loadAnimation(KeyActivity.this, R.anim.alpha);
            tvMessageOnCode.startAnimation(anim);
            Log.e("MY", "=" + invalidCode);

            content.setOnClickListener(new OnClickListener() {
                public void onClick(View view) {
                    popup.dismiss();
                }
            });
        }

        RelativeLayout layout = (RelativeLayout)findViewById(R.id.main);
        popup.showAtLocation(layout, Gravity.CENTER_VERTICAL, 0, 0);
    }
    
    private void createPopupOnAdditionalCode(String message) {
        final PopupWindow popup = new PopupWindow(this);

        LayoutInflater inflater = KeyActivity.this.getLayoutInflater();
        View content = inflater.inflate(R.layout.key_good_layout, null);

        TextView tvMessageOnCode = (TextView)content.findViewById(R.id.text);

        popup.setContentView(content);

        popup.setWidth( (int) getResources().getDimension(R.dimen.opricl_window_width) );
        popup.setHeight( (int) getResources().getDimension(R.dimen.opricl_window_height) );

        popup.setOutsideTouchable(true);
        popup.setTouchable(true);
        popup.setFocusable(true);
        popup.setBackgroundDrawable(getResources().getDrawable(R.drawable.key_good_image));
        
        soundValidKey.start();

        tvMessageOnCode.setText(message);

        Animation anim = AnimationUtils.loadAnimation(KeyActivity.this, R.anim.alpha);
        tvMessageOnCode.startAnimation(anim);

		content.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				finish();
				popup.dismiss();
			}
		});

        RelativeLayout layout = (RelativeLayout)findViewById(R.id.main);
        popup.showAtLocation(layout, Gravity.CENTER_VERTICAL, 0, 0);
    }

    void initUserButtons() {
        if(userButtons.size() > 0) {
            buttonUser00.setClickable(true);
            buttonUser00.setVisibility(View.VISIBLE);
            Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + userButtons.get(0).replace('\\', '/'));
            buttonUser00.setImageDrawable(d);
        }

        if(userButtons.size() > 1) {
            buttonUser01.setClickable(true);
            buttonUser01.setVisibility(View.VISIBLE);
            Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + userButtons.get(1).replace('\\', '/'));
            buttonUser01.setImageDrawable(d);
        }

        if(userButtons.size() > 2) {
            buttonUser02.setClickable(true);
            buttonUser02.setVisibility(View.VISIBLE);
            Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + userButtons.get(2).replace('\\', '/'));
            buttonUser02.setImageDrawable(d);
        }

        if(userButtons.size() > 3) {
            buttonUser03.setClickable(true);
            buttonUser03.setVisibility(View.VISIBLE);
            Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + userButtons.get(3).replace('\\', '/'));
            buttonUser03.setImageDrawable(d);
        }

        if(userButtons.size() > 4) {
            buttonUser04.setClickable(true);
            buttonUser04.setVisibility(View.VISIBLE);
            Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + userButtons.get(4).replace('\\', '/'));
            buttonUser04.setImageDrawable(d);
        }

        if(userButtons.size() > 5) {
            buttonUser05.setClickable(true);
            buttonUser05.setVisibility(View.VISIBLE);
            Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + userButtons.get(5).replace('\\', '/'));
            buttonUser05.setImageDrawable(d);
        }

        if(userButtons.size() > 6) {
            buttonUser06.setClickable(true);
            buttonUser06.setVisibility(View.VISIBLE);
            Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + userButtons.get(6).replace('\\', '/'));
            buttonUser06.setImageDrawable(d);
        }

        if(userButtons.size() > 7) {
            buttonUser07.setClickable(true);
            buttonUser07.setVisibility(View.VISIBLE);
            Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + userButtons.get(7).replace('\\', '/'));
            buttonUser07.setImageDrawable(d);
        }

        if(userButtons.size() > 8) {
            buttonUser08.setClickable(true);
            buttonUser08.setVisibility(View.VISIBLE);
            Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + userButtons.get(8).replace('\\', '/'));
            buttonUser08.setImageDrawable(d);
        }

        if(userButtons.size() > 9) {
            buttonUser09.setClickable(true);
            buttonUser09.setVisibility(View.VISIBLE);
            Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + userButtons.get(9).replace('\\', '/'));
            buttonUser09.setImageDrawable(d);
        }

        if(userButtons.size() > 10) {
            buttonUser10.setClickable(true);
            buttonUser10.setVisibility(View.VISIBLE);
            Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + userButtons.get(10).replace('\\', '/'));
            buttonUser10.setImageDrawable(d);
        }

        if(userButtons.size() > 11) {
            buttonUser11.setClickable(true);
            buttonUser11.setVisibility(View.VISIBLE);
            Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                    getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() + userButtons.get(11).replace('\\', '/'));
            buttonUser11.setImageDrawable(d);
        }
    }

    public class PauseTask extends TimerTask {

        @Override
        public void run() {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_IS_CODE_VALID, true);
            setResult(1, intent);

            finish();
        }
    }
}
