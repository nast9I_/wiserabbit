/**
 * 
 */
package ru.bigbears.wiserabbit.activities;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.Utils;
import ru.bigbears.wiserabbit.consts.ScenarioStructure;
import ru.bigbears.wiserabbit.dialogs.CallbackDialog;
import ru.bigbears.wiserabbit.dialogs.DialogExit;
import ru.bigbears.wiserabbit.dialogs.InputPasswordDialog;
import ru.bigbears.wiserabbit.models.HelperDirectory;
import ru.bigbears.wiserabbit.models.ReferenceDirectory;
import ru.bigbears.wiserabbit.models.ScenarioDirectory;
import ru.bigbears.wiserabbit.models.ScenarioImage;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Vladimir
 *
 */
public class ActivityNotepad extends ActivityBase {
	private static final String TAG = "My";

	private MediaPlayer soundYesNo;

	private ArrayList<ScenarioDirectory> mNotepadDirectories;
	private ArrayList<ScenarioDirectory> mFailDirectories;

	// Index for checking user progress in game.
    // These index can't be bigger then count of directories in Scenario path
    int mProgressDirectoryIndex = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		soundYesNo = MediaPlayer.create(this, R.raw.yes_no);
		soundYesNo.setVolume(0.7f, 0.7f);

			mFlipper.setCurrentIndex(config.getCurrentImageIndex());

		//videoController.hideButtons();

		config.setMenuVisible(true);
	}

	@Override
	protected void initUI() {
		super.initUI();

        mBack.setVisibility(View.INVISIBLE);

		boolean check = getIntent().getBooleanExtra("check",false);
		if (check) {
			checkBtn.setVisibility(View.VISIBLE);
		}

		menuButton.setVisibility(View.VISIBLE);
		referenceButton.setVisibility(View.INVISIBLE);

		/*Button refer = (Button) findViewById(R.id.btnRefer);
		refer.setVisibility(View.VISIBLE);*/

		mProgressDirectoryIndex = config.getCurrentDirectoryIndex();

		// set notepad name from xml file
        String notepadTitle = xmlParser.getNotepadName();
        if(notepadTitle != null && !notepadTitle.equals(""))
        	setTitle(notepadTitle);
        else
        	setTitle(R.string.title_notepad);

		try {
			mFailDirectories = xmlParser.getFailDirectories();
		} catch (Exception ex) {
			mFailDirectories = new ArrayList<ScenarioDirectory>();
		}
        // Add images from directories that was opened by player previously
        mNotepadDirectories = xmlParser.getScenarioDirectories();
        for(int i = 0; i != mProgressDirectoryIndex+1; i++) {
        	navigatorAdapter.addImages(mNotepadDirectories.get(i).getImages());
			if (config.isFailDirUse(i) && i < mFailDirectories.size()) {
				navigatorAdapter.addImages(mFailDirectories.get(i).getImages());
			}
        }

        // Now me must to check next directories for 'opening code'. If code is not
        // available then we should add images from not-closed directory too...
        for (int i = mProgressDirectoryIndex+1; i != mNotepadDirectories.size(); ++i) {

        	ScenarioDirectory directory = mNotepadDirectories.get(i);

        	// if a code for directory is not exists, add images at the navigation strip
        	if (directory.getCode() == null || directory.getCode().equals("")) {
        		mProgressDirectoryIndex++;
        		config.setCurrentDirectoryIndex(mProgressDirectoryIndex);

        		navigatorAdapter.addImages(directory.getImages());
        		navigatorAdapter.notifyDataSetChanged();
        	} else {
        		// found directory with a code, we should break the loop
        		break;
        	}
        }

        navigatorAdapter.notifyDataSetChanged();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(data == null) {
        	Log.d(TAG, "Result: data is null");
        	return;
        }

        if (resultCode == 1) {
	        if(data.getBooleanExtra(KeyActivity.EXTRA_IS_CODE_VALID, false)) {
	        	Log.d(TAG, "Result: good key get!!!");

	        	mProgressDirectoryIndex++;
	            handleNextDirectoryOpen2();

				if (mProgressDirectoryIndex == (mNotepadDirectories.size() - 1)) {
					Timer timer = new Timer();
					timer.schedule(new CallbackTimerTask(),4000);
				}
	        }
        }

		if (resultCode == 2) {
			if(data.getBooleanExtra(KeyActivity.EXTRA_IS_CODE_INVALID, false)) {
				Log.d(TAG, "Result: Fail key get!!!");

				handleFailDirectoryOpen();
			}
		}
	}

	@Override
	public void onBackPressed() {
		DialogExit.show(this);
	}

	@Override
	protected void setPageAt(int position) {
		super.setPageAt(position);

		// In notepad we must save current page position in preference using 
		// Config class.
		config.setCurrentImageIndex(currentPagePosition);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return super.onTouchEvent(event);
	}

	@Override
	protected void handleButtonHintPressed() {
		ScenarioImage image = (ScenarioImage)currentDirectoryElement;
		try {
			String tipPath = image.getTipPath();
	    	if (tipPath != null && tipPath.length() != 0) {
	    		tipPath = tipPath.replace('\\', '/');
	    		File tipFile = new File(Environment.getExternalStorageDirectory().getPath() +
	                    getResources().getString(R.string.root_path) +
	                    config.getCurrentScenarioFile() + tipPath);
	    		if (!tipFile.exists()) {
	    			return;
	    		}
	    	} else {
	    		return;
	    	}
		} catch (Exception e) {
			return;
		}

        if (!config.isHintUse(currentPagePosition)) {
        	showHintDialog(image.getTipMarks(), image.getTipDescription());
        } else {
			Log.d("LOG ARTEM HINT: ", image.getTipPath());
            Intent intent = new Intent(this, HintActivity.class);
            intent.putExtra(HintActivity.INTENT_PARAM_OPENED_IMAGE_PATH, image.getTipPath());
			config.setMenuVisible(false);

            /*if (image.getTipVideoPath() != null) {
            	intent.putExtra(HintActivity.INTENT_PARAM_TIP_VIDEO_FILE_PATH, image.getTipVideoPath());
            }*/

            startActivity(intent);
        }
	}

	@Override
	protected void handleButtonReferencePressed() {
		ScenarioImage image = (ScenarioImage)currentDirectoryElement;
		try {
			String referPath = image.getReferencePath();
			if (referPath != null && referPath.length() != 0) {
				referPath = referPath.replace('\\', '/');
				File referFile = new File(Environment.getExternalStorageDirectory().getPath() +
						getResources().getString(R.string.root_path) +
						config.getCurrentScenarioFile() + referPath);
				if (!referFile.exists()) {
					Log.w("Error:", "Error!");
					return;
				}
			} else {
				return;
			}
		} catch (Exception e) {
			Log.w("Error:", "Error!");
			return;
		}

		final ArrayList<ReferenceDirectory> refList = xmlParser.getReferenceDirectories();

		final Intent intent = new Intent(this, ReferenceActivity.class);
		intent.putExtra(ReferenceActivity.INTENT_PARAM_OPENED_IMAGE_PATH, image.getReferencePath());
		config.setMenuVisible(false);

		if (image.getReferenceVideoPath() != null) {
			intent.putExtra(ReferenceActivity.INTENT_PARAM_REFERENCE_VIDEO_FILE_PATH, image.getReferenceVideoPath());
		}

		for (int i=0; i != refList.size(); ++i) {
			if (refList.get(i).getImage(0).getPath().equalsIgnoreCase(image.getReferencePath())) {
				if (refList.get(i).getCode() != null && (!refList.get(i).getCode().equals(""))) {
					if (!config.isReferCodeUse(currentPagePosition)) {
						final ReferenceDirectory directory = refList.get(i);
						InputPasswordDialog dialog = new InputPasswordDialog();
						dialog.show(getSupportFragmentManager(), InputPasswordDialog.TAG);
						final int mark = refList.get(i).getMarks();
						dialog.setOnPasswordInputed(new InputPasswordDialog.OnPasswordInputedListener() {

							public void onPasswordInputed(String password) {
								if (password.equals(directory.getCode())) {
									config.useReferCode(currentPagePosition);
									//dashboardAdapter.notifyDataSetChanged();

									int newMark = mark + config.getScoreBunned();
									config.setScoreBunned(newMark);

									startActivity(intent);
								} else {
									Toast.makeText(ActivityNotepad.this,
											R.string.password_wrong, Toast.LENGTH_SHORT).show();
									return;
								}
							}
						});
						/*intent.putExtra("code", refList.get(i).getCode());
						intent.putExtra("marks", refList.get(i).getMarks());
						intent.putExtra("validCode", refList.get(i).getMessageValidCode());
						intent.putExtra("invalidCode", refList.get(i).getMessageInvalidCode());
						intent.putExtra("referPage",currentPagePosition);*/
					} else {
						startActivity(intent);
					}
					break;
				} else {
					startActivity(intent);
				}
			}
		}


	}

	@Override
	protected void onResume() {
		super.onResume();
        config.setNotepad(true);
        config.setCanVisibleMenu(true);
		config.setMenuVisible(true);
	}

    @Override
    protected void onStop() {
        super.onStop();
        config.setNotepad(false);
    }

	@Override
	protected void handleOnVideoViewAnimationEnd() {
		if (currentDirectoryElement instanceof ScenarioImage) {

			String filePath = ((ScenarioImage)currentDirectoryElement).getVideoFilePath();
			filePath = filePath.replace('\\', '/');

			File videoFile = new File(Environment.getExternalStorageDirectory().getPath() +
				getString(R.string.root_path) +
				config.getCurrentScenarioFile() + filePath);

			Log.d(TAG, "Video: " + videoFile.getPath());
			videoController.play(videoFile);
		}
	}

    @Override
    protected void setupDrawer() {
        mMenuNavigation.setVisibility(View.VISIBLE);
        mMenuNavigation.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                soundMenuButton.start();

                if (mNavigation.getVisibility() == View.VISIBLE) {
                    mNavigation.setVisibility(View.INVISIBLE);
                } else {
                    mNavigation.setVisibility(View.VISIBLE);
                }
                mDrawer.close();
            }
        });

        mMenuManual.setVisibility(View.VISIBLE);
        mMenuManual.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                soundMenuButton.start();

                // check helper image exists
                ArrayList<HelperDirectory> helperDirectories = xmlParser.getHelperDirectoryNames();
                if (helperDirectories == null || helperDirectories.size() == 0) {
                    Toast.makeText(ActivityNotepad.this, R.string.message_helper_empty, Toast.LENGTH_LONG).show();
                    mDrawer.close();
                    return;
                }

                Intent intent = new Intent(ActivityNotepad.this, ActivityHelperDashboard.class);
                startActivity(intent);

                mDrawer.close();
            }
        });

        mMenuOpricl.setVisibility(View.VISIBLE);
        mMenuOpricl.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                soundMenuButton.start();

                Intent intent = new Intent(ActivityNotepad.this, KeyActivity.class);
                Log.e("My", "mDirs.size = " + mNotepadDirectories.size() + " mIndexDir = " + (mProgressDirectoryIndex + 1));
                try {
                    intent.putExtra("code", mNotepadDirectories.get(mProgressDirectoryIndex + 1).getCode());
                    intent.putExtra("validCode", mNotepadDirectories.get(mProgressDirectoryIndex + 1).getMessageValidCode());
                    intent.putExtra("invalidCode", mNotepadDirectories.get(mProgressDirectoryIndex + 1).getMessageInvalidCode());

                    // put in intent fail keys and messages
                    intent.putExtra("failKey1", mNotepadDirectories.get(mProgressDirectoryIndex + 1).getFailKey1());
                    intent.putExtra("failKey2", mNotepadDirectories.get(mProgressDirectoryIndex + 1).getFailKey2());
                    intent.putExtra("failKey3", mNotepadDirectories.get(mProgressDirectoryIndex + 1).getFailKey3());
                    intent.putExtra("failKey4", mNotepadDirectories.get(mProgressDirectoryIndex + 1).getFailKey4());

                    intent.putExtra("failMessage1", mNotepadDirectories.get(mProgressDirectoryIndex + 1).getFailMessage1());
                    intent.putExtra("failMessage2", mNotepadDirectories.get(mProgressDirectoryIndex + 1).getFailMessage2());
                    intent.putExtra("failMessage3", mNotepadDirectories.get(mProgressDirectoryIndex + 1).getFailMessage3());
                    intent.putExtra("failMessage4", mNotepadDirectories.get(mProgressDirectoryIndex + 1).getFailMessage4());

                    startActivityForResult(intent, 1);
                } catch (Exception ex) {

                }

                mDrawer.close();
            }
        });

        mMenuDelete.setVisibility(View.VISIBLE);
        mMenuDelete.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                InputPasswordDialog dialog = new InputPasswordDialog();
                dialog.show(getSupportFragmentManager(), InputPasswordDialog.TAG);
                dialog.setOnPasswordInputed(new InputPasswordDialog.OnPasswordInputedListener() {
                    public void onPasswordInputed(String password) {
                        if (password.equals("00000")) {
                            deletePak();
                            config.delPak();
                            Toast.makeText(ActivityNotepad.this,
                                    "Пакет успешно удален...", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ActivityNotepad.this,PreStartActivity.class));
                            finish();
                        } else {
                            Toast.makeText(ActivityNotepad.this,
                                    "Неверный пароль для удаления...", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });
    }

	private void showPlayerResults() {
		final PopupWindow popup = new PopupWindow(this);

		LayoutInflater inflater = getLayoutInflater();
		View content = inflater.inflate(R.layout.marks_result, null);

		TextView tvCounted = (TextView)content.findViewById(R.id.tvCounted);
		TextView tvBunned = (TextView)content.findViewById(R.id.tvBunned);
		TextView tvResult = (TextView)content.findViewById(R.id.tvResult);

		// get marks bunned and counted
		int bunned = config.getScoreBunned();
		int counted = config.getScore();

		// ... and show them
		tvCounted.setText( String.valueOf(counted) );
		tvBunned.setText( String.valueOf(bunned) );
		tvResult.setText( String.valueOf(counted - bunned) );

		Button btnSendBase64 = (Button)content.findViewById(R.id.btnSendBase64);
		btnSendBase64.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				String expeditionId = String.valueOf(xmlParser.getExpeditionId());
				String activationCode = xmlParser.getPassword();
				String score = String.valueOf(config.getScore() - config.getScoreBunned());

				String str = expeditionId + "," + activationCode + "," + score;// + "," + new Date().to;
				String base64 = null;
				try {
					base64 = Base64.encodeToString(str.getBytes("UTF-8"), Base64.DEFAULT);
				} catch (UnsupportedEncodingException e) {}

				Intent intent = new Intent(Intent.ACTION_SEND);
			    intent.setType("text/plain");
			    intent.putExtra(Intent.EXTRA_TEXT, base64);
			    startActivity(Intent.createChooser(intent, "Share with"));
			}
		});

		Button btnResetGame = (Button)content.findViewById(R.id.btnResetGame);
		btnResetGame.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				InputPasswordDialog dialog = new InputPasswordDialog();
				dialog.show(getSupportFragmentManager(), InputPasswordDialog.TAG);
				dialog.setOnPasswordInputed(new InputPasswordDialog.OnPasswordInputedListener() {
					public void onPasswordInputed(String password) {
						if (password.equals("00000")) {
							popup.dismiss();
							deletePak();
							config.delPak();
							Toast.makeText(ActivityNotepad.this,
									"Пакет успешно удален...", Toast.LENGTH_SHORT).show();
							startActivity(new Intent(ActivityNotepad.this,PreStartActivity.class));
							finish();
						} else {
							Toast.makeText(ActivityNotepad.this,
									"Неверный пароль для удаления...", Toast.LENGTH_SHORT).show();
							return;
						}
					}
				});
			}
		});

		Button close = (Button)content.findViewById(R.id.btnClose);
		close.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				popup.dismiss();
			}
		});

		popup.setContentView(content);

		float density = getResources().getDisplayMetrics().density;
		popup.setWidth( (int)(460*density) );
		popup.setHeight( (int)(250*density) );

		popup.setOutsideTouchable(false);
		popup.setTouchable(true);
		popup.setFocusable(true);
		popup.setBackgroundDrawable(getResources().getDrawable(R.drawable.menu));

		RelativeLayout layout = (RelativeLayout)findViewById(R.id.main);
		popup.showAtLocation(layout, Gravity.CENTER_VERTICAL, 0, 0);
	}

	private void showHintDialog(int marksBan, String tipDescription) {
		final PopupWindow popup = new PopupWindow(this);

		LayoutInflater inflater = getLayoutInflater();
		View content = inflater.inflate(R.layout.dialog_open_hint, null);

		TextView tv = (TextView) content.findViewById(R.id.hint_desc);

		// ... and show them
		tv.setText(String.format(getResources().getString(R.string.hint_desc), marksBan));
        tv.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));

        tv = (TextView) content.findViewById(R.id.hint_title);
        tv.setTypeface(Utils.getTypeface(this, Utils.Font.PROTO_SANS));

		Button yes = (Button)content.findViewById(R.id.btnYes);
        yes.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
		yes.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                soundYesNo.start();

                config.useHint(currentPagePosition);

                ScenarioImage image = (ScenarioImage) currentDirectoryElement;

                // write tip count value...
                config.setScoreBunned(config.getScoreBunned() + image.getTipMarks());

                Intent intent = new Intent(ActivityNotepad.this, HintActivity.class);
                intent.putExtra(HintActivity.INTENT_PARAM_OPENED_IMAGE_PATH, image.getTipPath());
                startActivity(intent);

                popup.dismiss();
            }
        });

		Button no = (Button)content.findViewById(R.id.btnNo);
        no.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
		no.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				soundYesNo.start();

				popup.dismiss();
			}
		});

		popup.setContentView(content);

		float density = getResources().getDisplayMetrics().density;
		popup.setWidth( (int)(getResources().getDimensionPixelSize(R.dimen.dialog_width_px)*density) );
		popup.setHeight( (int)(getResources().getDimensionPixelSize(R.dimen.dialog_height_px)*density) );

		popup.setOutsideTouchable(false);
		popup.setTouchable(true);
		popup.setFocusable(true);
		popup.setBackgroundDrawable(getResources().getDrawable(R.drawable.menu));

		RelativeLayout layout = (RelativeLayout)findViewById(R.id.main);
		popup.showAtLocation(layout, Gravity.CENTER_VERTICAL, 0, 0);
	}

	private void handleNextDirectoryOpen2() {
		config.setFailDirUse(false,mProgressDirectoryIndex);
		if (mProgressDirectoryIndex < mNotepadDirectories.size()) {

			currentPagePosition = navigatorAdapter.getCount();

			navigatorAdapter.addImages(mNotepadDirectories.get(mProgressDirectoryIndex).getImages());
			navigatorAdapter.notifyDataSetChanged();
			updateNavigation();

			/*if (mProgressDirectoryIndex == mNotepadDirectories.size() -1) {
				currentPagePosition = navigatorAdapter.getNewSelected()+1;
			} else {
				currentPagePosition = navigatorAdapter.getNewSelected();
			}*/

			if (currentPagePosition < 0) {
				currentPagePosition = 0;
			} else if (currentPagePosition >= navigatorAdapter.getCount()) {
				currentPagePosition = navigatorAdapter.getCount()-1;
			}

			config.setScore(config.getScore() + mNotepadDirectories.get(mProgressDirectoryIndex).getMarks());
			config.setCurrentDirectoryIndex(mProgressDirectoryIndex);

			mFlipper.setCurrentIndex(currentPagePosition);
//			setPageAt(navigatorAdapter.getNewSelected());

			if ((mProgressDirectoryIndex + 1) != mNotepadDirectories.size()) {

				// Now me must to check next directories for 'opening code'. If code is not 
		        // available then we should add images from not-closed directory too...
		        for (int i = mProgressDirectoryIndex+1; i != mNotepadDirectories.size(); ++i) {

		        	ScenarioDirectory directory = mNotepadDirectories.get(i);

		        	// if a code for directory is not exists, add images at the navigation strip
		        	if (directory.getCode() == null || directory.getCode().equals("")) {
		        		mProgressDirectoryIndex++;
		        		config.setCurrentDirectoryIndex(mProgressDirectoryIndex);

		        		navigatorAdapter.addImages(directory.getImages());
		        		navigatorAdapter.notifyDataSetChanged();
						updateNavigation();
		        	} else {
		        		// found directory with a code, we should break the loop
		        		break;
		        	}
		        }
			}
		} else
			Log.d(TAG, "NEXT DIR: code not work");
	}

	private void handleFailDirectoryOpen() {
		config.setFailDirUse(true,mProgressDirectoryIndex);
		if (mProgressDirectoryIndex < mFailDirectories.size()) {

			int currentPage = navigatorAdapter.getCount();

			navigatorAdapter.addImages(mFailDirectories.get(mProgressDirectoryIndex).getImages());
			navigatorAdapter.notifyDataSetChanged();
			updateNavigation();


			if (mProgressDirectoryIndex == mFailDirectories.size() -1) {
				//currentPage = currentPagePosition+1;
				currentPagePosition = navigatorAdapter.getNewSelected()+1;

			} else {
				currentPage = currentPagePosition+1;
				currentPagePosition = navigatorAdapter.getNewSelected();
			}

			if (currentPagePosition < 0) {
				currentPagePosition = 0;
			} else if (currentPagePosition >= navigatorAdapter.getCount()) {
				currentPagePosition = navigatorAdapter.getCount()-1;
			}

			config.setScore(config.getScore() + mFailDirectories.get(mProgressDirectoryIndex).getMarks());
			config.setCurrentDirectoryIndex(mProgressDirectoryIndex);

			mFlipper.setCurrentIndex(currentPage);
//			setPageAt(navigatorAdapter.getNewSelected());
	} else
			Log.d(TAG, "NEXT DIR: code not work");
	}

	private boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();

			if (files == null) {
				return true;
			}
			Log.d(TAG, "In delete directory: " + files.length + " files");

			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					Log.d(TAG, "  Delete directory: " + files[i].getName());
					deleteDirectory(files[i]);
				} else {
					Log.d(TAG, "  Delete file: " + files[i].getName());
					files[i].delete();
				}
			}
		} else {
			Log.d(TAG, "Delete path not exist: " + path.getPath());
		}
		return (path.delete());
	}

	private boolean deletePak() {
		boolean isScenarioDeleted = false;

			isScenarioDeleted = true;

			// delete scenario content
			deleteDirectory(new File(Environment.getExternalStorageDirectory() +
					getResources().getString(R.string.root_path) + config.getCurrentScenarioFile() +
					File.separator + ScenarioStructure.SCENARIO_DIRECTORY));
			deleteDirectory(new File(Environment.getExternalStorageDirectory() +
					getResources().getString(R.string.root_path) + config.getCurrentScenarioFile() +
					File.separator + ScenarioStructure.HELPER_DIRECTORY));
			deleteDirectory(new File(Environment.getExternalStorageDirectory() +
					getResources().getString(R.string.root_path) + config.getCurrentScenarioFile() +
					File.separator + ScenarioStructure.REFERENCE_DIRECTORY));
			deleteDirectory(new File(Environment.getExternalStorageDirectory() +
					getResources().getString(R.string.root_path) + config.getCurrentScenarioFile() +
					File.separator + ScenarioStructure.HINT_DIRECTORY));
			deleteDirectory(new File(Environment.getExternalStorageDirectory() +
					getResources().getString(R.string.root_path) + config.getCurrentScenarioFile() +
					File.separator + ScenarioStructure.FAIL_EXIT_DIRECTORY));
			deleteDirectory(new File(Environment.getExternalStorageDirectory() +
					getResources().getString(R.string.root_path) + config.getCurrentScenarioFile() +
					File.separator + ScenarioStructure.KEYBOARD_DIRECTORY));

			// get Fotopad directory and clear
			File fotoPadDir = new File(Environment.getExternalStorageDirectory().toString() + getResources().getString(R.string.pak_path));
			fotoPadDir.setReadable(true);
			fotoPadDir.listFiles()[0].delete();
			File[] files = fotoPadDir.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
			fotoPadDir.setReadable(false);

		deleteDirectory(new File(Environment.getExternalStorageDirectory() +
				String.valueOf(R.string.root_path)));
		deleteDirectory(new File(Environment.getExternalStorageDirectory() +
				String.valueOf(R.string.pak_path)));

		new File(Environment.getExternalStorageDirectory() +
				String.valueOf(R.string.root_path)).mkdirs();
		new File(Environment.getExternalStorageDirectory() +
				String.valueOf(R.string.pak_path)).mkdirs();

		config.delPak();;

		return isScenarioDeleted;
	}

	public class CallbackTimerTask extends TimerTask {

		@Override
		public void run() {
			CallbackDialog dialog = new CallbackDialog();
			dialog.show(getSupportFragmentManager(), CallbackDialog.TAG);
			dialog.setOnCallbackInputed(new CallbackDialog.OnCallbackInputedListener() {

				public void onCallbackInputed(String callback, float rating) {
					config.setRating(rating);
					config.setCallback(callback);
				}
			});
		}
	}
}