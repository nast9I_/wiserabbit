package ru.bigbears.wiserabbit.activities;

import java.io.File;
import java.util.ArrayList;

import ru.bigbears.wiserabbit.Config;
import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.VideoController;
import ru.bigbears.wiserabbit.dialogs.DialogExit;
import ru.bigbears.wiserabbit.models.HintDirectory;
import ru.bigbears.wiserabbit.models.HintImage;
import ru.bigbears.wiserabbit.models.ReferenceDirectory;
import ru.bigbears.wiserabbit.models.ReferenceImage;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;

public class HintActivity extends ActivityBase {

	public static final String INTENT_PARAM_OPENED_IMAGE_PATH = "OpenedImagePath";
	public static final String INTENT_PARAM_TIP_VIDEO_FILE_PATH = "VideoFilePath";

	private ImageView hintImage;
	private Config config;

	private MediaPlayer soundYesNo;

	private static final String TAG = "My";

	protected VideoController videoController;

	private ArrayList<HintDirectory> mHintDirectories;

	int mHintDirectoryIndex;

	private MediaPlayer soundOpenHint;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		soundYesNo = MediaPlayer.create(this, R.raw.yes_no);
		soundYesNo.setVolume(0.7f, 0.7f);

		mFlipper.setCurrentIndex(0);
		//videoController.hideButtons();
	}

	@Override
	protected void initUI() {
		super.initUI();

		config = new Config(this);

		menuButton.setVisibility(View.GONE);
		//notepadReferenceButton.setVisibility(View.VISIBLE);

		DisplayMetrics metrics = getResources().getDisplayMetrics();
		videoController = new VideoController(this, metrics);

		/*Button refer = (Button) findViewById(R.id.btnRefer);
		refer.setVisibility(View.VISIBLE);*/

		//mHintDirectoryIndex = 0;

		// set notepad name from xml file
		String referTitle = xmlParser.getHintName();
		if(referTitle != null && !referTitle.equals(""))
			setTitle(referTitle);
		else
			setTitle(R.string.title_hint);

		// Add images from directories that was opened by player previously
		mHintDirectories = xmlParser.getHintDirectories();

		// Now me must to check next directories for 'opening code'. If code is not
		// available then we should add images from not-closed directory too...

		String path = getIntent().getStringExtra(HintActivity.INTENT_PARAM_OPENED_IMAGE_PATH);
		//Log.d("LOG ARTEM HINT: ",path + "\n");
		for (int i=0; i<mHintDirectories.size(); i++) {
			//Log.d("LOG ARTEM HINT: ",mHintDirectories.get(i).getImages().get(0).getPath());
			//String currPath = mHintDirectories.get(i).getImages().get(0).getPath();
			if (mHintDirectories.get(i).getImages().get(0).getPath().equalsIgnoreCase(path)) {
				mHintDirectoryIndex = i;
				//Log.d("LOG ARTEM HINT: ", String.valueOf(mHintDirectoryIndex));
				//Log.d("LOG ARTEM HINT: ", String.valueOf(i));
				//break;
			}
		}

		//Log.d("LOG ARTEM HINT: ", String.valueOf(mHintDirectoryIndex));
		//Log.d("LOG ARTEM HINT: ",mHintDirectories.get(mHintDirectoryIndex).getImages().get(0).getPath());

		HintDirectory directory = mHintDirectories.get(mHintDirectoryIndex);

		// if a code for directory is not exists, add images at the navigation strip

		navigatorAdapter.addImages(directory.getImages());
			// found directory with a code, we should break the loop


		navigatorAdapter.notifyDataSetChanged();

		String videoFilePath = getIntent().getStringExtra(INTENT_PARAM_TIP_VIDEO_FILE_PATH);
		if (videoFilePath == null) {
			videoController.setControlPanelVisibility(View.INVISIBLE);
		} else if (videoFilePath.length() == 0) {
			videoController.setControlPanelVisibility(View.INVISIBLE);
		}
 /*
		hintImage = (ImageView) findViewById(R.id.pageImage);

		String hintImagePath = getIntent().getStringExtra(INTENT_PARAM_OPENED_IMAGE_PATH);

		hintImagePath = Environment.getExternalStorageDirectory().getPath() +
				getResources().getString(R.string.root_path) +
				config.getCurrentHintFile() + hintImagePath;
		hintImagePath = hintImagePath.replace('\\', '/');
		Log.d(TAG, "Reference image path: " + hintImagePath);

		Drawable d = BitmapDrawable.createFromPath(hintImagePath);

		hintImage.setBackgroundDrawable(d);*/
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(data == null) {
			Log.d(TAG, "Result: data is null");
			return;
		}

		if (resultCode == 1) {
			if(data.getBooleanExtra(KeyActivity.EXTRA_IS_CODE_VALID, false)) {
				Log.d(TAG, "Result: good key get!!!");

			}
		}
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	protected void setPageAt(int position) {
		super.setPageAt(position);

		// In notepad we must save current page position in preference using
		// Config class.
		//config.setCurrentImageIndex(currentPagePosition);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return super.onTouchEvent(event);
	}

	@Override
	protected void handleButtonHintPressed() {
	}

	@Override
	protected void handleButtonReferencePressed() {
	}

	@Override
	protected void handleOnVideoViewAnimationEnd() {
		if (currentDirectoryElement instanceof HintImage) {

			String filePath = ((HintImage)currentDirectoryElement).getVideoPath();
			filePath = filePath.replace('\\', '/');

			File videoFile = new File(Environment.getExternalStorageDirectory().getPath() +
					getString(R.string.root_path) +
					config.getCurrentScenarioFile() + filePath);

			Log.d(TAG, "Video: " + videoFile.getPath());
			videoController.play(videoFile);
		}
	}

	@Override
	protected void setupDrawer() {}
	
	/*@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_hint);
		
		initUI();
		
		videoController.setAnimationListener(this);
        videoController.setOnClickListener(this);
        videoController.setOnCompletionListener(smallVideoCompletionListener);
	}

	private void initUI() {
		config = new Config(this);
		
		DisplayMetrics metrics = getResources().getDisplayMetrics();
        videoController = new VideoController(this, metrics);
        
        String videoFilePath = getIntent().getStringExtra(INTENT_PARAM_TIP_VIDEO_FILE_PATH);
        if (videoFilePath == null) {
        	videoController.setControlPanelVisibility(View.INVISIBLE);
        } else if (videoFilePath.length() == 0) {
        	videoController.setControlPanelVisibility(View.INVISIBLE);
        }
		
		hintImage = (ImageView) findViewById(R.id.image);
		
		String hintImagePath = getIntent().getStringExtra(INTENT_PARAM_OPENED_IMAGE_PATH);
		Log.d(TAG, "Hint image path: " + hintImagePath);
		hintImagePath = hintImagePath.replace('\\', '/');

        Drawable d = BitmapDrawable.createFromPath(Environment.getExternalStorageDirectory().getPath() +
                getResources().getString(R.string.root_path) +
                config.getCurrentScenarioFile() + hintImagePath);
		
		hintImage.setBackgroundDrawable(d);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnPlayVideo:
			videoController.handlePlayVideoButton();
			break;
			
		case R.id.btnToggleScreenMode:
			videoController.handleToggleScreenModeButton();
			break;

		default:
			break;
		}
	}

	public void onAnimationRepeat(Animation animation) {}
	public void onAnimationStart(Animation animation) {}
	public void onAnimationEnd(Animation animation) {
		handleOnVideoViewAnimationEnd();
	}
	
	private void handleOnVideoViewAnimationEnd() {
		String videoFilePath = getIntent().getStringExtra(INTENT_PARAM_TIP_VIDEO_FILE_PATH);
		if (videoFilePath != null) {
			videoFilePath = videoFilePath.replace('\\', '/');
		
			File videoFile = new File(Environment.getExternalStorageDirectory().getPath() + 
					getString(R.string.root_path) +
					config.getCurrentScenarioFile() + videoFilePath);
		
			Log.d(TAG, "Video: " + videoFile.getPath());
			videoController.play(videoFile);
		} else 
			Log.d(TAG, "Tip video file is NULL!");
	}
	
	private OnCompletionListener smallVideoCompletionListener = new OnCompletionListener() {
		public void onCompletion(MediaPlayer mp) {
			videoController.handleOnCompletion();
		}
	};*/
}
