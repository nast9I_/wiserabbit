/**
 * 
 */
package ru.bigbears.wiserabbit.activities;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.bigbears.wiserabbit.Config;
import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.Utils;
import ru.bigbears.wiserabbit.VideoController;
import ru.bigbears.wiserabbit.XmlParser;
import ru.bigbears.wiserabbit.adapters.NavigationStripAdapter;
import ru.bigbears.wiserabbit.flip.CurlPage;
import ru.bigbears.wiserabbit.flip.CurlView;
import ru.bigbears.wiserabbit.models.DirectoryElement;
import ru.bigbears.wiserabbit.models.HelperImage;
import ru.bigbears.wiserabbit.models.HintImage;
import ru.bigbears.wiserabbit.models.ReferenceDirectory;
import ru.bigbears.wiserabbit.models.ReferenceImage;
import ru.bigbears.wiserabbit.models.ScenarioImage;
import ru.bigbears.wiserabbit.services.CleanService;
import ru.bigbears.wiserabbit.ui.DrawerLayout;
import ru.bigbears.wiserabbit.ui.LadderLayout;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.View.OnClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.LocalDateTime;

/**
 * @author Vladimir
 *
 */
public abstract class ActivityBase extends FragmentActivity implements OnClickListener, AnimationListener, CurlView.OnPageChangeListener, Config.OnScoreChangeListener {
	private static final String TAG = "My";
	
	protected int activityLayoutId = R.layout.activity_screen_with_video;

	protected NavigationStripAdapter navigatorAdapter;

	protected CurlView mFlipper;
    protected DrawerLayout mDrawer;
    protected View mDrawerView;
	protected LadderLayout mNavigation;

    private PageProvider mPageProvider;

    protected TextView mMenuTitle;
    protected TextView mMenuManual;
    protected TextView mMenuOpricl;
    protected TextView mMenuNavigation;
    protected TextView mMenuPointsPos;
    protected TextView mMenuPointsNeg;
    protected TextView mMenuPointsSum;
    protected TextView mMenuDelete;
    protected TextView mMenuChronometerTitle;
    protected TextView mMenuChronometer;

    protected Button mResizeButton;
    protected View mSwitcher;
    private TextView mTitle;
    protected Button mBack;

	protected Button 			menuButton;
	protected Button			hintButton;
	protected Button			referenceButton;
	protected Button            notepadReferenceButton;
	protected Button            checkBtn;

    protected VideoController	videoController;
    
    protected boolean isNavigationButtonsVisible = true;

    private MediaPlayer soundOpenMenu;
    protected MediaPlayer soundMenuButton;
    private MediaPlayer soundOpenHint;
	private MediaPlayer soundHint;
	private MediaPlayer soundReference;
	
    protected Config config;
    
    protected int currentPagePosition = 0;
    protected DirectoryElement currentDirectoryElement;

	protected int dir;
	protected int img;
	protected boolean checked = false;
    
    protected XmlParser xmlParser;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	     setContentView(activityLayoutId);
		
	     initUI();

        mPageProvider = new PageProvider();
        mFlipper.setPageProvider(mPageProvider);
        mFlipper.setPageChangeListener(this);
        mFlipper.setBackgroundColor(0xFF202830);
        mDrawer.setDrawerView(mDrawerView);
        mDrawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            public void OnDrawerOpened() {
                //mFlipper.setEnabled(false);
            }

            public void OnDrawerClosed() {
                //mFlipper.setEnabled(true);
            }
        });
        mNavigation.setOnItemClickListener(new LadderLayout.OnItemClickListener() {
            public void onItemClick(View v, int index) {
                mFlipper.setCurrentIndex(index);
            }
        });
        mNavigation.setSelectedIndex(mFlipper.getCurrentIndex());
        mNavigation.setVisibility(View.INVISIBLE);

		 CleanService.setServiceAlarm(this, true);

	     menuButton.setOnClickListener(this);
        mResizeButton.setOnClickListener(this);
	     hintButton.setOnClickListener(this);
		 referenceButton.setOnClickListener(this);
        mSwitcher = findViewById(R.id.switcher);
        mSwitcher.setOnClickListener(this);
		 //notepadReferenceButton.setOnClickListener(this);
		 checkBtn.setOnClickListener(this);
	     
	     videoController.setAnimationListener(this);
	     videoController.setOnClickListener(this);
	     videoController.setOnCompletionListener(smallVideoCompletionListener);
	     
	     //pageImage.setOnTouchListener(mPageTouchListener);

	     soundOpenMenu = MediaPlayer.create(getBaseContext(), R.raw.open_menu);
	     soundOpenMenu.setVolume(0.7f, 0.7f);
	     soundMenuButton = MediaPlayer.create(getBaseContext(), R.raw.menu_button);
	     soundMenuButton.setVolume(0.7f, 0.7f);
	     soundOpenHint = MediaPlayer.create(getBaseContext(), R.raw.open_hint);
	     soundOpenHint.setVolume(0.7f, 0.7f);
		 soundHint = MediaPlayer.create(getBaseContext(), R.raw.open_hint);
		 soundHint.setVolume(0.7f, 0.7f);
		 soundReference = MediaPlayer.create(getBaseContext(), R.raw.open_hint);
		 soundReference.setVolume(0.7f, 0.7f);

        updateNavigation();
        setupDrawer();
	}

    protected abstract void setupDrawer();

    protected void toggleControls() {

        LocalDateTime startDateTime = LocalDateTime.now();
        Log.d("Date Year", String.valueOf(startDateTime.getYear()));
        Log.d("Date Mounth", String.valueOf(startDateTime.getMonthOfYear()));
        Log.d("Date Day", String.valueOf(startDateTime.getDayOfMonth()));
        Log.d("Date Hours", String.valueOf(startDateTime.getHourOfDay()));
        Log.d("Date Minutes", String.valueOf(startDateTime.getMinuteOfHour()));
        Log.d("Date Seconds", String.valueOf(startDateTime.getSecondOfMinute()));

        Log.d("---------------", "---------------------------------------------");

        Log.d("Start Date Year", String.valueOf(config.getStartDateTime().year));
        Log.d("Start Date Mounth", String.valueOf(config.getStartDateTime().month));
        Log.d("Start Date Day", String.valueOf(config.getStartDateTime().monthDay));
        Log.d("Start Date Hours", String.valueOf(config.getStartDateTime().hour));
        Log.d("Start Date Minutes", String.valueOf(config.getStartDateTime().minute));
        Log.d("Start Date Seconds", String.valueOf(config.getStartDateTime().second));

        Log.d("---------------", "---------------------------------------------");

        Log.d("End Date Year", String.valueOf(config.getEndDateTime().year));
        Log.d("End Date Mounth", String.valueOf(config.getEndDateTime().month));
        Log.d("End Date Day", String.valueOf(config.getEndDateTime().monthDay));
        Log.d("End Date Hours", String.valueOf(config.getEndDateTime().hour));
        Log.d("End Date Minutes", String.valueOf(config.getEndDateTime().minute));
        Log.d("End Date Seconds", String.valueOf(config.getEndDateTime().second));

        if (videoController.getScreenVisibility() == View.GONE) {
            if (!isNavigationButtonsVisible) {
                Log.e(TAG, "invisible");
                if (!config.getNotepad()) {
                    mBack.setVisibility(View.VISIBLE);
                }
                mSwitcher.setRotation(0);

                if (currentDirectoryElement instanceof ScenarioImage) {
                    ScenarioImage scenarioImage = (ScenarioImage)currentDirectoryElement;
                    if(scenarioImage.isScaled()) {
                        mResizeButton.setVisibility(View.VISIBLE);
                    }

                    String tipPath = ((ScenarioImage)currentDirectoryElement).getTipPath();
                    if (tipPath != null && tipPath.length() != 0) {
                        tipPath = tipPath.replace('\\', '/');
                        File tipFile = new File(Environment.getExternalStorageDirectory().getPath() +
                                getResources().getString(R.string.root_path) +
                                config.getCurrentScenarioFile() + tipPath);
                        if (tipFile.exists()) {
                            hintButton.setVisibility(View.VISIBLE);
                        }
                    }

                    String refPath = ((ScenarioImage)currentDirectoryElement).getReferencePath();
                    if (refPath != null && refPath.length() != 0) {
                        refPath = refPath.replace('\\', '/');
                        File refFile = new File(Environment.getExternalStorageDirectory().getPath() +
                                getResources().getString(R.string.root_path) +
                                config.getCurrentScenarioFile() + refPath);
                        if (refFile.exists()) {
                            referenceButton.setVisibility(View.VISIBLE);
                        }
                    }
                }

                if (currentDirectoryElement instanceof ReferenceImage) {
                    ReferenceImage referenceImage = (ReferenceImage) currentDirectoryElement;

                    String tipPath = ((ReferenceImage)currentDirectoryElement).getTipPath();
                    if (tipPath != null && tipPath.length() != 0) {
                        tipPath = tipPath.replace('\\', '/');
                        File tipFile = new File(Environment.getExternalStorageDirectory().getPath() +
                                getResources().getString(R.string.root_path) +
                                config.getCurrentReferenceFile() + tipPath);
                        if (tipFile.exists()) {
                            hintButton.setVisibility(View.VISIBLE);
                        }
                    }
                }

                if (config.getCanVisibleMenu()) {
                    if (config.getMenuVisible()) {
                        menuButton.setVisibility(View.VISIBLE);
                    } else menuButton.setVisibility(View.GONE);
                }
                isNavigationButtonsVisible = true;

                if (config.getChecked()) {
                    checkBtn.setVisibility(View.VISIBLE);
                }

            } else {
                Log.e(TAG, "visible");
                mSwitcher.setRotation(180);
                hintButton.setVisibility(View.INVISIBLE);
                referenceButton.setVisibility(View.INVISIBLE);
                mResizeButton.setVisibility(View.INVISIBLE);
                checkBtn.setVisibility(View.INVISIBLE);
                mBack.setVisibility(View.INVISIBLE);

                if (config.getCanVisibleMenu()) {
                    if (config.getMenuVisible()) {
                        menuButton.setVisibility(View.GONE);
                    } else menuButton.setVisibility(View.GONE);
                }
                isNavigationButtonsVisible = false;
            }
        }

        if (videoController.getScreenVisibility() == View.VISIBLE) {
            if (!videoController.isBtnsVisible()) {
                videoController.showButtons();
            } else {
                videoController.hideButtons();
            }
        }
    }

    protected void updateNavigation() {
        mNavigation.removeAllViews();
        for (int i = 0; i < mPageProvider.getPageCount(); i++) {
            mNavigation.addView(createNavigationItem(i));
        }
    }

    private View createNavigationItem(int index) {
        RelativeLayout rl = new RelativeLayout(this);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        rl.setLayoutParams(lp);

        int size = getResources().getDimensionPixelSize(R.dimen.navigation_item_size);
        int stroke = getResources().getDimensionPixelSize(R.dimen.navigation_item_stroke);
        int text = getResources().getDimensionPixelSize(R.dimen.navigation_item_text);

        lp = new RelativeLayout.LayoutParams(size, size);
        CircleImageView iv = new CircleImageView(this);
        iv.setLayoutParams(lp);
        iv.setImageBitmap(getBitmap(index));
        iv.setBorderColor(0xff937d5b);
        iv.setBorderWidth(stroke);

        lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, size, 0, 0);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        TextView tv = new TextView(this);
        tv.setText("" + (index + 1));
        tv.setTextColor(0xfffec929);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, text);
        tv.setLayoutParams(lp);

        rl.addView(iv);
        rl.addView(tv);

        return rl;
    }

	protected void initUI() {
		config = new Config(this);
		config.setCanVisibleMenu(true);
        config.setOnScoreChangeListener(this);

        mNavigation = (LadderLayout) findViewById(R.id.navigation);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerView = findViewById(R.id.drawerView);
        mFlipper = (CurlView) findViewById(R.id.flipper);
    	hintButton = (Button)findViewById(R.id.btnHint);
        menuButton = (Button)findViewById(R.id.btnMenu);
		referenceButton = (Button)findViewById(R.id.btnRefer);
		notepadReferenceButton = (Button) findViewById(R.id.btnNotepadReference);
        checkBtn = (Button)findViewById(R.id.btnCheck);
        mTitle = (TextView) findViewById(R.id.activity_title);
        mTitle.setTypeface(Utils.getTypeface(this, Utils.Font.PROTO_SANS));
        mBack = (Button) findViewById(R.id.back);
        mBack.setOnClickListener(this);

        mMenuTitle = (TextView) findViewById(R.id.menu_title);
        mMenuTitle.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
        mMenuManual = (TextView) findViewById(R.id.menu_manual);
        mMenuManual.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
        mMenuOpricl = (TextView) findViewById(R.id.menu_opricl);
        mMenuOpricl.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
        mMenuNavigation = (TextView) findViewById(R.id.menu_navigation);
        mMenuNavigation.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
        mMenuPointsPos = (TextView) findViewById(R.id.menu_points_positive);
        mMenuPointsPos.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
        mMenuPointsNeg = (TextView) findViewById(R.id.menu_points_negative);
        mMenuPointsNeg.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
        mMenuPointsSum = (TextView) findViewById(R.id.menu_points_sum);
        mMenuPointsSum.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
        mMenuDelete = (TextView) findViewById(R.id.menu_delete);
        mMenuDelete.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
        mMenuChronometerTitle = (TextView) findViewById(R.id.countdown_title);
        mMenuChronometerTitle.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));
        mMenuChronometer = (TextView) findViewById(R.id.countdown);
        mMenuChronometer.setTypeface(Utils.getTypeface(this, Utils.Font.MINION_PRO));

        onScoreChange(config);

        mResizeButton = (Button) findViewById(R.id.btnResize);
        menuButton.setVisibility(View.GONE);
		hintButton.setVisibility(View.INVISIBLE);
		referenceButton.setVisibility(View.INVISIBLE);
		//notepadReferenceButton.setVisibility(View.INVISIBLE);
		checkBtn.setVisibility(View.INVISIBLE);
        
    	//animationPageImage = (ImageView)findViewById(R.id.animationPageImage);
        //pageImage = (ImageView)findViewById(R.id.pageImage);
        
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        videoController = new VideoController(this, metrics);

        navigatorAdapter = new NavigationStripAdapter(this, config);
        
        xmlParser = new XmlParser(this, config, Environment.getExternalStorageDirectory().getPath() +
	    		 getResources().getString(R.string.root_path) + 
	             config.getCurrentScenarioFile() + "/" + 
	             config.getCurrentScenarioFile() + ".xml");

		if (config.getChecked()) {
			checkBtn.setVisibility(View.VISIBLE);
		}
	}

	protected void onCheck() {
		if (!config.getNotepad()) {
			config.setChecked(true);
			config.setCanVisibleMenu(true);
			config.setNotepad(true);
			finish();
		} else {
			config.setCanVisibleMenu(false);
			config.setNotepad(false);
			Intent intent = new Intent(this, ActivityHelper.class);
			intent.putExtra("index", config.getHelperDirectoryIndex());
			startActivity(intent);
		}
	}

    public void onScoreChange(Config newConfig) {
        mMenuPointsPos.setText(String.format(getString(R.string.menu_points_positive), newConfig.getScore()));
        mMenuPointsNeg.setText(String.format(getString(R.string.menu_points_negative), newConfig.getScoreBunned()));
        mMenuPointsSum.setText(String.format(getString(R.string.menu_points_sum), newConfig.getScore() - newConfig.getScoreBunned()));
    }

    protected void startResize() {
        ScenarioImage element = (ScenarioImage)currentDirectoryElement;

        Intent intent = new Intent(ActivityBase.this, ScaleActivity.class);
        intent.putExtra("ImagePath", element.getPath());
        startActivity(intent);
    }

    @Override
    public void setTitle(CharSequence title) {
        if (!TextUtils.isEmpty(title)) {
            mTitle.setVisibility(View.VISIBLE);
            mTitle.setText(title);
        } else {
            mTitle.setVisibility(View.GONE);
        }
    }

	public void onClick(View v) {
		switch (v.getId()) {

            case R.id.back:
                onBackPressed();
                break;

            case R.id.btnResize:
                soundOpenMenu.start();
                startResize();
                break;

            case R.id.switcher:
                soundOpenMenu.start();
                toggleControls();
                break;

			case R.id.btnMenu:
			    soundOpenMenu.start();
                mDrawer.toggle();
			break;

			case R.id.btnHint:
			soundOpenHint.start();
			handleButtonHintPressed();
			break;

			case R.id.btnPlayVideo:
			
			if (videoController.getVideoState() == VideoController.VIDEO_STATE_PLAY) {
                menuButton.setEnabled(false);
                mFlipper.setEnabled(false);
            }
			else {
                menuButton.setEnabled(true);
                mFlipper.setEnabled(true);
            }
			
			videoController.handlePlayVideoButton();
			break;

			case R.id.btnToggleScreenMode:
			videoController.handleToggleScreenModeButton();
			break;

			case R.id.btnRefer:
				handleButtonReferencePressed();
				break;

			case R.id.btnCheck:
				onCheck();
				break;

			default:
				break;
		}
	}
	
	protected abstract void handleButtonHintPressed();
	protected abstract void handleButtonReferencePressed();
	protected abstract void handleOnVideoViewAnimationEnd();

	
	public void onAnimationRepeat(Animation animation) {}
	public void onAnimationStart(Animation animation) {}
	public void onAnimationEnd(Animation animation) {
		handleOnVideoViewAnimationEnd();
	}
	
	protected void setPageAt(int position) {
		mNavigation.setSelectedIndex(position);
		config.setCurrentImageIndex(position);
		
        currentPagePosition = position;
        currentDirectoryElement = navigatorAdapter.getItem(position);
        
        // Handle logic for scenario image
        if (currentDirectoryElement instanceof ScenarioImage) {

			ScenarioImage scenarioImage = (ScenarioImage)currentDirectoryElement;

			if (scenarioImage.isScaled()) {
				Toast.makeText(this, getResources().getString(R.string.helper_info), Toast.LENGTH_SHORT).show();
                if (isNavigationButtonsVisible) {
                    mResizeButton.setVisibility(View.VISIBLE);
                }
				Log.w("SCALE","Изображение может быть увеличино!!!");
			} else {
                mResizeButton.setVisibility(View.INVISIBLE);
				Log.w("SCALE","Изображение HE может быть увеличино!!!");
			}

        	// logic for show/hide tip button...
        	String tipPath = ((ScenarioImage)currentDirectoryElement).getTipPath();
        	if (tipPath != null && tipPath.length() != 0) {
                Log.e(TAG, "has tip");
        		tipPath = tipPath.replace('\\', '/');
        		File tipFile = new File(Environment.getExternalStorageDirectory().getPath() +
                        getResources().getString(R.string.root_path) +
                        config.getCurrentScenarioFile() + tipPath);
                Log.e(TAG, Environment.getExternalStorageDirectory().getPath() +
                        getResources().getString(R.string.root_path) +
                        config.getCurrentScenarioFile() + tipPath);
        		if (tipFile.exists()) {
                    Log.e(TAG, "show tip");
	        		hintButton.setVisibility(View.VISIBLE);
	        		if(config.isHintUse(position)) {
					}
	        		else {
						Timer timer = new Timer();
						ShowButtonsTask showButtonsTask = new ShowButtonsTask(hintButton,
								soundHint);
						timer.schedule(showButtonsTask, 1000);
					}
        		}
            } else {
                hintButton.setVisibility(View.GONE);
            }

			// logic for show/hide reference button...
			String referPath = ((ScenarioImage)currentDirectoryElement).getReferencePath();
			if (referPath != null && referPath.length() != 0) {
                Log.e(TAG, "has refer");
				referPath = referPath.replace('\\', '/');
				File referFile = new File(Environment.getExternalStorageDirectory().getPath() +
						getResources().getString(R.string.root_path) +
						config.getCurrentScenarioFile() + referPath);
                Log.e(TAG, Environment.getExternalStorageDirectory().getPath() +
                        getResources().getString(R.string.root_path) +
                        config.getCurrentScenarioFile() + referPath);
				if (referFile.exists()) {
                    Log.e(TAG, "show refer");
					Timer timer = new Timer();
					ShowButtonsTask showButtonsTask = new ShowButtonsTask(referenceButton,
							soundReference);
					timer.schedule(showButtonsTask,1000);
					referenceButton.setVisibility(View.VISIBLE);
						//hintButton.setBackgroundResource(R.drawable.whisper1);
				}
			} else {
				referenceButton.setVisibility(View.GONE);
			}
        	
        	// logic for show/hide play video button...
        	String videoPath = ((ScenarioImage)currentDirectoryElement).getVideoFilePath();
        	if (videoPath != null && videoPath.length() != 0) {
        		videoPath = videoPath.replace("\\", "/");
        		File videoFile = new File(Environment.getExternalStorageDirectory().getPath() +
                        getResources().getString(R.string.root_path) +
                        config.getCurrentScenarioFile() + videoPath);
        		if (videoFile.exists()) {
        			videoController.setControlPanelVisibility(View.VISIBLE);
        		}
        	} else {
        		videoController.setControlPanelVisibility(View.INVISIBLE);
        	}
        	
        	//setBackgroundDrawable(currentDirectoryElement, pageImage);
           
        } else if (currentDirectoryElement instanceof HelperImage) {
        	hintButton.setVisibility(View.GONE);
        	
        	HelperImage helperElement = (HelperImage)currentDirectoryElement;
        	if (helperElement.getVideoFilePath() != null)
        		videoController.setControlPanelVisibility(View.VISIBLE);
        	else
        		videoController.setControlPanelVisibility(View.INVISIBLE);
        	
        	//setBackgroundDrawable(currentDirectoryElement, pageImage);
        } else if (currentDirectoryElement instanceof ReferenceImage) {
			//hintButton.setVisibility(View.GONE);

			ArrayList<ReferenceDirectory> referList = xmlParser.getReferenceDirectories();
			String tipPath = ((ReferenceImage)currentDirectoryElement).getTipPath();
			if (tipPath != null && tipPath.length() != 0) {
				tipPath = tipPath.replace('\\', '/');
				File tipFile = new File(Environment.getExternalStorageDirectory().getPath() +
						getResources().getString(R.string.root_path) +
						config.getCurrentScenarioFile() + tipPath);
				if (tipFile.exists()) {
					hintButton.setVisibility(View.VISIBLE);
					if(config.isReferHintUse(position)) {
					}
					else {
						Timer timer = new Timer();
						ShowButtonsTask showButtonsTask = new ShowButtonsTask(hintButton,
								soundHint);
						timer.schedule(showButtonsTask, 1000);
					}
				}
			} else {
				hintButton.setVisibility(View.GONE);
			}

			String videoPath = ((ReferenceImage)currentDirectoryElement).getVideoPath();
			if (videoPath != null && videoPath.length() != 0) {
				videoPath = videoPath.replace("\\", "/");
				File videoFile = new File(Environment.getExternalStorageDirectory().getPath() +
						getResources().getString(R.string.root_path) +
						config.getCurrentScenarioFile() + videoPath);
				if (videoFile.exists()) {
					videoController.setControlPanelVisibility(View.VISIBLE);
				}
			}  else {
				videoController.setControlPanelVisibility(View.INVISIBLE);
			}

			//setBackgroundDrawable(currentDirectoryElement, pageImage);
		} else if (currentDirectoryElement instanceof HintImage) {
		//hintButton.setVisibility(View.GONE);

			config.setMenuVisible(false);

		String videoPath = ((HintImage)currentDirectoryElement).getVideoPath();
		if (videoPath != null && videoPath.length() != 0) {
			videoPath = videoPath.replace("\\", "/");
			File videoFile = new File(Environment.getExternalStorageDirectory().getPath() +
					getResources().getString(R.string.root_path) +
					config.getCurrentScenarioFile() + videoPath);
			if (videoFile.exists()) {
				videoController.setControlPanelVisibility(View.VISIBLE);
			}
		}  else {
			videoController.setControlPanelVisibility(View.INVISIBLE);
		}

		//setBackgroundDrawable(currentDirectoryElement, pageImage);
	}

    }

	private Bitmap getBitmap(int index) {
		DirectoryElement elem = navigatorAdapter.getItem(index);
		String invertPath = elem.getPath().replace('\\', '/');
		String pagePath = Environment.getExternalStorageDirectory().getPath() +
				getResources().getString(R.string.root_path) +
				config.getCurrentScenarioFile() + invertPath;

		Log.d(TAG, ActivityBase.class.getSimpleName() + ": page path for drawable: " + pagePath);

		return BitmapFactory.decodeFile(pagePath);

	}

	private OnCompletionListener smallVideoCompletionListener = new OnCompletionListener() {
		public void onCompletion(MediaPlayer mp) {
			menuButton.setEnabled(true);
            mFlipper.setEnabled(true);

            videoController.handleOnCompletion();
		}
	};

	@Override
	protected void onResume() {
		super.onResume();
		if (config.getChecked()) {
			checkBtn.setVisibility(View.VISIBLE);
		} else {
            checkBtn.setVisibility(View.INVISIBLE);
        }
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (config.getChecked()) {
            checkBtn.setVisibility(View.VISIBLE);
        } else {
            checkBtn.setVisibility(View.INVISIBLE);
        }
	}

	class ShowButtonsTask extends TimerTask {

		private Button button;
		private MediaPlayer mediaPlayer;

		public ShowButtonsTask(Button _button, MediaPlayer _mediaPlayer) {
			this.button = _button;
			this.mediaPlayer = _mediaPlayer;
		}

		@Override
		public void run() {
			mediaPlayer.start();
		}
	}

    public void onPageChange(final int index) {
        currentPagePosition = index;
        runOnUiThread(new Runnable() {
            public void run() {
                setPageAt(index);
            }
        });
    }

	/**
	 * Bitmap provider.
	 */
	private class PageProvider implements CurlView.PageProvider {

        private SparseArray<Bitmap> mBitmaps = new SparseArray<Bitmap>();

		public int getPageCount() {
			return navigatorAdapter.getCount();
		}

		public void updatePage(CurlPage page, int width, int height, int index) {
            if (mBitmaps.get(index) == null || mBitmaps.get(index).isRecycled()) {
                mBitmaps.append(index, getBitmap(index));
            }

			// Create new bitmap to avoid issues
            page.setTexture(Bitmap.createBitmap(mBitmaps.get(index)), CurlPage.SIDE_BOTH);
            page.setColor(Color.argb(127, 255, 255, 255),
                    CurlPage.SIDE_BACK);
		}

        public double getBitmapHeight(int index, int width) {
            if (mBitmaps.get(index) == null || mBitmaps.get(index).isRecycled()) {
                mBitmaps.append(index, getBitmap(index));
            }

            return (double) mBitmaps.get(index).getHeight() * ((float) width / mBitmaps.get(index).getWidth());
        }
    }

}
