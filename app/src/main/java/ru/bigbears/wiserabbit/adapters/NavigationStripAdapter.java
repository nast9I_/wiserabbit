/**
 * 
 */
package ru.bigbears.wiserabbit.adapters;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import ru.bigbears.wiserabbit.Config;
import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.models.DirectoryElement;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * @author Vladimir
 *
 */
public class NavigationStripAdapter extends BaseAdapter {
    private static final String TAG = "My";
    
	private Context context;
	private Config config;
	
    int mGalleryItemBackground;

    // Use when user open new directory with images.
    // These variable save first position of new images array
    int mPosition = 0;

    private ArrayList<DirectoryElement> images;

    public NavigationStripAdapter(Context context, Config config) {
        this.context = context;
        this.config  = config;

        TypedArray attr = context.obtainStyledAttributes(R.styleable.NotepadGallery);
        mGalleryItemBackground = attr.getResourceId(R.styleable.NotepadGallery_android_galleryItemBackground, 0);
        attr.recycle();

        images = new ArrayList<DirectoryElement>();
    }

    public int getCount() {
        return images.size();
    }

    public DirectoryElement getItem(int position) {
        try {
            return images.get(position);
        } catch (Exception e) {
            return images.get(0);
        }
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = new ViewHolder();
        
        holder.layout = new RelativeLayout(context);
        holder.imageView = new ImageView(context);
        holder.numberText = new TextView(context);

        // get image object and read path where it replace
        String imagePath = images.get(position).getPath();
        imagePath = imagePath.replace('\\', '/');

        File file = new File(Environment.getExternalStorageDirectory().getPath() +
                context.getResources().getString(R.string.root_path) +
                config.getCurrentScenarioFile() +
                imagePath);
        
        //Log.d(TAG, "ImageAdapter: file path: " + file.getPath());
        String fileExtension = file.getPath().substring(file.getPath().lastIndexOf("."));
        
        Bitmap b = null;
        if (fileExtension.equals(".png") || fileExtension.equals(".jpg")) {
        	b = decodeFile(file);
        	holder.imageView.setImageBitmap(b);
        	
        } else if (fileExtension.equals(".3gp") || fileExtension.equals(".mp4")) {
        	b = null;
        	holder.imageView.setBackgroundColor(Color.BLACK);
        }

        holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        holder.imageView.setLayoutParams(new Gallery.LayoutParams(90, 72));
        holder.imageView.setBackgroundResource(mGalleryItemBackground);
         

        holder.numberText.setText(String.valueOf(position+1));
        holder.numberText.setTextColor(Color.WHITE);
        holder.numberText.setTextSize(40);
        holder.numberText.setPadding(24,12,0,0);

        holder.layout.addView(holder.imageView);
        holder.layout.addView(holder.numberText);

        return holder.layout;
    }

    public void addImages(ArrayList<DirectoryElement> newImages) {
        images.addAll(newImages);
        Log.e(TAG, "images size: " + images.size());
        
        // Exclude an images, that files were removed
        int removeOperationsCount = 0;
        List<DirectoryElement> elementsToRemove = new ArrayList<DirectoryElement>();
        for (int i = 0; i != images.size(); ++i) {
        	DirectoryElement element = images.get(i);
        	String imagePath = element.getPath();
            imagePath = imagePath.replace('\\', '/');

            File file = new File(Environment.getExternalStorageDirectory().getPath() +
                    context.getResources().getString(R.string.root_path) +
                    config.getCurrentScenarioFile() +
                    imagePath);
            if (!file.exists()) {
            	elementsToRemove.add(element);
            	++removeOperationsCount;
            }
        }
        
        images.removeAll(elementsToRemove);
        
        mPosition = this.images.size() - removeOperationsCount - newImages.size();
        Log.e(TAG, "images size: " + images.size());
    }

    // return position of first element of new added images
    public int getNewSelected() { return mPosition; }
    
	private Bitmap decodeFile(File f) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// The new size we want to scale to
			int REQUIRED_SIZE = 50;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_SIZE
					&& o.outHeight / scale / 2 >= REQUIRED_SIZE)
				scale *= 2;

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
			Log.e(TAG, "ImageAdapter: can't decode file from path: " + f.getPath());
		}
		return null;
	}
    
    static class ViewHolder {
    	RelativeLayout layout;
    	ImageView imageView;
        TextView numberText;
    }
}
