/**
 * 
 */
package ru.bigbears.wiserabbit.adapters;

import java.util.ArrayList;

import ru.bigbears.wiserabbit.Config;
import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.models.HelperDirectory;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Vladimir
 *
 */
public class HelperDashboardAdapter extends BaseAdapter {
	Context context;
	ArrayList<HelperDirectory> helperDirectories;
	LayoutInflater inflater;
	
	Config config;
	
	public HelperDashboardAdapter(Context c, ArrayList<HelperDirectory> names) {
		context = c;
		helperDirectories = names;
		inflater = LayoutInflater.from(context);
		
		config = new Config((Activity)c);
	}

	public int getCount() {
		return helperDirectories.size();
	}

	public HelperDirectory getItem(int position) {
		return helperDirectories.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		
		if (convertView == null) {
			holder = new ViewHolder();
			
			convertView = inflater.inflate(R.layout.icon, null);
			holder.itemName = (TextView)convertView.findViewById(R.id.icon_text);
			holder.itemImage = (ImageView)convertView.findViewById(R.id.icon_image);
			
			convertView.setTag(holder);
		} else
			holder = (ViewHolder)convertView.getTag();
		
		HelperDirectory directory = helperDirectories.get(position);
		holder.itemName.setText(directory.getName());
		
		String extension = directory.getName().substring(directory.getName().length());
		if (extension.equals(".3gp") || extension.equals(".mp4"))
			holder.itemImage.setImageResource(R.drawable.video_image);
		else {
			if (directory.getPassword() == null) {
				holder.itemImage.setImageResource(R.drawable.directiry_image);
			} else {
				if (!config.isPasswordUsedForHelperDirectory(directory.getName())) 
					holder.itemImage.setImageResource(R.drawable.directiry_image);
				else
					holder.itemImage.setImageResource(R.drawable.directiry_image_closed);
			}
		}
		
		return convertView;
	}

	private static class ViewHolder {
		TextView itemName;
		ImageView itemImage;
	}
}
