/**
 * 
 */
package ru.bigbears.wiserabbit.models;

/**
 * @author Vladimir
 * This model represent image from Unremove directory which 
 * not remove when delete date is coming.
 */
public class UnremoveDirectoryImage extends DirectoryElement {
	private String videoFilePath;
	
	public UnremoveDirectoryImage(String path, String videoFilePath) {
		super(path);
		
		this.videoFilePath = videoFilePath;
	}
	
	public String getVideoFilePath() {
		return videoFilePath;
	}
}
