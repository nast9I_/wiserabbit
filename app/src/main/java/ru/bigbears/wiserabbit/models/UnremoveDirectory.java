/**
 * 
 */
package ru.bigbears.wiserabbit.models;

import java.util.ArrayList;

/**
 * @author Vladimir
 * Undeletable directory which stay on sd-card when delete date is coming.
 */
public class UnremoveDirectory {
	private ArrayList<UnremoveDirectoryImage> mImages;
	
	public UnremoveDirectory(ArrayList<UnremoveDirectoryImage> images) {
		mImages = images;
	}
	
	public UnremoveDirectoryImage getImageAt(int index) {
		if (index >= 0 && index < mImages.size())
			return mImages.get(index);
		else 
			return null;
	}
	
	public int getCount() {
		return mImages.size();
	}
}
