package ru.bigbears.wiserabbit.models;

/**
 * Created by rad1ocat on 09.04.16.
 */
public class ReferenceImage extends DirectoryElement {

    private String mVideoPath;
    private String mTipPath;
    //private String mTipVideoPath;
    private int mTipMarks;
    private String mTipDescription;

    public ReferenceImage(String path, String videoFilePath, String tipPath,
                          String tipVideoPath, int tipMarks, String tipDescription) {
        super(path);

        mVideoPath = videoFilePath;
        mTipPath = tipPath;
        //mTipVideoPath = tipVideoPath;
        mTipMarks = tipMarks;
        mTipDescription = tipDescription;
    }

    public String getVideoPath() { return mVideoPath; }
    public String getTipPath() { return  mTipPath; }
    //public String getTipVideoPath() { return mTipVideoPath; }
    public int getTipMarks() { return mTipMarks; }
    public String getTipDescription() { return mTipDescription; }
}
