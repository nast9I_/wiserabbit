/**
 * 
 */
package ru.bigbears.wiserabbit.models;

import org.joda.time.DateTime;

/**
 * @author user
 * 
 */
public class RemovedDirectory {

	private long id;
	private String path;
	private DateTime dateToRemove;
	
	public RemovedDirectory(long id, String path, DateTime dateTime) {
		this.id = id;
		this.path = path;
		dateToRemove = dateTime;
	}
	
	public RemovedDirectory(String path, DateTime dateTime) {
		this.id = 0;
		this.path = path;
		dateToRemove = dateTime;
	}
	
	public long getId() { return id; }
	public String getPath() { return path; }
	public DateTime getDateToRemove() { return dateToRemove; }
}