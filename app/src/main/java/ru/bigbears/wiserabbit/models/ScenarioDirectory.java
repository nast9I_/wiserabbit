package ru.bigbears.wiserabbit.models;

import java.util.ArrayList;


public class ScenarioDirectory {
	// key for next content
	String mCode;
	
	// count of points added to player's result
	int mMarks;
	
	// array with scenario images in this directory
	private ArrayList<DirectoryElement> mImages;
	
	// messages for valid/invalid key than input player
	String mMessageValidCode = null;
	String mMessageInvalidCode = null;
	
	public ScenarioDirectory(String code, int marks, ArrayList<DirectoryElement> images, String validMess, String invalidMess) {
		mCode = code;
		mMarks = marks;
		
		mImages = images;
		
		mMessageValidCode = validMess;
		mMessageInvalidCode = invalidMess;
	}
	
	String failKey1 = null;
	String failMessage1 = null;
	public void setFailKey1(String key, String message) {
		failKey1 = key;
		failMessage1 = message;
	}
	public String getFailKey1() { return failKey1; }
	public String getFailMessage1() { return failMessage1; }
	
	String failKey2 = null;
	String failMessage2 = null;
	public void setFailKey2(String key, String message) {
		failKey2 = key;
		failMessage2 = message;
	}
	public String getFailKey2() { return failKey2; }
	public String getFailMessage2() { return failMessage2; }
	
	String failKey3 = null;
	String failMessage3 = null;
	public void setFailKey3(String key, String message) {
		failKey3 = key;
		failMessage3 = message;
	}
	public String getFailKey3() { return failKey3; }
	public String getFailMessage3() { return failMessage3; }
	
	String failKey4 = null;
	String failMessage4 = null;
	public void setFailKey4(String key, String message) {
		failKey4 = key;
		failMessage4 = message;
	}
	public String getFailKey4() { return failKey4; }
	public String getFailMessage4() { return failMessage4; }

	String failExit = null;
	public void setFailExit(String mFailExit) {
		failExit = mFailExit;
	}
	public String getFailExit() {
		return failExit;
	}
	
	public String getCode() { return mCode; }
	public int getMarks() { return mMarks; }
	
	public DirectoryElement getImage(int index) { 
		if (index > mImages.size())
			return null;
		else
			return mImages.get(index); 
	}
	
	public String getMessageValidCode() {return mMessageValidCode;}
	public String getMessageInvalidCode() {return mMessageInvalidCode;}
	
	public ArrayList<DirectoryElement> getImages() { return mImages; }
}
