package ru.bigbears.wiserabbit.models;

import java.util.ArrayList;

/**
 * Created by rad1ocat on 17.04.16.
 */
public class HintDirectory {

    private ArrayList<DirectoryElement> mImages;

    public HintDirectory(ArrayList<DirectoryElement> mImages) {
        this.mImages = mImages;
    }

    public ArrayList<DirectoryElement> getImages() {
        return mImages;
    }
}
