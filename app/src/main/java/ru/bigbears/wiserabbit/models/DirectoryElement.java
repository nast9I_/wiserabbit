/**
 * 
 */
package ru.bigbears.wiserabbit.models;

/**
 * @author Vladimir
 *
 */
public abstract class DirectoryElement {
	
	protected String mPath;
	
	public DirectoryElement(String path) {
		mPath = path;
	}
	
	public String getPath() { return mPath; }
}
