package ru.bigbears.wiserabbit.models;

/**
 * Created by rad1ocat on 17.04.16.
 */
public class HintImage extends DirectoryElement {

    private String mVideoPath;

    public HintImage(String path, String video) {
        super(path);

        mVideoPath = video;
    }

    public String getVideoPath() {
        return mVideoPath;
    }
}
