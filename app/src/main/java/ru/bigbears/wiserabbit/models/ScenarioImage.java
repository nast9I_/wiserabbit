package ru.bigbears.wiserabbit.models;

public class ScenarioImage extends DirectoryElement {
	
	private String mTipPath;
	//private String mTipVideoPath;
	private String mVideoFilePath;
	private String mReferencePath;
	private String mReferenceVideoPath;
	//private String mReferenceFilePath;
	private int mTipMarks;
	private boolean mScale;
	private String mTipDescription;
	
	public ScenarioImage(String path, String videoFilePath, String tipPath,
						  int tipMarks, String tipDescription, String referencePath, String referenceVideoPath, boolean scale) {
		super(path);

		if ( !mPath.substring(0, 1).equals("\\") ) {
			mPath = "\\" + mPath;
		}

		mReferencePath = referencePath;
		mReferenceVideoPath = referenceVideoPath;
		mVideoFilePath = videoFilePath;
		mTipPath = tipPath;
		//mTipVideoPath = tipVideoPath;
		mTipMarks = tipMarks;
		mTipDescription = tipDescription;
		mScale = scale;
	}
	
	public String getTipPath() { return mTipPath; }
	//public String getTipVideoPath() { return mTipVideoPath; }
	public String getVideoFilePath() { return mVideoFilePath; }
	public String getReferencePath() { return mReferencePath; }
	//public String getReferenceFilePath() { return mReferencePath; }
	public String getReferenceVideoPath() { return mReferenceVideoPath; }
	public int getTipMarks() { return mTipMarks; }
	public boolean isScaled() { return mScale; }
	public String getTipDescription() { return mTipDescription; }
}
