/**
 * 
 */
package ru.bigbears.wiserabbit.models;

/**
 * @author Vladimir
 *
 */
public class AdditionalCode {

	private String code;
	private String message;
	private int mark;
	private boolean isAnswered;
	
	public AdditionalCode(String code, String message, int mark, boolean isAnswered) {
		this.code = code;
		this.message = message;
		this.mark = mark;
		this.isAnswered = isAnswered;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

	public int getMark() {
		return mark;
	}
	
	public boolean isAnswered() {
		return isAnswered;
	}
}
