/**
 * 
 */
package ru.bigbears.wiserabbit.models;

/**
 * @author Vladimir
 *
 */
public class HelperDirectory {

	private String name;
	private String password;
	
	public HelperDirectory(String directoryName, String directoryPassword) {
		name = directoryName;
		password = directoryPassword;
	}
	
	public String getName() {
		return name;
	}
	
	public String getPassword() {
		return password;
	}
}
