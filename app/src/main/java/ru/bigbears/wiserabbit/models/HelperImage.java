package ru.bigbears.wiserabbit.models;

public class HelperImage extends DirectoryElement {
	private String mVideoFilePath;
	private boolean mScaled;
	
	public HelperImage() {
		super(null);
		
		mVideoFilePath = null;
		mScaled = false;
	}
	
	public HelperImage(String path, String videoFilePath, boolean scaled) {
		super(path);
		
		mVideoFilePath = videoFilePath;
		mScaled = scaled;
	}
	
	public String getVideoFilePath() { return mVideoFilePath; }
	public boolean isScaled() { return mScaled; }
}
