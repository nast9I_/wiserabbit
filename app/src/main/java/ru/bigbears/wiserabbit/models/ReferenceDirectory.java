package ru.bigbears.wiserabbit.models;

import java.util.ArrayList;

/**
 * Created by rad1ocat on 09.04.16.
 */
public class ReferenceDirectory {
    // key for next content
    String mCode;

    // array with scenario images in this directory
    private ArrayList<DirectoryElement> mImages;

    // count of points added to player's result
    int mMarks;

    // messages for valid/invalid key than input player
    String mMessageValidCode = null;
    String mMessageInvalidCode = null;

    public ReferenceDirectory(String code, ArrayList<DirectoryElement> images, int marks, String messageValidCode,
                              String messageInvalidCode) {
        mCode = code;
        mImages = images;
        mMarks = marks;
        mMessageValidCode = messageValidCode;
        mMessageInvalidCode = messageInvalidCode;
    }

    public String getCode() { return mCode; }

    public DirectoryElement getImage(int index) {
        if (index > mImages.size())
            return null;
        else
            return mImages.get(index);
    }

    public int getMarks() { return mMarks; }

    public String getMessageValidCode() {return mMessageValidCode;}
    public String getMessageInvalidCode() {return mMessageInvalidCode;}

    public ArrayList<DirectoryElement> getImages() { return mImages; }
}
