package ru.bigbears.wiserabbit.keyboard;

public class KeyboardKey {
	String defaultKeyPath;
	String pressedKeyPath;
	
	public KeyboardKey(String defaultPath, String pressedPath) {
		defaultKeyPath = defaultPath;
		pressedKeyPath = pressedPath;
	}
	
	public String getDefaultPath() { return defaultKeyPath; }
	public String getPressedPath() { return pressedKeyPath; }
}
