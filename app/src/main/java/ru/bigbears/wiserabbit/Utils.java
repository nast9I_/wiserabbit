package ru.bigbears.wiserabbit;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by runpolkano on 12-Dec-16.
 */
public class Utils {

    public enum Font {MINION_PRO, ARIAL_NARROW, LITHOS_PRO, PROTO_SANS}

    public static Typeface getTypeface(Context ctx, Font id) {
        String path;
        switch (id) {
            case MINION_PRO:
                path = "fonts/MinionPro_Regular.otf";
                break;
            case ARIAL_NARROW:
                path = "fonts/Arial_Narrow.ttf";
                break;
            case LITHOS_PRO:
                path = "fonts/LithosPro_Regular.otf";
                break;
            case PROTO_SANS:
                path = "fonts/proto_sans_56.otf";
                break;
            default:
                path = "";
        }
        return Typeface.createFromAsset(ctx.getAssets(), path);
    }
}
