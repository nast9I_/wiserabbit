/**
 * 
 */
package ru.bigbears.wiserabbit.dialogs;

import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.Utils;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * @author Vladimir
 *
 */
public class InputPasswordDialog extends DialogFragment {
	public static final String TAG = InputPasswordDialog.class.getSimpleName();
	
	public interface OnPasswordInputedListener {
		public void onPasswordInputed(String password);
	}
	
	private OnPasswordInputedListener onPasswordInputedListener;
	
	private EditText editPassword;
	private Button button;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(STYLE_NO_TITLE, R.style.Dialog_Theme_NoBackground);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.dialog_input_password, container, false);

        TextView tv = (TextView) view.findViewById(R.id.tvMessage);
        tv.setTypeface(Utils.getTypeface(getActivity(), Utils.Font.PROTO_SANS));

		editPassword = (EditText)view.findViewById(R.id.editPassword);

		button = (Button)view.findViewById(R.id.btnOK);
        button.setTypeface(Utils.getTypeface(getActivity(), Utils.Font.MINION_PRO));
		button.setOnClickListener(new OnClickListener() {

            public void onClick(View view) {
                String password = editPassword.getText().toString();
                onPasswordInputedListener.onPasswordInputed(password);
                dismiss();
            }
        });

		button = (Button)view.findViewById(R.id.btnCancel);
        button.setTypeface(Utils.getTypeface(getActivity(), Utils.Font.MINION_PRO));
		button.setOnClickListener(new OnClickListener() {

            public void onClick(View view) {
                dismiss();
            }
        });
		
		return view;
	}
	
	public void setOnPasswordInputed(OnPasswordInputedListener listener) {
		onPasswordInputedListener = listener;
	}
}
