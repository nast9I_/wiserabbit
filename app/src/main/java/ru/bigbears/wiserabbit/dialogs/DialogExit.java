/**
 * 
 */
package ru.bigbears.wiserabbit.dialogs;

import ru.bigbears.wiserabbit.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * @author Vladimir
 *
 */
public class DialogExit {
	
	public static void show(final Activity activity) {

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(R.string.exit_message)
				.setCancelable(false)
				.setIcon(0)
				.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						activity.finish();
					}
				})
				.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
		});

		AlertDialog dialog = builder.create();
		dialog.show();
	}
}