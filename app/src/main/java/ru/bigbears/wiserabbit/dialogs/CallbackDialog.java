package ru.bigbears.wiserabbit.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import ru.bigbears.wiserabbit.R;

/**
 * Created by User on 28.06.2016.
 */
public class CallbackDialog extends DialogFragment {
    public static final String TAG = CallbackDialog.class.getSimpleName();

    public interface OnCallbackInputedListener {
        public void onCallbackInputed(String callback, float rating);
    }

    private OnCallbackInputedListener onCallbackInputedListener;

    private EditText editCallback;
    private RatingBar ratingBar;
    private Button btnSend;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.Dialog_Theme_NoBackground);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_callback, container, false);

        ratingBar = (RatingBar)view.findViewById(R.id.ratingBar);
        editCallback = (EditText)view.findViewById(R.id.editCallback);

        btnSend = (Button)view.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                float rating = ratingBar.getRating();
                String callback = editCallback.getText().toString();
                onCallbackInputedListener.onCallbackInputed(callback,rating);
                dismiss();
            }
        });

        return view;
    }

    public void setOnCallbackInputed(OnCallbackInputedListener listener) {
        onCallbackInputedListener = listener;
    }
}
