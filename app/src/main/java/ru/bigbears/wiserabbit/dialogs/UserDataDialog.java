package ru.bigbears.wiserabbit.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import ru.bigbears.wiserabbit.R;

/**
 * Created by User on 19.06.2016.
 */
public class UserDataDialog extends DialogFragment {

    public static final String TAG = UserDataDialog.class.getSimpleName();

    public interface OnUserDataInputedListener {
        public void onUserDataInputed(String email, String login);
    }

    private OnUserDataInputedListener onUserDataInputedListener;

    private EditText editEmail;
    private EditText editLogin;
    private Button btnOK;
    private Button btnCancel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.Dialog_Theme_NoBackground);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_user_data, container, false);

        editEmail = (EditText)view.findViewById(R.id.editEmail);
        editLogin = (EditText)view.findViewById(R.id.editLogin);

        btnOK = (Button)view.findViewById(R.id.btn_ok);
        btnCancel = (Button)view.findViewById(R.id.btn_cancel);

        btnOK.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String email = editEmail.getText().toString();
                String login = editLogin.getText().toString();
                onUserDataInputedListener.onUserDataInputed(email,login);
                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                dismiss();
            }
        });

        return view;
    }

    public void setOnUserDataInputed(OnUserDataInputedListener listener) {
        onUserDataInputedListener = listener;
    }
}
