package ru.bigbears.wiserabbit.dialogs;

import android.content.res.Resources;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import ru.bigbears.wiserabbit.R;

/**
 * Created by User on 27.06.2016.
 */
public class LicenseDialog extends DialogFragment {

    public static final String TAG = LicenseDialog.class.getSimpleName();

    public interface OnLicenseListener {
        public void onLicenseInputed();
    }

    private OnLicenseListener onLicenseListener;

    private TextView licText;
    private Button btnAccept;
    private Button btnCancel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.Dialog_Theme_NoBackground);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_license, container, false);

        licText = (TextView) view.findViewById(R.id.licText);

        btnAccept = (Button)view.findViewById(R.id.btnAccept);
        btnCancel = (Button)view.findViewById(R.id.btnCancel);

        //String licenseText = openLicFile();
        //licText.setText(licenseText);

        btnAccept.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                onLicenseListener.onLicenseInputed();
                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                dismiss();
            }
        });

        return view;
    }

    public void setOnLicenseInputed(OnLicenseListener listener) {
        onLicenseListener = listener;
    }

}
