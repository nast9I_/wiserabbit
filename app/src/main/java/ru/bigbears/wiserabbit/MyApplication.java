package ru.bigbears.wiserabbit;

import com.crittercism.app.Crittercism;

import android.app.Application;

public class MyApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		Crittercism.initialize(getApplicationContext(), "54b0ef6351de5e9f042ec9fc");
	}
}
