package ru.bigbears.wiserabbit;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

public class Decompress { 
	  private final static int BUFFER_SIZE = 4*1024;
	  
	  public static void unzip(String zipFile, String location) throws IOException {
		    int size;
		    byte[] buffer = new byte[BUFFER_SIZE];

		    try {
		        File f = new File(location);
		        if(!f.isDirectory()) {
		            f.mkdirs();
		        }
		        ZipInputStream zin = new ZipInputStream(new BufferedInputStream(new FileInputStream(zipFile), BUFFER_SIZE));
		        try {
		            ZipEntry ze = null;
		            while ((ze = zin.getNextEntry()) != null) {
		                String path = location + ze.getName();

		                if (ze.isDirectory()) {
		                    File unzipFile = new File(path);
		                    if(!unzipFile.isDirectory()) {
		                        unzipFile.mkdirs();
		                    }
		                }
		                else {
		                    FileOutputStream out = new FileOutputStream(path, false);
		                    
		                    BufferedOutputStream fout = new BufferedOutputStream(out, BUFFER_SIZE);
		                    try {
		                        while ( (size = zin.read(buffer, 0, BUFFER_SIZE)) != -1 ) {
		                        	
		                            fout.write(buffer, 0, size);
		                            //bw.write(buffer, 0, size);
		                        }

		                        zin.closeEntry();
		                    }
		                    finally {
		                        fout.flush();
		                        fout.close();
		                    }
		                }
		            }
		        }
		        finally {
		            zin.close();
		        }
		    }
		    catch (Exception e) {
		        e.printStackTrace();
		    }
		}
	  
	  public static void unzipFile(String zipFileName, String location) {
		  try {
				// Initiate ZipFile object with the path/name of the zip file.
				ZipFile zipFile = new ZipFile(zipFileName);
				
				// Extracts all files to the path specified
				zipFile.extractAll(location);
				
			} catch (ZipException e) {
				e.printStackTrace();
			}
	  }
	} 