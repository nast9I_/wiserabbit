/**
 * 
 */
package ru.bigbears.wiserabbit.animations;

import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;

/**
 * @author Vladimir
 *
 */
public class VideoViewAnimation extends Animation {

	public VideoViewAnimation() {}
	
	@Override
	public void initialize(int width, int height, int parentWidth, int parentHeight) {
		super.initialize(width, height, parentWidth, parentHeight);
		
		setDuration(1500);
		setFillAfter(false);
		setInterpolator(new LinearInterpolator());
	}
	
	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		super.applyTransformation(interpolatedTime, t);
		
		final Matrix matrix = t.getMatrix();
		matrix.setScale(interpolatedTime, interpolatedTime);
		//matrix.preTranslate(-centerX, -centerY);
	}
}
