/**
 * 
 */
package ru.bigbears.wiserabbit.consts;

/**
 * @author Vladimir
 * Represent structure of pak-file. Constants with directory names.
 */
public class ScenarioStructure {

	public static final String SCENARIO_DIRECTORY = "Scenario";
	public static final String HELPER_DIRECTORY   = "Helper";
	public static final String REFERENCE_DIRECTORY   = "Reference";
	public static final String KEYBOARD_DIRECTORY = "Keyboard";
	public static final String HINT_DIRECTORY = "Hint";
	public static final String FAIL_EXIT_DIRECTORY = "FailExit";
	public static final String UNREMOVE_DIRECTORY = "Unremove";
}
