package ru.bigbears.wiserabbit.popups;

import java.io.File;

import ru.bigbears.wiserabbit.R;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

public class PopupVideoPlayer extends PopupSimple {
	
	private VideoView mVideoView;
	
	private File mVideoFile;
	
	public PopupVideoPlayer(Context context, int width, int height, File videoFile) {
		super(context, width, height);
		
		mVideoFile = videoFile;
		
		LayoutInflater inflater = LayoutInflater.from(context);
		View layout = inflater.inflate(R.layout.popup_video_player, null);
		
		mVideoView = (VideoView)layout.findViewById(R.id.VideoView);
		
		popup.setContentView(layout);
	}
	
	public void show(View anchor, int offsetX, int offsetY) {
		popup.showAtLocation(anchor, Gravity.CENTER, offsetX, offsetY);
		
		mVideoView.setVideoPath(mVideoFile.getPath());
	    mVideoView.setMediaController(new MediaController(context));
		mVideoView.requestFocus();
	    mVideoView.start();
	}
}