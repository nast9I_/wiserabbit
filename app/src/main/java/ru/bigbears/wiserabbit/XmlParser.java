package ru.bigbears.wiserabbit;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ru.bigbears.wiserabbit.models.AdditionalCode;
import ru.bigbears.wiserabbit.models.DirectoryElement;
import ru.bigbears.wiserabbit.models.HelperDirectory;
import ru.bigbears.wiserabbit.models.HelperImage;
import ru.bigbears.wiserabbit.models.HintDirectory;
import ru.bigbears.wiserabbit.models.HintImage;
import ru.bigbears.wiserabbit.models.ReferenceDirectory;
import ru.bigbears.wiserabbit.models.ReferenceImage;
import ru.bigbears.wiserabbit.models.ScenarioDirectory;
import ru.bigbears.wiserabbit.models.ScenarioImage;
import ru.bigbears.wiserabbit.models.UnremoveDirectoryImage;
import ru.bigbears.wiserabbit.models.VideoElement;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class XmlParser {
	String mXmlFile;
	Document mDocument;
	
	Element mHelperNode;
	Element mScenarioNode;
	Element mUnremoveNode;
	Element mReferenceNode;
	Element mHintNode;
	Element mFailExitNode;
	Element mPasswordNode;
	Element mExpeditionIdNode;
	Element mKeyboardNode;
	Element mAdditionalCodeNode;
	Element removedByCodeNode;
	ArrayList<Node> mScenarioDirectories;
	ArrayList<Node> mHelperDirectories;
	ArrayList<Node> mReferenceDirectories;
	ArrayList<Node> mHintDirectories;
	ArrayList<Node> mFailDirectories;
	ArrayList<Node> mUnremoveDirectory;

	final static String XML_TAG_HELPER 			= "Helper";
	final static String XML_TAG_SCENARIO 		= "Scenario";
	final static String XML_TAG_EXPEDITION_ID	= "ExpeditionId";
	final static String XML_TAG_PASSWORD        = "Password";
	final static String XML_TAG_UNREMOVE		= "Unremove";
	final static String XML_TAG_DIRECTORY 	    = "Directory";
	final static String XML_TAG_IMAGE 		    = "Image";
	final static String XML_TAG_VIDEO 		    = "Video";
	final static String XML_TAG_ADDITIONAL_CODES = "AdditionalCodes";
	final static String XML_TAG_REMOVED_BY_CODE	= "RemovedByCode";
	final static String XML_TAG_REFERENCE       = "Reference";
	final static String XML_TAG_HINT            = "Hint";
	final static String XML_TAG_FAIL_EXIT       = "FailExit";
	
	// keyboard tags
	final static String XML_KEYBOARD	   = "Keyboard";
	final static String XML_DEFAULT		   = "Default";
	final static String XML_PRESSED		   = "Pressed";
	final static String XML_KEY 		   = "Key";
	final static String XML_ATTRIBUTE_PATH = "Path";
	
	final static String XML_ATTRIBUTE_NAME      			= "Name";
	final static String XML_ATTRIBUTE_FULL_PATH 			= "FullPath";
	final static String XML_ATTRIBUTE_VIDEO_FILE_PATH 		= "VideoFilePath";
	final static String XML_ATTRIBUTE_HINT_VIDEO_FILE_PATH 	= "VideoFilePath";
	final static String XML_ATTRIBUTE_TIP_PATH  			= "TipPath";
	final static String XML_ATTRIBUTE_TIP_DESCRIPTION  		= "TipDescription";
	final static String XML_ATTRIBUTE_REFER_PATH            = "ReferPath";
	final static String XML_ATTRIBUTE_MARKS_BAN 			= "MarksBan";
	final static String XML_ATTRIBUTE_MARKS     			= "Marks";
	final static String XML_ATTRIBUTE_SCALED    			= "Scaled";
	final static String XML_ATTRIBUTE_CODE      			= "Code";
    final static String XML_ATTRIBUTE_NOTEPAD_NAME 			= "NotepadName";
	final static String XML_ATTRIBUTE_REFERENCE_NAME 		= "ReferenceName";
	final static String XML_ATTRIBUTE_HINT_NAME             = "HintName";
    final static String XML_ATTRIBUTE_REMOVE_DATE 			= "RemoveDate";
    final static String XML_ATTRIBUTE_HOURS_TO_REMOVE		= "HoursToRemove";
	final static String XML_ATTRIBUTE_HOURS_FOR_GAME		= "HoursForGame";
    final static String XML_ATTRIBUTE_VALID_CODE 			= "CodeValidMessage";
    final static String XML_ATTRIBUTE_INVALID_CODE 			= "CodeInvalidMessage";
	final static String XML_ATTRIBUTE_REFERENCE_VIDEO_FILE_PATH = "ReferenceVideoFilePath";
    
    final static String XML_ATTRIBUTE_FAIL_KEY1 	= "FailKey1";
    final static String XML_ATTRIBUTE_FAIL_KEY2 	= "FailKey2";
    final static String XML_ATTRIBUTE_FAIL_KEY3 	= "FailKey3";
    final static String XML_ATTRIBUTE_FAIL_KEY4 	= "FailKey4";
    
    final static String XML_ATTRIBUTE_FAIL_MESSAGE1 = "FailMessage1";
    final static String XML_ATTRIBUTE_FAIL_MESSAGE2 = "FailMessage2";
    final static String XML_ATTRIBUTE_FAIL_MESSAGE3 = "FailMessage3";
    final static String XML_ATTRIBUTE_FAIL_MESSAGE4 = "FailMessage4";

	final static String XML_ATTRIBUTE_FAIL_EXIT = "FailExit";
    
    // names of parameters related with additional codes for OPRICL
    private static final String XML_ATTR_CODE_CODE = "Code";
    private static final String XML_ATTR_CODE_MARK = "Mark";
    private static final String XML_ATTR_CODE_MESSAGE = "Message";
    private static final String XML_ATTR_IS_ANSWERED = "Answered";
	
	final static String TAG = "My";

	private Context context;
	private Config config;
	
    boolean mIsXmlExist = false;
	
	public XmlParser(Context context, Config config, String xmlFileName) {
		this.context = context;
		this.config = config;
		mXmlFile = xmlFileName;
		mDocument = null;
		
		mIsXmlExist = setUp();
	}

    public boolean isXmlExist() { return mIsXmlExist; }
	
	// init main variables
	private boolean setUp() {
		// init document for working
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		
		try {
			DocumentBuilder builder = dbf.newDocumentBuilder();
			
			FileInputStream f = new FileInputStream(mXmlFile);
			mDocument = builder.parse(f);
			
		} catch (IOException e) {
			Log.e("FotoPad ERROR", "File not found!");
			return false;
		} catch (SAXException e) {
			
			return false;
		} catch (ParserConfigurationException e) {
			
			return false;
		}
		
		// get Scenario node and childs
		NodeList scenarioTag = mDocument.getElementsByTagName(XML_TAG_SCENARIO);
		mScenarioNode = (Element) scenarioTag.item(0);
		mScenarioDirectories = this.getChilds(mScenarioNode);
		
		// get Helper node and childs
		NodeList helperTag = mDocument.getElementsByTagName(XML_TAG_HELPER);
		mHelperNode = (Element) helperTag.item(0);	
		mHelperDirectories = this.getChilds(mHelperNode);

		// get Reference node and childs
		NodeList referenceTag = mDocument.getElementsByTagName(XML_TAG_REFERENCE);
		mReferenceNode = (Element) referenceTag.item(0);
		mReferenceDirectories = this.getChilds(mReferenceNode);

		// get Hint node and childs
		NodeList hintTag = mDocument.getElementsByTagName(XML_TAG_HINT);
		mHintNode = (Element)hintTag.item(0);
		mHintDirectories = this.getChilds(mHintNode);

		NodeList failExitTag = mDocument.getElementsByTagName(XML_TAG_FAIL_EXIT);
		mFailExitNode = (Element)failExitTag.item(0);
		mFailDirectories = this.getChilds(mFailExitNode);

		NodeList unremoveTag = mDocument.getElementsByTagName(XML_TAG_UNREMOVE);
		mUnremoveNode = (Element)unremoveTag.item(0);
		mUnremoveDirectory = this.getChilds(mUnremoveNode);
		
		NodeList removedByCodeTag = mDocument.getElementsByTagName(XML_TAG_REMOVED_BY_CODE);
		removedByCodeNode = (Element) removedByCodeTag.item(0);
		
		// get Password node
		NodeList passwordTag = mDocument.getElementsByTagName(XML_TAG_PASSWORD);
		mPasswordNode = (Element) passwordTag.item(0);
		
		// get Expedition id node
		NodeList expeditionIdTags = mDocument.getElementsByTagName(XML_TAG_EXPEDITION_ID);
		mExpeditionIdNode = (Element) expeditionIdTags.item(0);
		
		// get keyboard node
		NodeList keyboardList = mDocument.getElementsByTagName(XML_KEYBOARD);
		if (keyboardList != null)
			mKeyboardNode = (Element) keyboardList.item(0);
		else
			mKeyboardNode = null;
		
		NodeList additionalCodeTags = mDocument.getElementsByTagName(XML_TAG_ADDITIONAL_CODES);
		if (additionalCodeTags != null)
			mAdditionalCodeNode = (Element) additionalCodeTags.item(0);
		
		return true;
	}
	
	public ArrayList<String> getDefaultKeys() {
		ArrayList<String> keys = new ArrayList<String>();
		
		// if information about keyboard exist...
		if (mKeyboardNode != null) {
			NodeList nodes = mKeyboardNode.getElementsByTagName(XML_DEFAULT);
			if (nodes != null) {
				Element defaultElement = (Element)nodes.item(0);

                if (defaultElement != null) {
                    NodeList defaults = defaultElement.getElementsByTagName(XML_KEY);
                    for (int i = 0; i != defaults.getLength(); ++i) {
                        Node node = defaults.item(i);

                        String s = node.getAttributes().getNamedItem(XML_ATTRIBUTE_PATH).getTextContent();
                        keys.add(s);
                    }
                } else {
                    return null;
                }
			} else 
				return null;
		} else
			return null;
		
		return keys;
	}
	
	public ArrayList<String> getPressedKeys() {
		ArrayList<String> keys = new ArrayList<String>();
		
		// if information about keyboard exist...
		if (mKeyboardNode != null) {
			NodeList nodes = mKeyboardNode.getElementsByTagName(XML_PRESSED);
			if (nodes != null) {
				Element pressedElement = (Element)nodes.item(0);

                if (pressedElement != null) {
				NodeList presseds = pressedElement.getChildNodes();
                    for (int i = 0; i != presseds.getLength(); ++i) {
                        Node node = presseds.item(i);

                        String s = node.getAttributes().getNamedItem(XML_ATTRIBUTE_PATH).getNodeValue();
                        keys.add(s);
                    }
                } else {
                    return null;
                }
			} else 
				return null;
		} else
			return null;
		
		return keys;
	}
	
	/**
	 * Return password of current xml file
	 * @return - passworn in string
	 */
	public String getPassword() { return mPasswordNode.getTextContent(); }
	
	public int getExpeditionId() {
		int id = Integer.parseInt(mExpeditionIdNode.getTextContent());
		return id;
	}

    public String getNotepadName() {
    	String s = "";
    	try {
    		s = mScenarioNode.getAttributes().getNamedItem(XML_ATTRIBUTE_NOTEPAD_NAME).getNodeValue();
    	} catch (NullPointerException e) {
    		return "";
    	}

        return s;
    }

	public String getReferenceName() {
		String s = "";
		try {
			s = mReferenceNode.getAttributes().getNamedItem(XML_ATTRIBUTE_REFERENCE_NAME).getNodeValue();
		} catch (NullPointerException e) {
			return "";
		}

		return s;
	}

	public String getHintName() {
		String s = "";
		try {
			s = mHintNode.getAttributes().getNamedItem(XML_ATTRIBUTE_HINT_NAME).getNodeValue();
		} catch (NullPointerException e) {
			return "";
		}

		return s;
	}
    
    /**
     * Provide remove date from xml in dd.MM.yyyy format
     * @return - string with date
     */
    public String getRemoveDate() {
    	String s = null;
    	try {
    		s = mScenarioNode.getAttributes().getNamedItem(XML_ATTRIBUTE_REMOVE_DATE).getNodeValue();
    	} catch (NullPointerException e) {
    		return null;
    	}
    	
    	return s;
    }
    
    public int getHoursToRemove() {
    	try {
    		int hours = Integer.parseInt(mScenarioNode.getAttributes().getNamedItem(XML_ATTRIBUTE_HOURS_TO_REMOVE).getNodeValue());
    		return hours;
    	} catch (Exception e) {
    		return 0;
    	}
    }

	public int getHoursForGame() {
		try {
			int gameHours = Integer.parseInt(mScenarioNode.getAttributes().getNamedItem(XML_ATTRIBUTE_HOURS_FOR_GAME).getNodeValue());
			return gameHours;
		} catch (Exception e) {
			return 0;
		}
	}

	// Get child directories from Helper tag in xml. Save them in ArrayList
	private ArrayList<Node> getChilds(Node node) {
		ArrayList<Node> nodes = new ArrayList<Node>();
		
		try {
			NodeList childs = node.getChildNodes();
		
			for (int i = 0; i != childs.getLength(); ++i) {
				Node n = childs.item(i);
			
				if (n.getNodeName().equals(XML_TAG_DIRECTORY) || n.getNodeName().equals(XML_TAG_VIDEO))
					nodes.add( n );
			}
		
			return nodes;
		} catch(Exception e) {
			return null;
		}
	}

	/**
	 * Get names of helper directories
	 * @return - string array
	 */
	public ArrayList<HelperDirectory> getHelperDirectoryNames() {
		ArrayList<HelperDirectory> names = new ArrayList<HelperDirectory>();
		
		String name = null;
		String password = null;
		
		for (int i = 0; i != mHelperDirectories.size(); ++i) {
			name = mHelperDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_NAME).getNodeValue();
			try {
				password = mHelperDirectories.get(i).getAttributes().getNamedItem(XML_TAG_PASSWORD).getNodeValue();
			} catch (Exception e) {
				password = null;
			}
			
			names.add(new HelperDirectory(name, password)); 
		}
		
		if (names.size() == 0)
			return null;
		else
			return names;
	}

	// get image nodes from helper directory with index
	private ArrayList<Node> getDirectoryFiles(ArrayList<Node> dirs, int directoryIndex) {
		ArrayList<Node> nodes = new ArrayList<Node>();
		
		NodeList childs = null;
		try {
			childs = dirs.get(directoryIndex).getChildNodes();
		} catch (Exception e) {
			return null;
		}
		
		for (int i = 0; i != childs.getLength(); ++i) {
			Node n = childs.item(i);
			
			if (n.getNodeName().equals(XML_TAG_IMAGE)) //|| n.getNodeName().equals(XML_TAG_VIDEO))
				nodes.add(n);
		}
		
		return nodes;
	}
	
	/**
	 * Get all images nodes from directory with index
	 * @param directoryIndex - index of directory in array with helper directories
	 * @return - array list with HelperImage objects
	 */
	public ArrayList<DirectoryElement> getFilesFromHelperDirectory(int directoryIndex) {
		if (directoryIndex > mHelperDirectories.size())
			return null;
		
		ArrayList<DirectoryElement> directoryElements = new ArrayList<DirectoryElement>();
		
		// get child nodes from directory with index from parameter
		ArrayList<Node> childs = this.getDirectoryFiles(mHelperDirectories, directoryIndex);
		
		for (int i = 0; i != childs.size(); ++i) {
			String path = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FULL_PATH).getNodeValue();
			
			// if node is image then create HelperImage object...
			if (childs.get(i).getNodeName().equals(XML_TAG_IMAGE)) {
			
				// try get video file path..
				String videoFilePath = null;
				try {
					videoFilePath = childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_VIDEO_FILE_PATH).getNodeValue();
				} catch (Exception e) {
					videoFilePath = null;
				}
			
				boolean scaled = false;
				try {
					scaled = Boolean.parseBoolean( childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_SCALED).getNodeValue() );
				} catch (Exception e) {
					scaled = false;
				}
			
				directoryElements.add(new HelperImage(path, videoFilePath, scaled));
				
			} else if (childs.get(i).getNodeName().equals(XML_TAG_VIDEO)) 
				directoryElements.add(new VideoElement(path));
		}
		
		return directoryElements;
	}

	/**
	 * Get all directory in Scenario xml tag
	 * @return - array with ScenarioDirectory objects
	 */
	public ArrayList<ReferenceDirectory> getReferenceDirectories() {
		ArrayList<ReferenceDirectory> dirs = new ArrayList<ReferenceDirectory>();

		for (int i = 0; i != mReferenceDirectories.size(); ++i) {
			String code = "";
			try {
				code = mReferenceDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_CODE).getNodeValue();
			} catch (Exception e) {
				code = "";
			}

			int marks = 0;
			try {
				marks = Integer.parseInt( mReferenceDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_MARKS).getNodeValue() );
			} catch (Exception e) {
				marks = 0;
			}

			// read messages on key input...
			String messageValid = null;
			try {
				messageValid = mReferenceDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_VALID_CODE).getNodeValue();
				Log.d("My", "Valid: " + messageValid);
			} catch (Exception e) {
				messageValid = null;
			}

			String messageInvalid = null;
			try {
				messageInvalid = mReferenceDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_INVALID_CODE).getNodeValue();
				Log.d("My", "Invalid: " + messageInvalid);
			} catch (Exception e) {
				messageInvalid = null;
			}

			ArrayList<DirectoryElement> images = this.getImagesFromReferenceDirectory(i);

			ReferenceDirectory directory = new ReferenceDirectory(code, images, marks, messageValid, messageInvalid);

			dirs.add(directory);
		}

		return dirs;
	}

	public ArrayList<HintDirectory> getHintDirectories() {
		ArrayList<HintDirectory> dirs = new ArrayList<HintDirectory>();

		for (int i = 0; i != mHintDirectories.size(); ++i) {

			ArrayList<DirectoryElement> images = this.getImagesFromHintDirectory(i);

			HintDirectory directory = new HintDirectory(images);

			dirs.add(directory);
		}

		return dirs;
	}

	// get images from reference directory by index
	private ArrayList<DirectoryElement> getImagesFromReferenceDirectory(int directoryIndex) {
		ArrayList<DirectoryElement> directoryElements = new ArrayList<DirectoryElement>();

		// get child nodes from directory with index from parameter
		ArrayList<Node> childs = this.getDirectoryFiles(mReferenceDirectories, directoryIndex);

		for (int i = 0; i != childs.size(); ++i) {

			// create common path-parameter for video and image parameter
			String path = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FULL_PATH).getNodeValue();

			// if node is image then create ScenarioImage object...
			if (childs.get(i).getNodeName().equals(XML_TAG_IMAGE)) {
				// try get tip path
				String tipPath = null;
				try {
					tipPath = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_TIP_PATH).getNodeValue();
				} catch (Exception e) {
					tipPath = null;
				}

				String videoFilePath = null;
				try {
					videoFilePath = childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_VIDEO_FILE_PATH)
							.getNodeValue();
				} catch (Exception e) {
					videoFilePath = null;
				}

				// try get mark for tip
				int tipMarks = 0;
				try {
					tipMarks = Integer.parseInt(childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_MARKS_BAN)
							.getNodeValue());
				} catch (Exception e) {
					tipMarks = 0;
				}

				String tipDescription = null;
				try {
					tipDescription = childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_TIP_DESCRIPTION)
							.getNodeValue();
				} catch (Exception e) {
					tipDescription = null;
				}

				// try get tip video file path
				String tipVideoPath = null;
				try {
					tipVideoPath = childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_HINT_VIDEO_FILE_PATH)
							.getNodeValue();
				} catch (Exception e) {
					tipVideoPath = null;
				}

				directoryElements.add(new ReferenceImage(path, videoFilePath, tipPath, tipVideoPath, tipMarks, tipDescription));

			}
		}

		return directoryElements;
	}

	// get hints from reference directory by index
	private ArrayList<DirectoryElement> getImagesFromHintDirectory(int directoryIndex) {
		ArrayList<DirectoryElement> directoryElements = new ArrayList<DirectoryElement>();

		// get child nodes from directory with index from parameter
		ArrayList<Node> childs = this.getDirectoryFiles(mHintDirectories, directoryIndex);

		for (int i = 0; i != childs.size(); ++i) {

			// create common path-parameter for video and image parameter
			String path = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FULL_PATH).getNodeValue();

			// if node is image then create ScenarioImage object...
			if (childs.get(i).getNodeName().equals(XML_TAG_IMAGE)) {

				String videoFilePath = null;
				try {
					videoFilePath = childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_VIDEO_FILE_PATH)
							.getNodeValue();
				} catch (Exception e) {
					videoFilePath = null;
				}

				directoryElements.add(new HintImage(path, videoFilePath));

			}
		}

		return directoryElements;
	}
	
	public ArrayList<UnremoveDirectoryImage> getUnremoveImages() {
		ArrayList<UnremoveDirectoryImage> images = new ArrayList<UnremoveDirectoryImage>();
		
		try {
			ArrayList<Node> childs = this.getDirectoryFiles(mUnremoveDirectory, 0);
			if (childs == null)
				return null;
		
			for (int i = 0; i != childs.size(); ++i) {
				String path = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FULL_PATH)
						.getNodeValue();
				
				String videoPath = null;
				try {
					videoPath = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_VIDEO_FILE_PATH)
							.getNodeValue();
				} catch (Exception e) {
					videoPath = null;
				}
			
				images.add(new UnremoveDirectoryImage(path, videoPath));
			}
		
			return images;
		} catch(Exception e) {
			return null;
		}
	}
	
	// get images from scenario directory by index
	private ArrayList<DirectoryElement> getImagesFromScenarioDirectory(int directoryIndex) {
		ArrayList<DirectoryElement> directoryElements = new ArrayList<DirectoryElement>();
		
		// get child nodes from directory with index from parameter
		ArrayList<Node> childs = this.getDirectoryFiles(mScenarioDirectories, directoryIndex);
		
		for (int i = 0; i != childs.size(); ++i) {
			
			// create common path-parameter for video and image parameter
			String path = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FULL_PATH).getNodeValue();
			
			// if node is image then create ScenarioImage object...
			if (childs.get(i).getNodeName().equals(XML_TAG_IMAGE)) {
				// try get tip path
				String tipPath = null;
				try {
					tipPath = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_TIP_PATH).getNodeValue();
				} catch (Exception e) {
					tipPath = null;
				}

				String tipDescription = null;
				try {
					tipDescription = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_TIP_DESCRIPTION).getNodeValue();
				} catch (Exception e) {
					tipDescription = null;
				}

				String referPath = null;
				try {
					referPath = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_REFER_PATH).getNodeValue();
				} catch (Exception e) {
					referPath = null;
				}

				String videoFilePath = null;
				try {
					videoFilePath = childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_VIDEO_FILE_PATH)
							.getNodeValue();
				} catch (Exception e) {
					videoFilePath = null;
				}

				// try get mark for tip
				int tipMarks = 0;
				try {
					tipMarks = Integer.parseInt(childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_MARKS_BAN)
							.getNodeValue());
				} catch (Exception e) {
					tipMarks = 0;
				}

				// try get tip video file path
				String tipVideoPath = null;
				try {
					tipVideoPath = childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_HINT_VIDEO_FILE_PATH)
							.getNodeValue();
				} catch (Exception e) {
					tipVideoPath = null;
				}

				String referVideoPath = null;
				try {
					referVideoPath = childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_REFERENCE_VIDEO_FILE_PATH)
							.getNodeValue();
				} catch (Exception e) {
					referVideoPath = null;
				}

				boolean scaled = false;
				try {
					scaled = Boolean.parseBoolean( childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_SCALED).getNodeValue() );
				} catch (Exception e) {
					scaled = false;
				}

				directoryElements.add(new ScenarioImage(path, videoFilePath, tipPath, tipMarks, tipDescription,
						referPath, referVideoPath, scaled));
				
			}
		}
		
		return directoryElements;
	}
	
	/**
	 * Get all directory in Scenario xml tag
	 * @return - array with ScenarioDirectory objects
	 */
	public ArrayList<ScenarioDirectory> getScenarioDirectories() {
		ArrayList<ScenarioDirectory> dirs = new ArrayList<ScenarioDirectory>();

		for (int i = 0; i != mScenarioDirectories.size(); ++i) {
			String code;
			try {
				code = mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_CODE).getNodeValue();
			} catch (Exception e) {
				code = "";
			}
			
			int marks = 0;
			try {
				marks = Integer.parseInt( mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_MARKS).getNodeValue() );
			} catch (Exception e) {
				marks = 0;
			}
			
			// read messages on key input...
			String messageValid = null;
			try {
				messageValid = mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_VALID_CODE).getNodeValue();
				Log.d("My", "Valid: " + messageValid);
			} catch (Exception e) {
				messageValid = null;
			}
						
			String messageInvalid = null;
			try {
				messageInvalid = mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_INVALID_CODE).getNodeValue();
				Log.d("My", "Invalid: " + messageInvalid);
			} catch (Exception e) {
				messageInvalid = null;
			} 
			
			ArrayList<DirectoryElement> images = this.getImagesFromScenarioDirectory(i);
			
			ScenarioDirectory directory = new ScenarioDirectory(code, marks, images, messageValid, messageInvalid);
			
			try {
				String key = mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_KEY1).getNodeValue();
				String message = mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_MESSAGE1).getNodeValue();
				directory.setFailKey1(key, message);
			} catch (Exception e) {
				directory.setFailKey1("", "");
			}
			
			try {
				String key = mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_KEY2).getNodeValue();
				String message = mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_MESSAGE2).getNodeValue();
				directory.setFailKey2(key, message);
			} catch (Exception e) {
				directory.setFailKey2("", "");
			}
			
			try {
				String key = mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_KEY3).getNodeValue();
				String message = mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_MESSAGE3).getNodeValue();
				directory.setFailKey3(key, message);
			} catch (Exception e) {
				directory.setFailKey3("", "");
			}
			
			try {
				String key = mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_KEY4).getNodeValue();
				String message = mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_MESSAGE4).getNodeValue();
				directory.setFailKey4(key, message);
			} catch (Exception e) {
				directory.setFailKey4("", "");
			}

			try {
				String failExit = mScenarioDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_EXIT).getNodeValue();
				directory.setFailExit(failExit);
			} catch (Exception e) {
				directory.setFailExit("");
			}
 			
			dirs.add(directory);
		}
		
		return dirs;
	}

	public ArrayList<ScenarioDirectory> getFailDirectories() {
		ArrayList<ScenarioDirectory> dirs = new ArrayList<ScenarioDirectory>();

		for (int i = 0; i != mFailDirectories.size(); ++i) {
			String code = "";
			try {
				code = mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_CODE).getNodeValue();
			} catch (Exception e) {
				code = "";
			}

			int marks = 0;
			try {
				marks = Integer.parseInt( mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_MARKS).getNodeValue() );
			} catch (Exception e) {
				marks = 0;
			}

			// read messages on key input...
			String messageValid = null;
			try {
				messageValid = mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_VALID_CODE).getNodeValue();
				Log.d("My", "Valid: " + messageValid);
			} catch (Exception e) {
				messageValid = null;
			}

			String messageInvalid = null;
			try {
				messageInvalid = mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_INVALID_CODE).getNodeValue();
				Log.d("My", "Invalid: " + messageInvalid);
			} catch (Exception e) {
				messageInvalid = null;
			}

			ArrayList<DirectoryElement> images = this.getImagesFromFailDirectory(i);

			ScenarioDirectory directory = new ScenarioDirectory(code, marks, images, messageValid, messageInvalid);

			try {
				String key = mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_KEY1).getNodeValue();
				String message = mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_MESSAGE1).getNodeValue();
				directory.setFailKey1(key, message);
			} catch (Exception e) {
				directory.setFailKey1("", "");
			}

			try {
				String key = mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_KEY2).getNodeValue();
				String message = mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_MESSAGE2).getNodeValue();
				directory.setFailKey2(key, message);
			} catch (Exception e) {
				directory.setFailKey2("", "");
			}

			try {
				String key = mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_KEY3).getNodeValue();
				String message = mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_MESSAGE3).getNodeValue();
				directory.setFailKey3(key, message);
			} catch (Exception e) {
				directory.setFailKey3("", "");
			}

			try {
				String key = mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_KEY4).getNodeValue();
				String message = mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_MESSAGE4).getNodeValue();
				directory.setFailKey4(key, message);
			} catch (Exception e) {
				directory.setFailKey4("", "");
			}

			try {
				String failExit = mFailDirectories.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FAIL_EXIT).getNodeValue();
				directory.setFailExit(failExit);
			} catch (Exception e) {
				directory.setFailExit("");
			}

			dirs.add(directory);
		}

		return dirs;
	}

	private ArrayList<DirectoryElement> getImagesFromFailDirectory(int directoryIndex) {
		ArrayList<DirectoryElement> directoryElements = new ArrayList<DirectoryElement>();

		// get child nodes from directory with index from parameter
		ArrayList<Node> childs = this.getDirectoryFiles(mFailDirectories, directoryIndex);

		for (int i = 0; i != childs.size(); ++i) {

			// create common path-parameter for video and image parameter
			String path = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_FULL_PATH).getNodeValue();

			// if node is image then create ScenarioImage object...
			if (childs.get(i).getNodeName().equals(XML_TAG_IMAGE)) {
				// try get tip path
				String tipPath = null;
				try {
					tipPath = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_TIP_PATH).getNodeValue();
				} catch (Exception e) {
					tipPath = null;
				}

				String tipDescription = null;
				try {
					tipDescription = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_TIP_DESCRIPTION).getNodeValue();
				} catch (Exception e) {
					tipDescription = null;
				}

				String referPath = null;
				try {
					referPath = childs.get(i).getAttributes().getNamedItem(XML_ATTRIBUTE_REFER_PATH).getNodeValue();
				} catch (Exception e) {
					referPath = null;
				}

				String videoFilePath = null;
				try {
					videoFilePath = childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_VIDEO_FILE_PATH)
							.getNodeValue();
				} catch (Exception e) {
					videoFilePath = null;
				}

				// try get mark for tip
				int tipMarks = 0;
				try {
					tipMarks = Integer.parseInt(childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_MARKS_BAN)
							.getNodeValue());
				} catch (Exception e) {
					tipMarks = 0;
				}

				// try get tip video file path
				String tipVideoPath = null;
				try {
					tipVideoPath = childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_HINT_VIDEO_FILE_PATH)
							.getNodeValue();
				} catch (Exception e) {
					tipVideoPath = null;
				}

				String referVideoPath = null;
				try {
					referVideoPath = childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_REFERENCE_VIDEO_FILE_PATH)
							.getNodeValue();
				} catch (Exception e) {
					referVideoPath = null;
				}

				boolean scaled = false;
				try {
					scaled = Boolean.parseBoolean( childs.get(i).getAttributes()
							.getNamedItem(XML_ATTRIBUTE_SCALED).getNodeValue() );
				} catch (Exception e) {
					scaled = false;
				}

				directoryElements.add(new ScenarioImage(path, videoFilePath, tipPath, tipMarks, tipDescription,
						referPath, referVideoPath, scaled));

			}
		}

		return directoryElements;
	}
	
	public List<AdditionalCode> getAdditionalCodes() {
		List<AdditionalCode> codes = new ArrayList<AdditionalCode>();
		
		if (mAdditionalCodeNode != null) {
			NodeList childs = mAdditionalCodeNode.getChildNodes();
			for (int i = 0; i != childs.getLength(); ++i) {
				try {
					Node child = childs.item(i);
					
					String code = child.getAttributes().getNamedItem(XML_ATTR_CODE_CODE).getNodeValue();
					String message = child.getAttributes().getNamedItem(XML_ATTR_CODE_MESSAGE).getNodeValue();
					int mark = Integer.parseInt(child.getAttributes().getNamedItem(XML_ATTR_CODE_MARK).getNodeValue());
					boolean isAnswered = Boolean.parseBoolean(child.getAttributes().getNamedItem(XML_ATTR_IS_ANSWERED).getNodeValue());
				
					AdditionalCode additionalCode = new AdditionalCode(code, message, mark, isAnswered);
					codes.add(additionalCode);
				} catch (Exception e) {}
			}
		}
		
		return codes;
	}
	
	public void setAdditionalCodeAnswered(AdditionalCode code) {
		if (mAdditionalCodeNode != null) {
			
			NodeList childs = mAdditionalCodeNode.getChildNodes();
			for (int i = 0; i != childs.getLength(); ++i) {
				
				Node answeredCode = childs.item(i);
				
				try {
					String childCode = answeredCode.getAttributes().getNamedItem(XML_ATTR_CODE_CODE).getNodeValue();
						
					if (code.getCode().equals(childCode)) {
						answeredCode.getAttributes().getNamedItem(XML_ATTR_IS_ANSWERED).setNodeValue("true");
						try {
							writeToFile(mDocument, mXmlFile);
						} catch (IOException e) {}
					
						return;
					}
				} catch (Exception e) {}
			}
		}
	}
	
	/**
	 * Writes XML information to file.
	 * @param doc - Document class reference
	 * @param filePath - output file path
	 * @throws IOException
	 */
	private void writeToFile(Document doc, String filePath) throws IOException {
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = factory.newTransformer();
		} catch (TransformerConfigurationException e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		
		Properties outFormat = new Properties();
        outFormat.setProperty(OutputKeys.INDENT, "yes");
        outFormat.setProperty(OutputKeys.METHOD, "xml"); 
        outFormat.setProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        outFormat.setProperty(OutputKeys.VERSION, "1.0");
        outFormat.setProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperties(outFormat);
		
		DOMSource domSource = new DOMSource(doc.getDocumentElement());
		OutputStream output = new ByteArrayOutputStream();
		StreamResult result = new StreamResult(output);
		
		try {
			transformer.transform(domSource, result);
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (Exception e) {}
		
		Writer writer = new FileWriter(filePath);
		writer.write(output.toString());
		writer.close();
	}
	
	/**
	 * Returns a list of files path that needed to remove if a code was typed.
	 * @param code - Code associated with removed files.
	 * @return - List of files path.
	 */
	public List<File> getRemovedFilesForCode(String code) {
		List<File> result = new ArrayList<File>();
		Node codeElement = getElementByCode(code);
		if (codeElement != null) {
			NodeList fileNodes = codeElement.getChildNodes();
			for (int i = 0; i != fileNodes.getLength(); ++i) {
				Node fileNode = fileNodes.item(i);
				try {
					String imagePath = fileNode.getAttributes().getNamedItem(XML_ATTRIBUTE_NAME).getNodeValue();
					imagePath = imagePath.replace('\\', '/');
					File file = new File(Environment.getExternalStorageDirectory().getPath() +
			                context.getResources().getString(R.string.root_path) +
			                config.getCurrentScenarioFile() +
			                imagePath);
					if (file.exists()) {
						result.add(file);
					}
				} catch (Exception e) {}
			}
		}
		return result;
	}
	
	private Node getElementByCode(String code) {
		NodeList codeElements = removedByCodeNode.getChildNodes();
		for (int i = 0; i != codeElements.getLength(); ++i) {
			Node codeElement = codeElements.item(i);
			try {
				String codeOfElement = codeElement.getAttributes().getNamedItem(XML_ATTR_CODE_CODE).getNodeValue();
				if (code.equals(codeOfElement)) {
					return codeElement;
				}
			} catch (Exception e) {}
		}
		return null;
	}
}