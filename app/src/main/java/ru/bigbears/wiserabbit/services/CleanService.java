/**
 * 
 */
package ru.bigbears.wiserabbit.services;

import java.io.File;

import org.joda.time.DateTime;

import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.consts.ScenarioStructure;
import ru.bigbears.wiserabbit.database.Database;
import ru.bigbears.wiserabbit.database.Database.DirectoriesCursor;
import ru.bigbears.wiserabbit.models.RemovedDirectory;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

/**
 * @author user
 * Service launching each time period and remove all directories from database by date.
 */
public class CleanService extends Service {

	private static final int TIMEOUT_IN_MILLIS = 1000 * 60 * 30; // each half-hour
	
	public static void setServiceAlarm(Context context, boolean isOn) {
		Intent intent = new Intent(context, CleanService.class);
		PendingIntent pIntent = PendingIntent.getService(context, 1000, intent, 0);
		
		AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if (isOn) {
			alarm.setRepeating(AlarmManager.RTC, System.currentTimeMillis(), 
					TIMEOUT_IN_MILLIS, pIntent);
		} else {
			alarm.cancel(pIntent);
			pIntent.cancel();
		}
	} 
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("My", "CleanService started...");
		DirectoriesCursor cursor = Database.getInstance(this).getDirectories();
		Log.d("My", "  process cursor with size: " + cursor.getCount());
		processCursor(cursor);
		cursor.close();
		Database.getInstance(this).close();
		
		return START_STICKY;
	}
	
	private void processCursor(DirectoriesCursor cursor) {
		if (cursor.getCount() != 0) {
			cursor.moveToFirst();
			do {
				RemovedDirectory directory = cursor.getRemovedDirectory();
				if (isDirectoryNeededToRemove(directory)) {
					removeDirectory(directory);
				}
			} while (cursor.moveToNext());
		}
	}
	
	private boolean isDirectoryNeededToRemove(RemovedDirectory directory) {
		Log.d("DATE TO REMOVE",directory.getDateToRemove().toString());
		return DateTime.now().isAfter(directory.getDateToRemove());
	}
	
	private void removeDirectory(RemovedDirectory directory) {
		Log.d("My", "FotoPad:");
		Log.d("My", "  - trying to remove directory " + directory.getPath());
		int result = Database.getInstance(this).removeDirectory(directory);
		Log.d("My", "  - result of removing op: " + result);
		
		try {
			// delete scenario content
	        deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.SCENARIO_DIRECTORY));
	        deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.HELPER_DIRECTORY));
	        deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.KEYBOARD_DIRECTORY));
			deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.REFERENCE_DIRECTORY));
			deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.FAIL_EXIT_DIRECTORY));
			deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.HINT_DIRECTORY));
			deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.UNREMOVE_DIRECTORY));
			deleteDirectoryFromSDCard(new File(Environment.getExternalStorageDirectory().getPath() +
					getResources().getString(R.string.pak_path)));
			deleteDirectoryFromSDCard(new File(Environment.getExternalStorageDirectory().getPath() +
					getResources().getString(R.string.root_path)));
	        Log.d("My", "  - result of removing from SD: scenario directories removed SUCCESS");
		} catch (Exception e) {
			Log.e("My", "  - result of removing from SD: FAIL, message - " + e.getMessage());
		}
        
		try {
	        // get Fotopad directory and clear
	        File fotoPadDir = new File(Environment.getExternalStorageDirectory().getPath() + getResources().getString(R.string.pak_path));
	        File[] files = fotoPadDir.listFiles();
	        for (int i = 0; i < files.length; i++) {
	            if (files[i].isDirectory()) {
	            	deleteDirectoryFromSDCard(files[i]);
	            } else {
	                files[i].delete();
	            }
	        }

			new File(Environment.getExternalStorageDirectory().getPath() +
					getResources().getString(R.string.root_path)).mkdirs();
			new File(Environment.getExternalStorageDirectory().getPath() +
					getResources().getString(R.string.pak_path)).mkdirs();
		} catch (Exception e) {
			Log.e("My", "  - Fotopad directory is not remove: message - " + e.getMessage());
		}
	}
	
	private boolean deleteDirectoryFromSDCard(File directory) {
		if (directory.exists()) {
			File[] files = directory.listFiles();
			
			if (files == null) {
				return true;
			}
			
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectoryFromSDCard(files[i]);
				} else {
					files[i].delete();
				}
			}

			try {
				// delete scenario content
				deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.SCENARIO_DIRECTORY));
				deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.HELPER_DIRECTORY));
				deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.KEYBOARD_DIRECTORY));
				deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.REFERENCE_DIRECTORY));
				deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.FAIL_EXIT_DIRECTORY));
				deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.HINT_DIRECTORY));
				deleteDirectoryFromSDCard(new File(directory.getPath() + File.separator + ScenarioStructure.UNREMOVE_DIRECTORY));
				deleteDirectoryFromSDCard(new File(Environment.getExternalStorageDirectory().getPath() +
						getResources().getString(R.string.pak_path)));
				deleteDirectoryFromSDCard(new File(Environment.getExternalStorageDirectory().getPath() +
						getResources().getString(R.string.root_path)));
				Log.d("My", "  - result of removing from SD: scenario directories removed SUCCESS");
			} catch (Exception e) {
				Log.e("My", "  - result of removing from SD: FAIL, message - " + e.getMessage());
			}

			new File(Environment.getExternalStorageDirectory().getPath() +
					getResources().getString(R.string.root_path)).mkdirs();
			new File(Environment.getExternalStorageDirectory().getPath() +
					getResources().getString(R.string.pak_path)).mkdirs();
		} 
		return (directory.delete());
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}