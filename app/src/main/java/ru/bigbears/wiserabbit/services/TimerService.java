package ru.bigbears.wiserabbit.services;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.text.format.Time;
import android.util.Log;

import org.joda.time.LocalDateTime;

import java.util.concurrent.TimeUnit;

import ru.bigbears.wiserabbit.Config;

public class TimerService extends Service {

    public static final int MILLIS_PER_SECOND = 1000;
    public static final int SECONDS_TO_COUNTDOWN = 30;

    private Time startTime;
    private Time endTime;
    private Time nowTime;

    private CountDownTimer timerDown;

    Config mConfig;

    public TimerService() {
        mConfig = new Config(TimerService.this);

        startTime = mConfig.getStartDateTime();
        endTime = mConfig.getEndDateTime();

        nowTime = new Time();
        nowTime.set(LocalDateTime.now().getSecondOfMinute(),LocalDateTime.now().getMinuteOfHour(),
                LocalDateTime.now().getHourOfDay(),LocalDateTime.now().getDayOfYear(),
                LocalDateTime.now().getMonthOfYear(),LocalDateTime.now().getYear());
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void onCreate() {
        super.onCreate();

        if (nowTime.after(endTime)) {
            Log.d("GAME MESSAGE","Game is over!");
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void showTimer(int countdownMillis) {
        if(timerDown != null) { timerDown.cancel(); }
        timerDown = new CountDownTimer(countdownMillis, MILLIS_PER_SECOND) {
            @Override
            public void onTick(long millisUntilFinished) {
                // countdownDisplay.setText("counting down: " +
                // millisUntilFinished / MILLIS_PER_SECOND);
                Log.d("TIMER","counting down: " +
                        millisUntilFinished / MILLIS_PER_SECOND);
            }
            @Override
            public void onFinish() {
                Log.d("TIMER","TIMER FINISHED!");
                // countdownDisplay.setText("KABOOM!");
            }
        }.start();
    }

    void someTask() {
        for (int i = 1; i<=60; i++) {
            Log.d("TIMER", "i = " + i);
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
