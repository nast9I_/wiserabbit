package ru.bigbears.wiserabbit.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.File;

import ru.bigbears.wiserabbit.Config;
import ru.bigbears.wiserabbit.R;
import ru.bigbears.wiserabbit.consts.ScenarioStructure;

public class DeleteReceiver extends BroadcastReceiver {
    private static final String TAG = "My";

    public DeleteReceiver() {}

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        deletePak();
    }

    private boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();

            if (files == null) {
                return true;
            }
            Log.d(TAG, "In delete directory: " + files.length + " files");

            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    Log.d(TAG, "  Delete directory: " + files[i].getName());
                    deleteDirectory(files[i]);
                } else {
                    Log.d(TAG, "  Delete file: " + files[i].getName());
                    files[i].delete();
                }
            }
        } else {
            Log.d(TAG, "Delete path not exist: " + path.getPath());
        }
        return (path.delete());
    }

    private boolean deletePak() {
        boolean isScenarioDeleted = false;

        isScenarioDeleted = true;

        // get Fotopad directory and clear
        File fotoPadDir = new File(Environment.getExternalStorageDirectory().toString() + String.valueOf(R.string.pak_path));
        fotoPadDir.setReadable(true);
        //fotoPadDir.listFiles()[0].delete();
        File[] files = fotoPadDir.listFiles();
        try {
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        } catch (Exception ex) {}

        deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                String.valueOf(R.string.root_path)));
        deleteDirectory(new File(Environment.getExternalStorageDirectory() +
                String.valueOf(R.string.pak_path)));

        Log.d(TAG,"Все данные успешно удалены...");

        return isScenarioDeleted;
    }
}
