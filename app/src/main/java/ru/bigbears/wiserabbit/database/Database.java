package ru.bigbears.wiserabbit.database;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import ru.bigbears.wiserabbit.models.RemovedDirectory;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "fotopadDB.db";
	private static final int SCHEMA_VERSION = 1;
	private static final String DATE_FORMAT = "dd.MM.yyyy HH:mm";
	
	private static Database instance = null;
	private Context context = null;
	
	public synchronized static Database getInstance(Context context) {
		if (instance == null) {
			instance = new Database(context.getApplicationContext());
		}
		return instance;
	}

	private Database(Context context) {
		super(context, DATABASE_NAME, null, SCHEMA_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DirectoriesTable.queryOnCreateTable());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + DirectoriesTable.TABLE_NAME);
		onCreate(db);
	}
	
	public long addDirectory(RemovedDirectory directory) {
		ContentValues cv = new ContentValues();
		cv.put(DirectoriesTable.DIRECTORY_PATH, directory.getPath());
		DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT);
		cv.put(DirectoriesTable.DATE_TO_REMOVE, formatter.print(directory.getDateToRemove()));
		return getWritableDatabase().insert(DirectoriesTable.TABLE_NAME, null, cv);
	}
	
	public DirectoriesCursor getDirectories() {
		Cursor wrapped = getReadableDatabase().rawQuery("SELECT * FROM " + DirectoriesTable.TABLE_NAME, null);
		return new DirectoriesCursor(wrapped);
	}
	
	public int removeDirectory(RemovedDirectory directory) {
		return getWritableDatabase().delete(DirectoriesTable.TABLE_NAME, DirectoriesTable.ID + "="+ directory.getId(), null);
	}
	
	public static class DirectoriesCursor extends CursorWrapper {

		public DirectoriesCursor(Cursor cursor) {
			super(cursor);
		}
		
		public RemovedDirectory getRemovedDirectory() {
			long id = getLong(getColumnIndex(DirectoriesTable.ID));
			String path = getString(getColumnIndex(DirectoriesTable.DIRECTORY_PATH));
			DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT);
			DateTime dateTime = formatter.parseDateTime(getString(getColumnIndex(DirectoriesTable.DATE_TO_REMOVE)));
			return new RemovedDirectory(id, path, dateTime);
		}
	}
}