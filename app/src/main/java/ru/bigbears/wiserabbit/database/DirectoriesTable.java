package ru.bigbears.wiserabbit.database;

public class DirectoriesTable {

	public static final String TABLE_NAME = "RemovedDirectories";
	
	public static final String ID = "_id";
	public static final String DIRECTORY_PATH = "directoryName";
	public static final String DATE_TO_REMOVE = "dateToRemove";
	
	private DirectoriesTable() {}
	
	public static String queryOnCreateTable() {
		StringBuilder builder = new StringBuilder();
		builder.append("create table ")
			.append(TABLE_NAME)
			.append("(")
			.append(ID + " INTEGER primary key autoincrement, ")
			.append(DIRECTORY_PATH + " TEXT, ")
			.append(DATE_TO_REMOVE + " TEXT);");
		return builder.toString();
	}
}