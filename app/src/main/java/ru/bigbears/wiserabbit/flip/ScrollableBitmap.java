package ru.bigbears.wiserabbit.flip;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class ScrollableBitmap {

    private Bitmap mSource;
    private int mCurrentScroll;

    public ScrollableBitmap(Bitmap b) {
        mSource = Bitmap.createBitmap(b);
    }

    public void scaleToWidth(int width) {
        if (mSource.getWidth() == width) {
            return;
        }
        int height = (int) (mSource.getHeight() * ((float)width / mSource.getWidth()));
        mSource = Bitmap.createScaledBitmap(mSource, width, height, false);
    }

    public Bitmap get(int scrollYBy, int height) {
        Log.e("AAA", "scroll by" + scrollYBy);
        mCurrentScroll += scrollYBy;
        ByteBuffer b = ByteBuffer.allocate(mSource.getByteCount());
        mSource.copyPixelsToBuffer(b);

        mCurrentScroll = Math.max(0, mCurrentScroll);
        if (mCurrentScroll + height > mSource.getHeight()) {
            // ensure that bitmap is cut correctly, without white spaces
            mCurrentScroll = mSource.getHeight() - height;
        }
        Log.e("AAA", "scroll " + mCurrentScroll);

        /*Bitmap out = Bitmap.createBitmap(mSource.getWidth(), height, mSource.getConfig());
        Canvas c = new Canvas(out);
        c.drawBitmap(mSource, 0, 0, null);
        c.drawRect(0, scrollYBy, mSource.getWidth(), scrollYBy + height, new Paint());
        return  out;*/
        return Bitmap.createBitmap(mSource, 0, mCurrentScroll, mSource.getWidth(), height);
    }

    public boolean isRecycled() {
        return mSource.isRecycled();
    }
}
