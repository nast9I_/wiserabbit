package ru.bigbears.wiserabbit;

import android.app.Activity;
import android.app.Service;
import android.content.SharedPreferences;
import android.text.format.Time;
import android.util.Log;

import org.joda.time.LocalDateTime;

public class Config {
	public String fileName;
	private int score;

	private OnScoreChangeListener mListener;

	SharedPreferences mySharedPreferences;
	private static final String FOTOPAD_CONFIG = "FOTOPAD";

	public static final String EMPTY_STRING_VALUE = "";

	public interface OnScoreChangeListener {
		void onScoreChange(Config newConfig);
	}

	public void setOnScoreChangeListener(OnScoreChangeListener l) {
		mListener = l;
	}

	public Config(Activity activity) {
		mySharedPreferences = activity.getSharedPreferences(FOTOPAD_CONFIG, Activity.MODE_PRIVATE);

		this.fileName = mySharedPreferences.getString("fileName", "");
		score = mySharedPreferences.getInt("score", 0);
	}

	public Config(Service service) {
		mySharedPreferences = service.getSharedPreferences(FOTOPAD_CONFIG, Service.MODE_PRIVATE);

		this.fileName = mySharedPreferences.getString("fileName", "");
		score = mySharedPreferences.getInt("score", 0);
	}

	public void reset() {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("fileName", fileName);
		editor.putInt("score", 0);

		for (int i = 0; i < 1000; i++) {
			editor.putBoolean("hint 1" + i, false);
			editor.putBoolean("hint 2" + i, false);
			editor.putBoolean("reference " + i, false);
		}

		setCurrentImageIndex(0);

		editor.commit();

		score = 0;
	}

	public void delPak() {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("fileName", "");
		editor.putInt("score", 0);

		for (int i = 0; i < 1000; i++) {
			editor.putBoolean("hint 1" + i, false);
			editor.putBoolean("hint 2" + i, false);
			editor.putBoolean("reference " + i, false);
		}

		setCurrentImageIndex(0);

		editor.commit();

		score = 0;
	}

	public void setUserEmail(String email) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("userEmail", email);
		editor.commit();
	}

	public String getUserEmail() {
		return mySharedPreferences.getString("userEmail","default@wiserabbit.ru");
	}

	public void setCode(String code) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("code", code);
		editor.commit();
	}

	public String getCode() {
		return mySharedPreferences.getString("code","");
	}

	public void setUserLogin(String login) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("userLogin", login);
		editor.commit();
	}

	public String getUserLogin() {
		return mySharedPreferences.getString("userLogin","");
	}

	public void setScore(int score) {
		if (mListener != null) {
			mListener.onScoreChange(this);
		}
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putInt("score", score);
		editor.commit();
	}

	public void setRating(float rating) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putFloat("rating", rating);
		editor.commit();
	}

	public float getRating() {
		return mySharedPreferences.getFloat("rating",4);
	}

	public void setCallback(String callback) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("callback", callback);
		editor.commit();
	}

	public String getCallback() {
		return mySharedPreferences.getString("callback","");
	}

	public void setScoreBunned(int score) {
		if (mListener != null) {
			mListener.onScoreChange(this);
		}
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putInt("score_bunned", score);
		editor.commit();
	}

	public int getScore() {
		this.score = mySharedPreferences.getInt("score", 0);
		return this.score;
	}

	public int getScoreBunned() {
		return mySharedPreferences.getInt("score_bunned", 0);
	}

	public void setCurrentScenarioFile(String fileName) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("fileName", fileName);
		editor.commit();
	}

	public String getCurrentScenarioFile() {
		this.fileName = mySharedPreferences.getString("fileName", EMPTY_STRING_VALUE);
		//Log.d("FILENAME: ",this.fileName);
		return this.fileName;
	}

	public void setCurrentReferenceFile(String fileName) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("fileName", fileName);
		editor.commit();
	}

	public String getCurrentReferenceFile() {
		Log.d("FILENAME: ",this.fileName);
		this.fileName = mySharedPreferences.getString("fileName", EMPTY_STRING_VALUE);
		return this.fileName;
	}

	public String getCurrentHintFile() {
		Log.d("FILENAME: ",this.fileName);
		this.fileName = mySharedPreferences.getString("fileName", EMPTY_STRING_VALUE);
		return this.fileName;
	}

	public void setFilePass(String fileName) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("filePass", fileName);
		editor.commit();
	}

	public String getFilePass() {
		this.fileName = mySharedPreferences.getString("filePass", "");
		return this.fileName;
	}

	public void useHint(int i) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putBoolean("hint 1" + i, true);
		editor.commit();
	}

	public boolean isHintUse(int i) {
		return mySharedPreferences.getBoolean("hint 1" + i, false);
	}

	public void useReferHint(int i) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putBoolean("hint 2" + i, true);
		editor.commit();
	}

	public boolean isReferHintUse(int i) {
		return mySharedPreferences.getBoolean("hint 2" + i, false);
	}

	public void useReferCode(int i) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putBoolean("reference " + i, true);
		editor.commit();
	}

	public boolean isReferCodeUse(int i) {
		return mySharedPreferences.getBoolean("reference " + i, false);
	}

	public void setCurrentImageIndex(int i) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putInt("progressImage", i);
		editor.commit();
	}

	public int getCurrentImageIndex() {
		return mySharedPreferences.getInt("progressImage", 0);
	}

	public void setCurrentDirectoryIndex(int i) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putInt("indexDir", i);
		editor.commit();
	}

	public int getCurrentDirectoryIndex() {
		return mySharedPreferences.getInt("indexDir", 0);
	}

	public void setFailDirectoryIndex(int i) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putInt("indexDir", i);
		editor.commit();
	}

	public int getFailDirectoryIndex() {
		return mySharedPreferences.getInt("indexDir", 0);
	}

	public void setHelperImageIndex(int i) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putInt("helperImage", i);
		editor.commit();
	}

	public int getHelperImageIndex() {
		return mySharedPreferences.getInt("helperImage", 0);
	}

	public void setHelperDirectoryIndex(int i) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putInt("helperIndexDir", i);
		editor.commit();
	}

	public int getHelperDirectoryIndex() {
		return mySharedPreferences.getInt("helperIndexDir", 0);
	}

	public void setMenuVisible(boolean vis) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.commit();
	}

	public boolean getMenuVisible() {
		Log.e("AA", " menu " + mySharedPreferences.getBoolean("menuVisible", true));
		return mySharedPreferences.getBoolean("menuVisible", true);
	}

	public void setPasswordStatusForHelperDirectory(String directoryName, boolean isUses) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putBoolean(directoryName, isUses);
		editor.commit();
	}
	
	public boolean isPasswordUsedForHelperDirectory(String directoryName) {
		return mySharedPreferences.getBoolean(directoryName, true);
	}

	public void setStartDateTime(LocalDateTime localDateTime) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		Time time = new Time();

		time.set(localDateTime.getSecondOfMinute(),localDateTime.getMinuteOfHour(),localDateTime.getHourOfDay(),
				localDateTime.getDayOfMonth(),localDateTime.getMonthOfYear(),localDateTime.getYear());
		editor.putInt("startYear",time.year);
		editor.putInt("startMonth",time.month);
		editor.putInt("startDay",time.monthDay);
		editor.putInt("startHour",time.hour);
		editor.putInt("startMinute",time.minute);
		editor.putInt("startSecond",time.second);

		editor.commit();
	}

	public Time getStartDateTime() {
		LocalDateTime defaultDateTime = LocalDateTime.now();
		Time time = new Time();

		time.set(mySharedPreferences.getInt("startSecond",defaultDateTime.getSecondOfMinute()),
				mySharedPreferences.getInt("startMinute",defaultDateTime.getMinuteOfHour()),
				mySharedPreferences.getInt("startHour",defaultDateTime.getHourOfDay()),
				mySharedPreferences.getInt("startDay",defaultDateTime.getDayOfMonth()),
				mySharedPreferences.getInt("startMonth",defaultDateTime.getMonthOfYear()),
				mySharedPreferences.getInt("startYear",defaultDateTime.getYear()));

		return time;
	}

	public void setEndDateTime(LocalDateTime localDateTime) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		Time time = new Time();

		time.set(localDateTime.getSecondOfMinute(),localDateTime.getMinuteOfHour(),localDateTime.getHourOfDay(),
				localDateTime.getDayOfMonth(),localDateTime.getMonthOfYear(),localDateTime.getYear());
		editor.putInt("endYear",time.year);
		editor.putInt("endMonth",time.month);
		editor.putInt("endDay",time.monthDay);
		editor.putInt("endHour",time.hour);
		editor.putInt("endMinute",time.minute);
		editor.putInt("endSecond",time.second);

		editor.commit();
	}

	public Time getEndDateTime() {
		LocalDateTime defaultDateTime = LocalDateTime.now();
		Time time = new Time();

		time.set(mySharedPreferences.getInt("endSecond",defaultDateTime.getSecondOfMinute()),
				mySharedPreferences.getInt("endMinute",defaultDateTime.getMinuteOfHour()),
				mySharedPreferences.getInt("endHour",defaultDateTime.getHourOfDay()),
				mySharedPreferences.getInt("endDay",defaultDateTime.getDayOfMonth()),
				mySharedPreferences.getInt("endMonth",defaultDateTime.getMonthOfYear()),
				mySharedPreferences.getInt("endYear",defaultDateTime.getYear()));

		return time;
	}

	public void setChecked(boolean bool) {
		Log.e("AA", "setchecked " + bool);
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putBoolean("checked", bool);
		editor.commit();
	}

	public boolean getChecked() {
		return mySharedPreferences.getBoolean("checked", false);
	}

	public void setCanVisibleMenu(boolean bool) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putBoolean("visibleMenu", bool);
		editor.commit();
	}

	public boolean getCanVisibleMenu() {
		return mySharedPreferences.getBoolean("visibleMenu", false);
	}

	public void setFailDirUse(boolean failDirUse, int curDir) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putBoolean("use_fail_dir_"+ curDir, failDirUse);
		editor.commit();
	}

	public boolean isFailDirUse(int curDir) {
		return mySharedPreferences.getBoolean("use_fail_dir_"+ curDir, false);
	}

	public void setNotepad(boolean notepad) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putBoolean("notepad", notepad);
		editor.commit();
	}

	public boolean getNotepad() {
		return mySharedPreferences.getBoolean("notepad", false);
	}

	public void setDownloadCode(String code) {
		SharedPreferences.Editor editor = mySharedPreferences.edit();
		editor.putString("dcode", code);
		editor.commit();
	}

	public String getDownloadCode() {
		return mySharedPreferences.getString("dcode","");
	}

}
