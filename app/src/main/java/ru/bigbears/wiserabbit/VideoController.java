/**
 * 
 */
package ru.bigbears.wiserabbit;

import java.io.File;

import ru.bigbears.wiserabbit.animations.VideoViewAnimation;

import android.app.Activity;
import android.graphics.Color;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.VideoView;

/**
 * @author Vladimir
 *
 */
public class VideoController {
	private Activity activity;
	private VideoView videoView;
	private VideoViewAnimation videoViewAnimation;
	private Button playButton;
	private Button toggleScreenModeButton;
	
	private static final float VIDEO_VIEW_SIZE_RATIO = 0.5f;
	private int screenWidth;
	private int screenHeight;
	
	private static final int VIDEO_VIEW_LEFT_MARGIN = 8;
	private int leftMargin;
	
	public static final int VIDEO_STATE_PLAY = 0;
    public static final int VIDEO_STATE_STOP = 1;
    private int playState = VIDEO_STATE_PLAY;
    
    public static final int FULL_SCREEN = 10;
    public static final int NORMAL_SCREEN = 11;
    private int screenMode = NORMAL_SCREEN;

	private boolean btnsIsVisible;

	public VideoController(Activity a, DisplayMetrics metrics) {
		activity = a;
		
		screenWidth  = (int)(metrics.widthPixels * VIDEO_VIEW_SIZE_RATIO);
        screenHeight = (int)(metrics.heightPixels * VIDEO_VIEW_SIZE_RATIO);
        leftMargin   = (int)(metrics.density * VIDEO_VIEW_LEFT_MARGIN);
        
        videoView = (VideoView)activity.findViewById(R.id.smallVideoView);
        playButton = (Button)activity.findViewById(R.id.btnPlayVideo);
        toggleScreenModeButton = (Button)activity.findViewById(R.id.btnToggleScreenMode);
        
        videoView.setLayoutParams(getNormalScreenLayoutParams());
		videoView.setMediaController(new MediaController(activity));
		videoView.setZOrderOnTop(true);
        
        videoViewAnimation = new VideoViewAnimation();

		btnsIsVisible = false;
		//hideButtons();
	}
	
	public void setAnimationListener(AnimationListener listener) {
		videoViewAnimation.setAnimationListener(listener);
	}
	
	public void setOnClickListener(OnClickListener listener) {
		playButton.setOnClickListener(listener);
		toggleScreenModeButton.setOnClickListener(listener);
	}
	
	public void setOnCompletionListener(OnCompletionListener listener) {
		videoView.setOnCompletionListener(listener);
	}
	
	public int getScreenVisibility() {
		return videoView.getVisibility();
	}
	
	public int getVideoState() {
		return playState;
	}
	
	public int getScreenMode() {
		return screenMode;
	}
	
	public void handlePlayVideoButton() {
		switch (playState) {
		case VIDEO_STATE_PLAY:
			playState = VIDEO_STATE_STOP;
			playButton.setBackgroundResource(R.drawable.stop);
			//hideButtons();
			
    		videoView.setVisibility(View.VISIBLE);
    		videoView.setBackgroundColor(Color.BLACK);
    		videoView.startAnimation(videoViewAnimation);
    		
    		//videoView.setMediaController(new MediaController(activity));
    			
			break;
			
		case VIDEO_STATE_STOP:
			playState = VIDEO_STATE_PLAY;
			playButton.setBackgroundResource(R.drawable.play);
			showButtons();
			
			screenMode = NORMAL_SCREEN;
			toggleScreenModeButton.setBackgroundResource(R.drawable.full_screen);
			videoView.setLayoutParams(getNormalScreenLayoutParams());
			
			//videoView.setMediaController(null);
			//videoView.suspend();
			videoView.stopPlayback();
			
    		videoView.setVisibility(View.GONE);
    		videoView.setBackgroundColor(0);
			
			break;
				
		default: break;
		}
	}
	
	public void handleToggleScreenModeButton() {
		if (screenMode == NORMAL_SCREEN) {
			videoView.setLayoutParams(getFullScreenLayoutParams());
			toggleScreenModeButton.setBackgroundResource(R.drawable.normal_screen);
			screenMode = FULL_SCREEN;
		} else if (screenMode == FULL_SCREEN) {
			videoView.setLayoutParams(getNormalScreenLayoutParams());
			toggleScreenModeButton.setBackgroundResource(R.drawable.full_screen);
			screenMode = NORMAL_SCREEN;
		}
	}
	
	public void handleOnCompletion() {
		videoView.setVisibility(View.GONE);
		
		screenMode = NORMAL_SCREEN;
		toggleScreenModeButton.setBackgroundResource(R.drawable.full_screen);
		videoView.setLayoutParams(getNormalScreenLayoutParams());
		showButtons();
		playState = VIDEO_STATE_PLAY;
		playButton.setBackgroundResource(R.drawable.play);
	}
	
	public void setControlPanelVisibility(int visibility) {
		playButton.setVisibility(visibility);
		toggleScreenModeButton.setVisibility(visibility);
	}
	
	public void play(File videoFile) {
		// get current window information, and set format, set it up
		// differently, if you need some special effects
		//getWindow().setFormat(PixelFormat.TRANSLUCENT);
		//videoView.setBackgroundColor(0);
		//videoView.setMediaController(new MediaController(activity));
		
		videoView.setVideoPath(videoFile.getPath());
		videoView.requestFocus();
		videoView.start();
	}
	
	private RelativeLayout.LayoutParams getNormalScreenLayoutParams() {
		RelativeLayout.LayoutParams params = new LayoutParams(screenWidth, screenHeight);
        params.leftMargin = leftMargin;
        params.addRule(RelativeLayout.ABOVE, R.id.btnPlayVideo);
        
        return params;
	}
	
	private RelativeLayout.LayoutParams getFullScreenLayoutParams() {
		RelativeLayout.LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        
        return params;
	}

	public void hideButtons() {
		playButton.setVisibility(View.INVISIBLE);
		toggleScreenModeButton.setVisibility(View.INVISIBLE);
		btnsIsVisible = false;
	}

	public void showButtons() {
		playButton.setVisibility(View.VISIBLE);
		toggleScreenModeButton.setVisibility(View.VISIBLE);
		btnsIsVisible = true;
	}

	public boolean isBtnsVisible() {
		return btnsIsVisible;
	}
}
